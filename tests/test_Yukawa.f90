program test_Yukawa
  
!!  use mod_R_1_norm
  
  implicit none
  
  ! local variables
  integer, parameter      :: nmax = 3
  integer, parameter      :: ng   = 127
  real(kind=8), parameter :: PI   = 3.1415926535897932384626433832795d0
  real(kind=8), parameter :: a    = 1.0d16
  real(kind=8), parameter :: b    = 3.5d0
  real(kind=8), parameter :: d    = 2.0d0
  real(kind=8), parameter :: e    = 1.5d0
  real(kind=8), parameter :: lg   = 7.0d0
  real(kind=8), parameter :: x0   = -12.0d0/2
  real(kind=8), parameter :: y0   = -13.5d0/2
  real(kind=8), parameter :: z0   =  15.0d0/2
  integer, parameter      :: nx2  = 2
  integer, parameter      :: ny2  = 0
  integer, parameter      :: nz2  = 1
  integer, parameter      :: nx3  = 0
  integer, parameter      :: ny3  = 1
  integer, parameter      :: nz3  = 1
  integer, parameter      :: l1   = 1
  integer, parameter      :: m1   = 0
  integer, parameter      :: l2   = 2
  integer, parameter      :: m2   =-1
  integer, parameter      :: l3   = 3
  integer, parameter      :: m3   = 2
  integer, parameter      :: lmax = 6
  real(kind=8), parameter :: r1(3) = (/ 0.0d0, 0.0d0, 0.0d0 /)
  real(kind=8), parameter :: r2(3) = (/ 0.5d0,-0.3d0, 0.2d0 /)
  real(kind=8), parameter :: r3(3) = (/ 0.1d0, 0.0d0, 0.5d0 /)
  integer      :: i
  integer      :: ik
  integer      :: j
  integer      :: p
  integer      :: l
  integer      :: m
  integer      :: nx
  integer      :: ny
  integer      :: nz
  integer      :: nx_
  integer      :: ny_
  integer      :: nz_
  real(kind=8) :: c(455)
  real(kind=8) :: x
  real(kind=8) :: y
  real(kind=8) :: z
  real(kind=8) :: tmp
  real(kind=8) :: C_Coulomb_C
  real(kind=8) :: Y_Coulomb_Y
  real(kind=8) :: Y_Coulomb_C
  real(kind=8) :: C_Coulomb_Y
  real(kind=8) :: CC_Coulomb_Ion
  real(kind=8) :: CC_Coulomb_C
  real(kind=8) :: CC_Coulomb_Y
  real(kind=8) :: CY_Coulomb_C
  real(kind=8) :: YC_Coulomb_C
  real(kind=8) :: CC_Coulomb_CC
  real(kind=8) :: YY_Coulomb_Ion
  real(kind=8) :: YY_Coulomb_Y
  real(kind=8) :: YY_Coulomb_C
  real(kind=8) :: YC_Coulomb_Y
  real(kind=8) :: CY_Coulomb_Y
  real(kind=8) :: YY_Coulomb_YY
  real(kind=8) :: Y_Value
  real(kind=8) :: norme
  real(kind=8) :: omega
  real(kind=8) :: dist
  
  complex(kind=8) :: k
  complex(kind=8) :: V
  complex(kind=8) :: S1
  complex(kind=8) :: S2
  complex(kind=8) :: S3
  complex(kind=8) :: S1_
  complex(kind=8) :: S1__

  complex(kind=8) :: C_Yukawa_C
  complex(kind=8) :: Y_Yukawa_Y
  complex(kind=8) :: Y_Yukawa_C
  complex(kind=8) :: C_Yukawa_Y
  complex(kind=8) :: CC_Yukawa_Ion
  complex(kind=8) :: CC_Yukawa_C
  complex(kind=8) :: CC_Yukawa_Y
  complex(kind=8) :: CY_Yukawa_C
  complex(kind=8) :: YC_Yukawa_C
  complex(kind=8) :: CC_Yukawa_CC
  complex(kind=8) :: YY_Yukawa_Ion
  complex(kind=8) :: YY_Yukawa_Y
  complex(kind=8) :: YY_Yukawa_C
  complex(kind=8) :: YC_Yukawa_Y
  complex(kind=8) :: CY_Yukawa_Y
  complex(kind=8) :: YY_Yukawa_YY

  
  INTEGER :: count_rate
  INTEGER :: start_count,end_count
            
  ! compute 1 norm for delta function
  c=0.0d0
  c(1)=1.0d0
!!  norme=R_1_norm(a,c,0)
  norme=c(1)*PI**1*sqrt(PI)/(a*sqrt(a))
  
  ! compare with coulomb integrals
  k=0.0d0 
  
  ! check consistency between expert and standard drivers
  do nx=0,6
  do ny=0,6-nx
  do nz=0,6-nx-ny
    
    ! check integral
    S1 = C_Coulomb_C(b,r2,nx,ny,nz,d,r3,nx2,ny2,nz2)
    S2 = C_Yukawa_C(k,b,r2,nx,ny,nz,d,r3,nx2,ny2,nz2)
    write (*,'(A,I2,A,I2,A,I2,A,E22.16,E22.16,A,E22.16,E22.16)')  & 
          'nx=',nx,'  ny=',ny,'  nz=',nz,'  C_Yukawa_C[k=0]=',S2,'  C_Coulomb_C result=',S1
        
    if ( 2.0* abs( ( S1 - S2 ) / ( S1 + S2 ) ) > 1.0d-8 .or. S1.ne.S1 ) then
      print *,'Error: expert <CC> Two center Coulomb failed'
      print *,''
      print *,'test_Coulomb failed'
      print *,''
      stop 1
    end if
    
  end do
  end do
  end do
  
  print *,''
  write (*,'(A)') 'test_Yukawa passed up to l=6'
  print *,''
  
  ! check consistency between expert and standard drivers
  do l=0,6
  do m=-l,l
    
    ! check integral 
    S1 = Y_Coulomb_C(b,r2,l,m,d,r3,nx2,ny2,nz2)
    S2 = Y_Yukawa_C(k,b,r2,l,m,d,r3,nx2,ny2,nz2)
    
    write (*,'(A,I2,A,I2,A,E22.16,E22.16,A,E22.16,E22.16)')  & 
          'l=',l,'  m=',m,'  Y_Yukawa_C[k=0] =',S2,'  C_Coulomb_C result=',S1
        
    if ( 2.0* abs( ( S1 - S2 ) / ( S1 + S2 ) ) > 1.0d-8 .or. S1.ne.S1 ) then
      print *,'Error: expert <CC> Two center Coulomb failed'
      print *,''
      print *,'test_Coulomb failed'
      print *,''
      stop 1
    end if
    
    S1 = YY_Coulomb_C(b,r2,l,m,d,r3,l2,m2,d,r3,nx2,ny2,nz2)
    S2 = YY_Yukawa_C(k,b,r2,l,m,d,r3,l2,m2,d,r3,nx2,ny2,nz2)
    
    write (*,'(A,I2,A,I2,A,E22.16,E22.16,A,E22.16,E22.16)')  & 
          'l=',l,'  m=',m,'  YY_Yukawa_C[k=0]=',S2,'  C_Coulomb_C result=',S1
        
    if ( 2.0* abs( ( S1 - S2 ) / ( S1 + S2 ) ) > 1.0d-8 .or. S1.ne.S1 ) then
      print *,'Error: expert <CC> Two center Coulomb failed'
      print *,''
      print *,'test_Coulomb failed'
      print *,''
      stop 1
    end if
    
  end do
  end do
  
  print *,''
  write (*,'(A)') 'test_Coulomb passed up to l=6'
  print *,''
  
  ! check consistency between expert and standard drivers
  do nx=0,6
  do ny=0,6-nx
  do nz=0,6-nx-ny
    
    ! check integral 
    S1 = CC_Coulomb_CC(b,r2,nx,ny,nz,d,r3,nx2,ny2,nz2,b,r2,nx,ny,nz,d,r3,nx2,ny2,nz2)
    S2 = CC_Yukawa_CC(k,b,r2,nx,ny,nz,d,r3,nx2,ny2,nz2,b,r2,nx,ny,nz,d,r3,nx2,ny2,nz2)
    
    write (*,'(A,I2,A,I2,A,I2,A,E22.16,E22.16,A,E22.16,E24.16,E22.16,E24.16)')  & 
          'nx=',nx,'  ny=',ny,'  nz=',nz,'  Y_Yukawa_C[k=0]=',S2,'  C_Coulomb_C result=',S1
      
    if ( 2.0* abs( ( S1 - S2 ) / ( S1 + S2 ) ) > 1.0d-8 .or. S1.ne.S1 ) then
      print *,'Error: expert <CC> Four center Coulomb failed'
      print *,''
      print *,'test_Coulomb failed'
      print *,''
      stop 1
    end if
    
  end do
  end do
  end do
  
  print *,''
  write (*,'(A)') 'test_Coulomb passed up to l=6'
  print *,''
  
 
  do ik=1,2
  
  !! k for explicit calculations
  if ( ik.eq.1 ) then
    k=complex(1.0d0,0.0d0) 
  else
    k=complex(0.0d0,1.0d0) 
  end if
   
  !!
  !! check for C routines
  !!
!!  norme=R_1_norm(a,c,0)
  norme=c(1)*PI**1*sqrt(PI)/(a*sqrt(a))
  do nx=0,nmax
    do ny=0,nmax
      do nz=0,nmax
        
        ! explicit calculation
        S1=0.0d0
        omega=0.1D0
        do p=0,ng
          z=-lg+(2.0d0*lg*p)/ng
          do j=0,ng
            y=-lg+(2.0d0*lg*j)/ng
            do i=0,ng
              x=-lg+(2.0d0*lg*i)/ng
              dist = sqrt((x-x0)**2+(y-y0)**2+(z-z0)**2)
              V    = exp(-k*dist)/dist
              tmp  = x**nx * y**ny * z**nz * exp( -b * (x**2+y**2+z**2) )
              S1   = S1 + tmp * V
            end do
          end do
        end do
        S1=S1*8.0d0*lg**3/ng**3
        
        ! get corresponding overlap
        S1_= C_Yukawa_C(k,a,(/x0,y0,z0/),0,0,0,b,r1,nx,ny,nz,omega)/norme
        
        ! get corresponding symmetric overlap
        S1__= C_Yukawa_C(k,b,r1,nx,ny,nz,a,(/x0,y0,z0/),0,0,0,omega)/norme
        
        write (*,'(A,I2,A,I2,A,I2,A,E22.16,E22.16,A,E22.16,E24.16,E22.16,E24.16)')  & 
          'nx=',nx,'  ny=',ny,'  nz=',nz,'  explicit interaction=',S1,'  C_Yukawa_C result=',S1_,S1__
        
        ! test result
        if ( abs((S1-S1_)/S1) > 1.0d-4 ) then
          print *,'Error: Yukawa differs from explicit calculation for case'
          print *,'       nx =',nx
          print *,'       ny =',ny
          print *,'       nz =',nz
          print *,''
          print *,'test_Yukawa failed'
          print *,''
          stop 1
        end if
        if ( abs((S1-S1__)/S1) > 1.0d-4 ) then
          print *,'Error: Yukawa differs from explicit calculation for case'
          print *,'       nx =',nx
          print *,'       ny =',ny
          print *,'       nz =',nz
          print *,''
          print *,'test_Yukawa failed'
          print *,''
          stop 1
        end if
        
      end do
    end do
  end do
  
  print *,''
  write (*,'(A,I1,A,I1,A,I1)') 'test_Yukawa passed up to nx=',nmax,' ny=',nmax,' nz=',nmax
  print *,''

  
  !!
  !! check for Y routine
  !!
  norme = 2.0d0/(4.0d0*atan(1.0d0))*(a*sqrt(a))
  do l=0,lmax
    do m=-l,l
      ! explicit calculation
      omega=0.1D0
      S1=0.0d0
      do i=0,ng
        x=-lg+(2.0d0*lg*i)/ng
        do j=0,ng
          y=-lg+(2.0d0*lg*j)/ng
          do p=0,ng
            z=-lg+(2.0d0*lg*p)/ng
            dist = sqrt((x-x0)**2+(y-y0)**2+(z-z0)**2)
            V    = exp(-k*dist)/dist
            tmp=Y_Value(b,l,m,(/x,y,z/))
            S1 = S1 + tmp * V
          end do
        end do
      end do
      S1=S1*8.0d0*lg**3/ng**3
      
      ! get corresponding interaction
      S1_= Y_Yukawa_Y(k,a,(/x0,y0,z0/),0,0,b,r1,l,m,omega)*norme
      
      ! get corresponding symmetric interaction
      S1__= Y_Yukawa_Y(k,b,r1,l,m,a,(/x0,y0,z0/),0,0,omega)*norme
      
      write (*,'(A,I2,A,I2,A,E22.16,E22.16,A,E22.16,E24.16,E22.16,E24.16)')  & 
        'l=',l,'  m=',m,'  explicit interaction=',S1,'  Y_Yukawa_Y result=',S1_,S1__
        
      ! test result
      if ( abs((S1-S1_)/S1) > 1.0d-4 ) then
        print *,'Error: Yukawa differs from explicit calculation for case'
        print *,'       l =',l
        print *,'       m =',m
        print *,''
        print *,'test_Yukawa failed'
        print *,''
        stop 1
      end if
      if ( abs((S1-S1__)/S1) > 1.0d-4 ) then
        print *,'Error: Yukawa differs from explicit calculation for case'
        print *,'       l =',l
        print *,'       m =',m
        print *,''
        print *,'test_Yukawa failed'
        print *,''
        stop 1
      end if
      
    end do
  end do
  
  print *,''
  write (*,'(A,I1,A,I1,A,I1)') 'test_Yukawa passed up to lmax=',lmax
  print *,''
  
  
  !!
  !! check for C routines
  !!
  do nx=0,nmax
    do ny=0,nmax
      do nz=0,nmax
        
        ! explicit calculation
        S1=0.0d0
        
        do p=0,ng
          z=-lg+(2.0d0*lg*p)/ng
          do j=0,ng
            y=-lg+(2.0d0*lg*j)/ng
            do i=0,ng
              x=-lg+(2.0d0*lg*i)/ng
              dist = sqrt((x-x0)**2+(y-y0)**2+(z-z0)**2)
              V  = exp(-k*dist)/dist
              tmp= x**nx * y**ny * z**nz * exp( -b * (x**2+y**2+z**2) )
              tmp=tmp * (x-r2(1))**nx2 * (y-r2(2))**ny2 * (z-r2(3))**nz2 * exp( -d * ((x-r2(1))**2+(y-r2(2))**2+(z-r2(3))**2) )
              S1 = S1 + tmp * V
            end do
          end do
        end do
        S1=S1*8.0d0*lg**3/ng**3
        
        ! get corresponding overlap
        S1_= CC_Yukawa_Ion(k,d,r2,nx2,ny2,nz2,b,r1,nx,ny,nz,(/x0,y0,z0/))
        
        ! get corresponding symmetric overlap
        S1__= CC_Yukawa_Ion(k,b,r1,nx,ny,nz,d,r2,nx2,ny2,nz2,(/x0,y0,z0/))
        
        write (*,'(A,I2,A,I2,A,I2,A,E22.16,E22.16,A,E22.16,E24.16,E22.16,E24.16)')  & 
          'nx=',nx,'  ny=',ny,'  nz=',nz,'  explicit interaction=',S1,'  CC_Yukawa_Ion result=',S1_,S1__
        
        ! test result
        if ( abs((S1-S1_)/S1) > 1.0d-5 ) then
          print *,'Error: Yukawa differs from explicit calculation for case'
          print *,'       nx =',nx
          print *,'       ny =',ny
          print *,'       nz =',nz
          print *,''
          print *,'test_Yukawa failed'
          print *,''
          stop 1
        end if
        if ( abs((S1-S1__)/S1) > 1.0d-5 ) then
          print *,'Error: Yukawa differs from explicit calculation for case'
          print *,'       nx =',nx
          print *,'       ny =',ny
          print *,'       nz =',nz
          print *,''
          print *,'test_Yukawa failed'
          print *,''
          stop 1
        end if
        
      end do
    end do
  end do
  
  print *,''
  write (*,'(A,I1,A,I1,A,I1)') 'test_Yukawa passed up to nx=',nmax,' ny=',nmax,' nz=',nmax
  print *,''
  
  end do
  
  print *,''
  write (*,'(A)') 'test_Yukawa'
  print *,''
  
end program

