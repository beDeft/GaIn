program test_Coulomb
  
  use mod_R_1_norm
  
  implicit none
  
  ! local variables
  integer, parameter      :: nmax = 3
  integer, parameter      :: ng   = 127
  real(kind=8), parameter :: a    = 0.5d0
  real(kind=8), parameter :: b    = 3.5d0
  real(kind=8), parameter :: d    = 2.0d0
  real(kind=8), parameter :: e    = 1.5d0
  real(kind=8), parameter :: lg   = 7.0d0
  real(kind=8), parameter :: x0   = -12.0d0/2
  real(kind=8), parameter :: y0   = -13.5d0/2
  real(kind=8), parameter :: z0   =  15.0d0/2
  integer, parameter      :: nx2  = 2
  integer, parameter      :: ny2  = 0
  integer, parameter      :: nz2  = 1
  integer, parameter      :: nx3  = 0
  integer, parameter      :: ny3  = 1
  integer, parameter      :: nz3  = 1
  integer, parameter      :: l1   = 1
  integer, parameter      :: m1   = 0
  integer, parameter      :: l2   = 2
  integer, parameter      :: m2   =-1
  integer, parameter      :: l3   = 3
  integer, parameter      :: m3   = 2
  integer, parameter      :: lmax = 6
  real(kind=8), parameter :: r1(3) = (/-0.4d0, 0.2d0, 0.1d0 /)
  real(kind=8), parameter :: r2(3) = (/ 0.5d0,-0.3d0, 0.2d0 /)
  real(kind=8), parameter :: r3(3) = (/ 0.1d0, 0.1d0, 0.5d0 /)
  real(kind=8), parameter :: step  = 1.0d-5
  integer      :: i
  integer      :: j
  integer      :: k
  integer      :: l
  integer      :: m
  integer      :: nx
  integer      :: ny
  integer      :: nz
  integer      :: nx_
  integer      :: ny_
  integer      :: nz_
  real(kind=8) :: c(455)
  real(kind=8) :: x
  real(kind=8) :: y
  real(kind=8) :: z
  real(kind=8) :: tmp
  real(kind=8) :: V
  real(kind=8) :: r1_p(3)
  real(kind=8) :: r1_m(3)
  real(kind=8) :: S1_p
  real(kind=8) :: S1_m
  real(kind=8) :: S1
  real(kind=8) :: dS(3)
  real(kind=8) :: Y_Overlap_Y
  real(kind=8) :: Y_Laplacian_Y
  real(kind=8) :: YY_Coulomb_C
  real(kind=8) :: YY_Coulomb_Ion
 
  ! check consistency with finite diffs calculation
  do l=0,5
  do m=-l,l
    
    ! call routine
    call YdY_Coulomb_C(b,r2,l2,m2,a,r1,l,m,d,r3,nx2,ny2,nz2,dS)
    
    ! check consistency with finite diff
    do i=1,3
    
      r1_p    = r1
      r1_m    = r1
      r1_p(i) = r1(i)+step/2.0d0
      r1_m(i) = r1(i)-step/2.0d0
      S1_p    = YY_Coulomb_C(b,r2,l2,m2,a,r1_p,l,m,d,r3,nx2,ny2,nz2)
      S1_m    = YY_Coulomb_C(b,r2,l2,m2,a,r1_m,l,m,d,r3,nx2,ny2,nz2)
      S1      =(S1_p-S1_m)/step

      write (*,'(A,I2,A,I2,A,E22.16,A,E22.16)')  & 
            'l=',l,'  m=',m,'  finite diff=',S1,'  YdY_Coulomb_C result=',dS(i)
        
      if ( 2.0* abs( ( S1 - dS(i) ) / ( S1 + dS(i) ) ) > 1.0d-4 .or. S1.ne.S1 ) then
        print *,'Error: expert <YdY> Two center Coulomb failed'
        print *,''
        print *,'test_derivatives failed'
        print *,''
        stop 1
      end if
    
    end do
    
  end do
  end do
  
  print *,''
  write (*,'(A)') '3 center Coulomb derivatives passed up to l=5'
  print *,''

  ! check consistency with finite diffs calculation
  do l=0,5
  do m=-l,l
    
    ! call routine
    call YdY_Coulomb_Ion(b,r2,l2,m2,a,r1,l,m,r3,dS)
    
    ! check consistency with finite diff
    do i=1,3
    
      r1_p    = r1
      r1_m    = r1
      r1_p(i) = r1(i)+step/2.0d0
      r1_m(i) = r1(i)-step/2.0d0
      S1_p    = YY_Coulomb_Ion(b,r2,l2,m2,a,r1_p,l,m,r3)
      S1_m    = YY_Coulomb_Ion(b,r2,l2,m2,a,r1_m,l,m,r3)
      S1      =(S1_p-S1_m)/step

      write (*,'(A,I2,A,I2,A,E22.16,A,E22.16)')  & 
            'l=',l,'  m=',m,'  finite diff=',S1,'  YdY_Coulomb_Ion result=',dS(i)
        
      if ( 2.0* abs( ( S1 - dS(i) ) / ( S1 + dS(i) ) ) > 1.0d-4 .or. S1.ne.S1 ) then
        print *,'Error: expert <YdY> Ionic integral failed'
        print *,''
        print *,'test_derivatives failed'
        print *,''
        stop 1
      end if
    
    end do
    
  end do
  end do
  
  print *,''
  write (*,'(A)') '2 center Ionic derivatives passed up to l=5'
  print *,''

  ! check consistency with finite diffs calculation
  do l=0,5
  do m=-l,l
    
    ! call routine
    call YY_Coulomb_dIon(b,r2,l2,m2,a,r1,l,m,r3,dS)
    
    ! check consistency with finite diff
    do i=1,3
    
      r1_p    = r3
      r1_m    = r3
      r1_p(i) = r3(i)+step/2.0d0
      r1_m(i) = r3(i)-step/2.0d0
      S1_p    = YY_Coulomb_Ion(b,r2,l2,m2,a,r1,l,m,r1_p)
      S1_m    = YY_Coulomb_Ion(b,r2,l2,m2,a,r1,l,m,r1_m)
      S1      =(S1_p-S1_m)/step

      write (*,'(A,I2,A,I2,A,E22.16,A,E22.16)')  & 
            'l=',l,'  m=',m,'  finite diff=',S1,'  YY_Coulomb_dIon result=',dS(i)
        
      if ( 2.0* abs( ( S1 - dS(i) ) / ( S1 + dS(i) ) ) > 1.0d-4 .or. S1.ne.S1 ) then
        print *,'Error: expert <YY> Ionic integral derivative failed'
        print *,''
        print *,'base value = ',S1_p,S1_m
        print *,''
        print *,'test_derivatives failed'
        print *,''
        stop 1
      end if
    
    end do
    
  end do
  end do
  
  print *,''
  write (*,'(A)') '2 center Ionic derivatives passed up to l=5'
  print *,''


  ! check consistency with finite diffs calculation
  do l=0,5
  do m=-l,l
    
    ! call routine
    call Y_Overlap_dY(b,r2,l2,m2,a,r1,l,m,dS)
    
    ! check consistency with finite diff
    do i=1,3
    
      r1_p    = r1
      r1_m    = r1
      r1_p(i) = r1(i)+step/2.0d0
      r1_m(i) = r1(i)-step/2.0d0
      S1_p    = Y_Overlap_Y(b,r2,l2,m2,a,r1_p,l,m)
      S1_m    = Y_Overlap_Y(b,r2,l2,m2,a,r1_m,l,m)
      S1      =(S1_p-S1_m)/step

      write (*,'(A,I2,A,I2,A,E22.16,A,E22.16)')  & 
            'l=',l,'  m=',m,'  finite diff=',S1,'  Y_Overlap_dY result=',dS(i)
        
      if ( 2.0* abs( ( S1 - dS(i) ) / ( S1 + dS(i) ) ) > 1.0d-4 .or. S1.ne.S1 ) then
        print *,'Error: expert <YdY> overlap integral failed'
        print *,''
        print *,'test_derivatives failed'
        print *,''
        stop 1
      end if
    
    end do
    
  end do
  end do
  
  print *,''
  write (*,'(A)') '2 center overlap derivatives passed up to l=5'
  print *,''

  ! check consistency with finite diffs calculation
  do l=0,5
  do m=-l,l
    
    ! call routine
    call Y_Laplacian_dY(b,r2,l2,m2,a,r1,l,m,dS)
    
    ! check consistency with finite diff
    do i=1,3
    
      r1_p    = r1
      r1_m    = r1
      r1_p(i) = r1(i)+step/2.0d0
      r1_m(i) = r1(i)-step/2.0d0
      S1_p    = Y_Laplacian_Y(b,r2,l2,m2,a,r1_p,l,m)
      S1_m    = Y_Laplacian_Y(b,r2,l2,m2,a,r1_m,l,m)
      S1      =(S1_p-S1_m)/step

      write (*,'(A,I2,A,I2,A,E22.16,A,E22.16)')  & 
            'l=',l,'  m=',m,'  finite diff=',S1,'  Y_Laplacian_dY result=',dS(i)
        
      if ( 2.0* abs( ( S1 - dS(i) ) / ( S1 + dS(i) ) ) > 1.0d-4 .or. S1.ne.S1 ) then
        print *,'Error: expert <YdY> laplacian integral failed'
        print *,''
        print *,'test_derivatives failed'
        print *,''
        stop 1
      end if
    
    end do
    
  end do
  end do
  
  print *,''
  write (*,'(A)') '2 center Laplacian derivatives passed up to l=5'
  print *,''


  
  print *,''
  write (*,'(A)') 'test_derivatives passed'
  print *,''
  
end program

