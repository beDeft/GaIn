clear

stacksize('max')

function [str]=clean_expression(str)
    
    // remove trailing blanks
    c_=ascii(str);
    i_=1;
    tab_="";
    while ( i_<size(c_,'*') & c_(i_)==32 )
      tab_=tab_+" ";
      i_=i_+1;
    end 
    
    // isolate comments
    [expr,op]=strsplit(str,["!"]);
    str=expr(1);
    
    comments="";
    for i=2:size(expr,1)
        comments=comments+op(i-1)+expr(i);
    end
    // remove eventuals blank
    str=strsubst(str,"= ","=");
    
    // remove useless 0
      // split string along basic known operations
      [expr,op]=strsplit(str,["+","-"]);
      // loop over components
      if (size(expr,1)>1) then
          for i=1:size(expr,1)
              if expr(i)=="0" then
                  expr(i)="";
                  if i>1 then
                      op(i-1)="";
                  end
              end
          end
      end
      // reconstruct expression
      str=expr(1);
      for i=2:size(expr,1)
          str=str+op(i-1)+expr(i);
      end
      
    // replace fractional exponents
    str=strsubst(str,"(1/2)","0.5")
    str=strsubst(str,"(3/2)","1.5")
    str=strsubst(str,"(5/2)","2.5")
    str=strsubst(str,"(7/2)","3.5")
    str=strsubst(str,"(9/2)","4.5")
    str=strsubst(str,"(11/2)","5.5")
    str=strsubst(str,"(13/2)","6.5")
    str=strsubst(str,"(15/2)","7.5")
    str=strsubst(str,"(17/2)","8.5")
    str=strsubst(str,"(19/2)","9.5")
    str=strsubst(str,"(21/2)","10.5")
    str=strsubst(str,"(23/2)","11.5")
    str=strsubst(str,"(25/2)","12.5")
    str=strsubst(str,"(27/2)","13.5")
    str=strsubst(str,"(29/2)","14.5")
    str=strsubst(str,"(31/2)","15.5")
    str=strsubst(str,"(33/2)","16.5")
    str=strsubst(str,"(35/2)","17.5")
    str=strsubst(str,"(37/2)","18.5")
    str=strsubst(str,"(39/2)","19.5")

    str=strsubst(str,"(-1/2)","-0.5")
    str=strsubst(str,"(-3/2)","-1.5")
    str=strsubst(str,"(-5/2)","-2.5")
    str=strsubst(str,"(-7/2)","-3.5")
    str=strsubst(str,"(-9/2)","-4.5")
    str=strsubst(str,"(-11/2)","-5.5")
    str=strsubst(str,"(-13/2)","-6.5")
    str=strsubst(str,"(-15/2)","-7.5")
    str=strsubst(str,"(-17/2)","-8.5")
    str=strsubst(str,"(-19/2)","-9.5")
    str=strsubst(str,"(-21/2)","-10.5")
    str=strsubst(str,"(-23/2)","-11.5")
    str=strsubst(str,"(-25/2)","-12.5")
    str=strsubst(str,"(-27/2)","-13.5")
    str=strsubst(str,"(-29/2)","-14.5")
    str=strsubst(str,"(-31/2)","-15.5")
    str=strsubst(str,"(-33/2)","-16.5")
    str=strsubst(str,"(-35/2)","-17.5")
    str=strsubst(str,"(-37/2)","-18.5")
    str=strsubst(str,"(-39/2)","-19.5")


    str=strsubst(str,"(-1)","-1")
    str=strsubst(str,"(-2)","-2")
    str=strsubst(str,"(-3)","-3")
    str=strsubst(str,"(-4)","-4")
    str=strsubst(str,"(-5)","-5")
    str=strsubst(str,"(-6)","-6")
    str=strsubst(str,"(-7)","-7")
    str=strsubst(str,"(-8)","-8")
    str=strsubst(str,"(-9)","-9")
    str=strsubst(str,"(-10)","-10")
    str=strsubst(str,"(-11)","-11")
    str=strsubst(str,"(-12)","-12")
    str=strsubst(str,"(-13)","-13")
    str=strsubst(str,"(-14)","-14")
    str=strsubst(str,"(-15)","-15")
    str=strsubst(str,"(-16)","-16")
    str=strsubst(str,"(-17)","-17")
    str=strsubst(str,"(-18)","-18")
    str=strsubst(str,"(-19)","-19")
    
    // special for laplacian routine
    str=strsubst(str,"(a1+a2)","_a1pa2_")
    str=strsubst(str,"(a2+a1)","_a1pa2_")
    
    // write pi correctly
    str=strsubst(str,"%pi","PI")
    
  if (0 ) then
    // expand exponents
    // split string along basic known operations
    str=strsubst(str,'^-','^negatif');
    [expr,op]=strsplit(str,["+","-","*","/","(",")","="]);
    // loop over components
    for i=1:size(expr,1)
        if strstr(expr(i),"^")~="" then
            // restore negative exponent
            expr(i)=strsubst(expr(i),'^negatif','^-');
            // split between number and exponent
            [parts,sep]=strsplit(expr(i),["^"]);
            // if this is an numeric exponent
            if isnum(parts(2)) & parts(2)~='i' then
                // unroll exponent
                exponent=eval(parts(2))
                expr(i)="";
                par_=""
                if exponent>0 then
                  while ( exponent > 1 )
                    expr(i)=expr(i)+parts(1)+"*";
                    exponent=exponent-1;
                  end
                else
                  if (exponent<-1) then
                    par_="("
                  end
                  if (i>1)&(op(i-1)=="*") then
                    op(i-1)="/"+par_;
                  else
                    expr(i)="1/"+par_;
                  end
                  while ( exponent < -1 )
                    expr(i)=expr(i)+parts(1)+"*";
                    exponent=exponent+1;
                  end
                end
                // terminate
                if abs(exponent)==1 then
                    expr(i)=expr(i)+parts(1);
                elseif abs(exponent)==1/2;
                    expr(i)=expr(i)+"sqrt("+parts(1)+")";
                else
                    disp(str)
                    disp("Error: unknown exponent in clean_expression")
                    return
                end
                if (par_=="(") then
                    expr(i)=expr(i)+")"
                end
            end
        end
    end
    // reconstruct expression
    str=expr(1);
    for i=2:size(expr,1)
        str=str+op(i-1)+expr(i);
    end
    
    else
    
    // expand exponents
    // split string along basic known operations
    str=strsubst(str,'^-','^negatif');
    [expr,op]=strsplit(str,["+","-","*","/","(",")","="]);
    // loop over components
    for i=1:size(expr,1)
        if strstr(expr(i),"^")~="" then
            // restore negative exponent
            expr(i)=strsubst(expr(i),'^negatif','^-');
            // split between number and exponent
            [parts,sep]=strsplit(expr(i),["^"]);
            // if this is an numeric exponent
            if isnum(parts(2)) & parts(2)~='i' then
                // unroll exponent
                exponent=eval(parts(2))
                expr(i)="";
                par_=""
                if exponent>0 then
                    if exponent>1 then
                      expr(i)=parts(1)+"**__tmp_exp__"+string(int(exponent));
                      exponent=exponent-int(exponent);
                      if exponent>0 then
                        expr(i)=expr(i)+"*"
                      end
                    end
                else
                  if (exponent<-1) then
                    par_="("
                  end
                  if (i>1)&(op(i-1)=="*") then
                    op(i-1)="/"+par_;
                  else
                    expr(i)="1/"+par_;
                  end
                  if exponent<=-1 then
                    if int(abs(exponent))==1 then
                      expr(i)=expr(i)+parts(1)
                    else
                      expr(i)=expr(i)+parts(1)+"**__tmp_exp__"+string(int(abs(exponent)));
                    end
                    exponent=exponent+int(abs(exponent));
                    if exponent<0 then
                      expr(i)=expr(i)+"*"
                    end
                  end 
//                  while ( exponent < -1 )
//                   expr(i)=expr(i)+parts(1)+"*";
//                    exponent=exponent+1;
//                  end
                end
                // terminate
//                if abs(exponent)==1 then
//                    expr(i)=expr(i)+parts(1);
//                elseif abs(exponent)==1/2;
                if abs(exponent)==1/2
                    expr(i)=expr(i)+"sqrt("+parts(1)+")";
                elseif exponent==0;
                  // do nothing
                else
                    disp(exponent)
                    disp("Error: unknown exponent in clean_expression")
                    return
                end
                if (par_=="(") then
                    expr(i)=expr(i)+")"
                end
            end
        end
    end
    // reconstruct expression
    str=expr(1);
    for i=2:size(expr,1)
        str=str+op(i-1)+expr(i);
    end
    
  end
  
    // set to fortran format
    // split string along basic known operations
    [expr,op]=strsplit(str,["+","-","*","/","(",")","="]);
    // loop over components
    for i=1:size(expr,1)
        expr(i)=stripblanks(expr(i));
        if isnum(expr(i))  & isdigit(expr(i)) & int(eval(expr(i)))==eval(expr(i)) then
            if (i==1)|((strstr(expr(i-1),"dimension")=="")&(strstr(expr(i-1),"kind")==""))  then
               expr(i)=sprintf("%s.0d0",expr(i));
            end
        end
    end
    // reconstruct expression
    str=expr(1);
    for i=2:size(expr,1)
        str=str+op(i-1)+expr(i);
    end
    
    // remove exponents tags
    str=strsubst(str,'__tmp_exp__','');
    
    // substitute []
    str=strsubst(str,'[','(');
    str=strsubst(str,']',')');
    
    // special for laplacian routine
    str=strsubst(str,"_a1pa2_","(a1+a2)")
    
    // append comment
    str=tab_+str+comments;
    
endfunction

test="c[4]=a^2*y2*z1*(2*a*z1^2+3) ";
test="  c[50]=3/3380677604*5^(1/2)*7^(1/2)*11^-(3/2)*13^-(1/2)*%pi^-(1/2)*(7320378049*y2+428701272*y1) ";
//test="  S[5] = pref * ( 2^-(17/2)*3^(3/2)*5^(1/2)*7^(1/2)*11^(1/2)*13^(1/2)*17^(1/2)*%pi^(1/2)*alpha^-(7/2)*(4*alpha*y1*((2*alpha*x1*x2+1)*y1^6+x1^2*(x1^2*(7*(2*alpha*x1*x2+5)*y1^2-x1^2*(2*alpha*x1*x2+7))-7*(2*alpha*x1*x2+3)*y1^4))*z2^2+y2*(y1*(7*(alpha*x1^2*(3*y1^4+x1^2*(x1^2-(2*alpha*x1*x2+5)*y1^2))*y2+y1^4*(2*alpha^2*x1^3*x2*y2-(2*alpha*x1*x2+1)*y1))-alpha*y1^6*y2)+x1*(x1*(y1*(2*alpha^2*x1^5*x2*y2+35*(2*alpha*x1*x2+3)*y1^3)+x1^2*(x1^2*(2*alpha*x1*x2+7)-21*(2*alpha*x1*x2+5)*y1^2))-2*alpha^2*x2*y1^7*y2))+x2*y1*(x1*(7*(2*alpha^2*x1^2*x2^2+3*(3*alpha*x1*x2+2))*y1^4+x1^2*(x1^2*(2*alpha^2*x1^2*x2^2+21*(alpha*x1*x2+2))-7*(2*alpha^2*x1^2*x2^2+5*(3*alpha*x1*x2+4))*y1^2))-alpha*x2*(2*alpha*x1*x2+3)*y1^6)) )"

//test="c[81]=-45*2^-(21/2)*%pi^-1*a^-7*(a*(12*(3*z2^2+z1*(6*z2+z1))-y2*(27*y2+62*y1)+3*(3*(x1^2-x2^2)-7*y1^2))+5) "
disp(test)
disp(clean_expression(test))

//return

input_files=[
//"up_to_g/base_files/YlmTransforms.f90"
//"up_to_g/base_files/OverlapUtils2.f90"
//"up_to_g/base_files/SolHarConversionUtils.f90"
//"up_to_g/base_files/D_X_Ion_onsite.f90"
//"up_to_g/base_files/D_X_Ion_offsite.f90"
//"up_to_g/base_files/rlYlmOverlap.f90"
//"up_to_h/base_files/rlYlmLaplacian.f90"
//"up_to_h/base_files/CubHarExpansionUtils.f90"
//"up_to_h/base_files/SolHarExpansionUtils.f90"
//"up_to_g/base_files/D_X_D_offsite.f90"
//"up_to_g/base_files/D_X_D_onsite_new.f90"
//"up_to_g/base_files/Derivative.f90"
//"up_to_g/base_files/Laplacian.f90"
//"up_to_g/base_files/rlYlmOverlap_large.f90"
//"QuadOnExponentialUtils.f90"
//"up_to_h/base_files/CenteredPowerExpansion.f90"
//"up_to_h/base_files/R_from_Y.f90"
//"up_to_h_new/base_files/R_Laplacian.f90"
//"up_to_h_new/base_files/CubicHarmonicProduct.f90"
//"up_to_h_new/base_files/Y_Value.f90"
//"up_to_h_new/base_files/TestCoulombUtil.f90"
"tmp.f90"
]

output_files=[
//"up_to_g/YlmTransforms.f90"
//"up_to_g/OverlapUtils2.f90"
//"up_to_g/SolHarConversionUtils.f90"
//"up_to_g/D_X_Ion_onsite.f90"
//"up_to_g/D_X_Ion_offsite.f90"
//"up_to_g/rlYlmOverlap.f90"
//"up_to_h/rlYlmLaplacian.f90"
//"up_to_h/CubHarExpansionUtils.f90"
//"up_to_h/SolHarExpansionUtils.f90"
//"up_to_g/D_X_D_offsite.f90"
//"up_to_g/D_X_D_onsite_new.f90"
//"up_to_g/Derivative.f90"
//"up_to_g/Laplacian.f90"
//"up_to_g/rlYlmOverlap_large.f90"
//"QuadOnExponentialUtils.f90"
//"up_to_h/CenteredPowerExpansion.f90"
//"up_to_h/R_from_Y.f90"
//"up_to_h_new/R_Laplacian.f90"
//"up_to_h_new/CubicHarmonicProduct.f90"
//"up_to_h_new/Y_Value.f90"
//"up_to_h_new/TestCoulombUtil.f90"
"tmp_clean.f90"
]

for ifile=1:1

  txt=mgetl(input_files(ifile))
  txt_clean=""
  for i=1:size(txt,1)
    txt_clean(i)=clean_expression(txt(i));
  end
  mputl(txt_clean,output_files(ifile));

end
