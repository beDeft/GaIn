clear

//stacksize('max')

function [folded_str]=fold_expression(str,length_max)
    
    // isolate comments
    [expr,op]=strsplit(str,["!"]);
    str=expr(1);
    
    comments="";
    for i=2:size(expr,1)
        comments=comments+op(i-1)+expr(i);
    end
    
    // remove eventuals blank
    str=strsubst(str,"= ","=");
    
    // set PI as a constant
    str=strsubst(str,"real(kind=8), parameter :: PI=4.0d0*atan(1.0d0)","real(kind=8), parameter :: PI=3.1415926535897932384626433832795d0");
    
    // remove useless 0
    folded_str=[];
    [expr,op]=strsplit(str,["+","-","(",",",")"]);
    // loop over components
    i=1
    folded_str(1)=expr(i);
    while ( i<size(expr,1) )
      // reconstruct line
      while ( i<size(expr,1) & length(folded_str($)+op(i)+expr(i+1)+" &")<=length_max )
        folded_str($)=folded_str($)+op(i)+expr(i+1);
        i=i+1;
      end
      // go to next line if needed
      if ( i<size(expr,1) ) then
        folded_str($)=folded_str($)+" &";
        folded_str=[folded_str;""];
      end 
    end
    folded_str($)=folded_str($)+comments;
    
endfunction


input_files=[
//"../src/CenteredPowerExpansion.f90"
//"../src/Coulomb.f90"
//"../src/CoulombUtil_new.f90"
//"../src/CubicHarmonicProduct.f90"
//"../src/Laplacian.f90"
//"../src/Overlap.f90"
//"../src/R_1_norm.f90"
//"../src/R_from_Y.f90"
//"../src/R_Laplacian.f90"
//"../src/R_to_D_Conversion.f90"
//"../src/XYZ_power_to_ir.f90"
//"../src/Y_Value.f90"
"/home/sp2m_l_sim/duchemin/GaIn-master/src/rlYlmLaplacian.f90"
];

output_files=[
//"../folded_src/CenteredPowerExpansion.f90"
//"../folded_src/Coulomb.f90"
//"../folded_src/CoulombUtil.f90"
//"../folded_src/CubicHarmonicProduct.f90"
//"../folded_src/Laplacian.f90"
//"../folded_src/Overlap.f90"
//"../folded_src/R_1_norm.f90"
//"../folded_src/R_from_Y.f90"
//"../folded_src/R_Laplacian.f90"
//"../folded_src/R_to_D_Conversion.f90"
//"../folded_src/XYZ_power_to_ir.f90"
//"../folded_src/Y_Value.f90"
"/home/sp2m_l_sim/duchemin/GaIn-master/src/rlYlmLaplacian_folded.f90"
];

for ifile=1:size(input_files,1)

  txt=mgetl(input_files(ifile))
  txt_clean=""
  for i=1:size(txt,1)
    tmp_clean=fold_expression(txt(i),72);
    for j=1:size(tmp_clean,1)
      txt_clean($+1)=tmp_clean(j);
    end
  end
  mputl(txt_clean,output_files(ifile));

end
