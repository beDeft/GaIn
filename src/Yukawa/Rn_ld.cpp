#include "Rn_defs.h"
#include "Rn.h"

//> explicit instanciation of proxy for Yukawa R functions evaluation 
///
/// can be either called as 
///
///  Yukawa_weighted_R<order>(a, k, d)
///
/// or 
///
///  Yukawa_weighted_R(a, k, d, order)
///
template std::complex<long double> Yukawa_R(long double a, std::complex<long double> k, long double d, int order);

