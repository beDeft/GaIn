#ifndef MULTIPRECISION_H 
#define MULTIPRECISION_H 

#include <complex>
#define _Complex_I // to have access to cast functions for mpc_t numbers 

#include <mpfr.h>
#include <mpc.h>
#include <type_traits>

template <size_t N>
class Real
{
  private:
  
                     mpfr_t     r_;
    static constexpr mpfr_rnd_t rnd_=MPFR_RNDN;
    
  public:

    ///! empty constructor
    ///
    Real(void) { mpfr_init2 (r_, N);  mpfr_set_zero (r_, 1); }

    ///! init constructor
    ///   
    Real(const mpfr_t &r)                    { mpfr_init2(r_, N); mpfr_set_fr  (r_, r, rnd_); }
    Real(const int &r)                       { mpfr_init2(r_, N); mpfr_set_si  (r_, (long int)r, rnd_); }
    Real(const long int &r)                  { mpfr_init2(r_, N); mpfr_set_si  (r_, r, rnd_); }
    Real(const unsigned long int &r)         { mpfr_init2(r_, N); mpfr_set_ui  (r_, r, rnd_); }
    Real(const float &r)                     { mpfr_init2(r_, N); mpfr_set_flt (r_, r, rnd_); }
    Real(const double &r)                    { mpfr_init2(r_, N); mpfr_set_d   (r_, r, rnd_); }
    Real(const long double &r)               { mpfr_init2(r_, N); mpfr_set_ld  (r_, r, rnd_); }
    
    /// copy constructor
    template <size_t M>
    Real(const Real<M> &other) { mpfr_init2(r_, N); mpfr_set_fr(r_,other.r_,rnd_); }
    Real(const Real<N> &other) { mpfr_init2(r_, N); mpfr_set_fr(r_,other.r_,rnd_); }
    
    /// move constructor
    Real(Real<N> &&other) { mpfr_init2(r_, N); mpfr_swap(r_,other.r_); }
    
    /// assignment operators
    Real<N>& operator= (      Real<N> &&other) { mpfr_swap  (r_,other.r_);      return *this; }
    template <size_t M>
    Real<N>& operator= (const Real<M>  &other) { mpfr_set_fr(r_,other.r_,rnd_); return *this; }
    Real<N>& operator= (const Real<N>  &other) { mpfr_set_fr(r_,other.r_,rnd_); return *this; }
    
    /// assignment of arithmetic type to multiprecision Real
    template <class TYPE>
    // SFINAE on return type to discard Real<N> + Real<M>
    typename std::enable_if<std::is_arithmetic<TYPE>::value,Real<N>&>::type
    // addition operator definition: call divide class method
    operator= (const TYPE &a) { return ( *this = Real<N>(a)); }
    
    // cast 
    operator float()       const { return mpfr_get_flt     (r_,rnd_); }
    operator double()      const { return mpfr_get_d       (r_,rnd_); }
    operator long double() const { return mpfr_get_ld      (r_,rnd_); }
    
    /// member access
          mpfr_t&    value(void)       { return r_;   }  
    const mpfr_t&    value(void) const { return r_;   }  
    const mpfr_rnd_t rnd  (void) const { return rnd_; }
     
    /// arithmetic function
    ///
    /// inplace addition
    ///
    template <size_t M> 
    Real<N>& operator+=(const Real<M>           &other) { mpfr_add   (r_,r_,other.r_,rnd_);    return *this; }
    Real<N>& operator+=(const mpfr_t            &r)     { mpfr_add   (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator+=(const double            &r)     { mpfr_add_d (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator+=(const int               &r)     { mpfr_add_si(r_,r_,(long int)r,rnd_); return *this; }
    Real<N>& operator+=(const long int          &r)     { mpfr_add_si(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator+=(const long unsigned int &r)     { mpfr_add_ui(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator+=(const long double       &r)     { *this+=Real<N>(r);                   return *this; }
    ///
    /// inplace substraction
    ///
    template <size_t M> 
    Real<N>& operator-=(const Real<M>           &other) { mpfr_sub   (r_,r_,other.r_,rnd_);    return *this; }
    Real<N>& operator-=(const mpfr_t            &r)     { mpfr_sub   (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator-=(const double            &r)     { mpfr_sub_d (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator-=(const int               &r)     { mpfr_sub_si(r_,r_,(long int)r,rnd_); return *this; }
    Real<N>& operator-=(const long int          &r)     { mpfr_sub_si(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator-=(const long unsigned int &r)     { mpfr_sub_ui(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator-=(const long double       &r)     { *this-=Real<N>(r);                   return *this; }
    ///
    /// inplace multiplication
    ///
    template <size_t M> 
    Real<N>& operator*=(const Real<M>           &other) { mpfr_mul   (r_,r_,other.r_,rnd_);    return *this; }
    Real<N>& operator*=(const mpfr_t            &r)     { mpfr_mul   (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator*=(const double            &r)     { mpfr_mul_d (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator*=(const int               &r)     { mpfr_mul_si(r_,r_,(long int)r,rnd_); return *this; }
    Real<N>& operator*=(const long int          &r)     { mpfr_mul_si(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator*=(const long unsigned int &r)     { mpfr_mul_si(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator*=(const long double       &r)     { *this*=Real<N>(r);                   return *this; }
    ///
    /// inplace division
    ///
    template <size_t M> 
    Real<N>& operator/=(const Real<M>           &other) { mpfr_div   (r_,r_,other.r_,rnd_);    return *this; }
    Real<N>& operator/=(const mpfr_t            &r)     { mpfr_div   (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator/=(const double            &r)     { mpfr_div_d (r_,r_,r,rnd_);           return *this; }
    Real<N>& operator/=(const int               &r)     { mpfr_div_si(r_,r_,(long int)r,rnd_); return *this; }
    Real<N>& operator/=(const long int          &r)     { mpfr_div_si(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator/=(const long unsigned int &r)     { mpfr_div_ui(r_,r_,r,rnd_);           return *this; }
    Real<N>& operator/=(const long double       &r)     { *this/=Real<N>(r);                   return *this; }
    
    /// unary minus
    Real<N> operator- () const { Real<N> c; mpfr_neg(c.r_,r_,rnd_); return c; }
    
    /// addition 
    template <class TYPE>
    Real<N> operator+ (const TYPE &num) const { Real<N> c(*this); return c+=num; }

    /// substraction 
    template <class TYPE>
    Real<N> operator- (const TYPE &num) const { Real<N> c(*this); return c-=num; }

    /// multiplication 
    template <class TYPE>
    Real<N> operator* (const TYPE &num) const { Real<N> c(*this); return c*=num; }

    /// division 
    template <class TYPE>
    Real<N> operator/ (const TYPE &num) const { Real<N> c(*this); return c/=num; }

    ///
    /// num-*this 
    ///
    template <size_t M> 
    Real<std::max(M,N)> substract_to(const Real<M>           &other) const { Real<std::max(M,N)> result; mpfr_sub   (result.r_,other.r_,r_,rnd_);    return result; }
    Real<N>             substract_to(const mpfr_t            &r)     const { Real<N> result;             mpfr_sub   (result.r_,r,r_,rnd_);           return result; }
    Real<N>             substract_to(const double            &r)     const { Real<N> result;             mpfr_d_sub (result.r_,r,r_,rnd_);           return result; }
    Real<N>             substract_to(const int               &r)     const { Real<N> result;             mpfr_si_sub(result.r_,(long int)r,r_,rnd_); return result; }
    Real<N>             substract_to(const long int          &r)     const { Real<N> result;             mpfr_si_sub(result.r_,r,r_,rnd_);           return result; }
    Real<N>             substract_to(const long unsigned int &r)     const { Real<N> result;             mpfr_ui_sub(result.r_,r,r_,rnd_);           return result; }
    Real<N>             substract_to(const long double       &r)     const { return  this->divide(Real<N>(r)); }

    ///
    /// num/*this 
    ///
    template <size_t M> 
    Real<std::max(M,N)> divide(const Real<M>           &other) const { Real<std::max(M,N)> result; mpfr_div   (result.r_,other.r_,r_,rnd_);    return result; }
    Real<N>             divide(const mpfr_t            &r)     const { Real<N> result;             mpfr_div   (result.r_,r,r_,rnd_);           return result; }
    Real<N>             divide(const double            &r)     const { Real<N> result;             mpfr_d_div (result.r_,r,r_,rnd_);           return result; }
    Real<N>             divide(const int               &r)     const { Real<N> result;             mpfr_si_div(result.r_,(long int)r,r_,rnd_); return result; }
    Real<N>             divide(const long int          &r)     const { Real<N> result;             mpfr_si_div(result.r_,r,r_,rnd_);           return result; }
    Real<N>             divide(const long unsigned int &r)     const { Real<N> result;             mpfr_ui_div(result.r_,r,r_,rnd_);           return result; }
    Real<N>             divide(const long double       &r)     const { return  this->divide(Real<N>(r)); }
    
    // projections 
    Real<N> real (void) const { return Real<N>(*this); }   
    Real<N> conj (void) const { return Real<N>(*this); }   
    Real<N> imag (void) const { return Real<N>(); }   
    Real<N> arg  (void) const { return Real<N>(); }   
    Real<N> floor(void) const { return Real<N>(mpfr_get_si(r_, MPFR_RNDD)); } 
    Real<N> ceil (void) const { return Real<N>(mpfr_get_si(r_, MPFR_RNDU)); } 
    
    
    /// functions
    ///
    Real<N> sqrt (void)                const { Real<N> result; mpfr_sqrt(result.r_,r_,rnd_);       return result; }
    Real<N> abs  (void)                const { Real<N> result; mpfr_abs(result.r_,r_,rnd_);        return result; }
    Real<N> log  (void)                const { Real<N> result; mpfr_log(result.r_,r_,rnd_);        return result; }
    Real<N> log2 (void)                const { Real<N> result; mpfr_log2(result.r_,r_,rnd_);       return result; }
    Real<N> log10(void)                const { Real<N> result; mpfr_log10(result.r_,r_,rnd_);      return result; }
    Real<N> log1p(void)                const { Real<N> result; mpfr_log1p(result.r_,r_,rnd_);      return result; }
    Real<N> exp  (void)                const { Real<N> result; mpfr_exp(result.r_,r_,rnd_);        return result; }
    Real<N> exp2 (void)                const { Real<N> result; mpfr_exp2(result.r_,r_,rnd_);       return result; }
    Real<N> exp10(void)                const { Real<N> result; mpfr_exp10(result.r_,r_,rnd_);      return result; }
    Real<N> expm1(void)                const { Real<N> result; mpfr_expm1(result.r_,r_,rnd_);      return result; }
    Real<N> cos  (void)                const { Real<N> result; mpfr_cos(result.r_,r_,rnd_);        return result; }
    Real<N> sin  (void)                const { Real<N> result; mpfr_sin(result.r_,r_,rnd_);        return result; }
    Real<N> tan  (void)                const { Real<N> result; mpfr_tan(result.r_,r_,rnd_);        return result; }
    Real<N> sec  (void)                const { Real<N> result; mpfr_sec(result.r_,r_,rnd_);        return result; }
    Real<N> csc  (void)                const { Real<N> result; mpfr_csc(result.r_,r_,rnd_);        return result; }
    Real<N> cot  (void)                const { Real<N> result; mpfr_cot(result.r_,r_,rnd_);        return result; }
    Real<N> acos (void)                const { Real<N> result; mpfr_acos(result.r_,r_,rnd_);       return result; }
    Real<N> asin (void)                const { Real<N> result; mpfr_asin(result.r_,r_,rnd_);       return result; }
    Real<N> atan (void)                const { Real<N> result; mpfr_atan(result.r_,r_,rnd_);       return result; }
    Real<N> cosh (void)                const { Real<N> result; mpfr_cosh(result.r_,r_,rnd_);       return result; }
    Real<N> sinh (void)                const { Real<N> result; mpfr_sinh(result.r_,r_,rnd_);       return result; }
    Real<N> tanh (void)                const { Real<N> result; mpfr_tanh(result.r_,r_,rnd_);       return result; }
    Real<N> sech (void)                const { Real<N> result; mpfr_sech(result.r_,r_,rnd_);       return result; }
    Real<N> csch (void)                const { Real<N> result; mpfr_csch(result.r_,r_,rnd_);       return result; }
    Real<N> coth (void)                const { Real<N> result; mpfr_coth(result.r_,r_,rnd_);       return result; }
    Real<N> acosh(void)                const { Real<N> result; mpfr_acosh(result.r_,r_,rnd_);      return result; }
    Real<N> asinh(void)                const { Real<N> result; mpfr_asinh(result.r_,r_,rnd_);      return result; }
    Real<N> atanh(void)                const { Real<N> result; mpfr_atanh(result.r_,r_,rnd_);      return result; }
    Real<N> erf  (void)                const { Real<N> result; mpfr_erf  (result.r_,r_,rnd_);      return result; }
    Real<N> erfc (void)                const { Real<N> result; mpfr_erfc (result.r_,r_,rnd_);      return result; }
    
    Real<N> eint     (void)             const { Real<N> result; mpfr_eint   (result.r_,r_,rnd_);      return result; }
    Real<N> li2      (void)             const { Real<N> result; mpfr_li2    (result.r_,r_,rnd_);      return result; }
    Real<N> gamma    (void)             const { Real<N> result; mpfr_gamma  (result.r_,r_,rnd_);      return result; }
    Real<N> lngamma  (void)             const { Real<N> result; mpfr_lngamma(result.r_,r_,rnd_);      return result; }
    Real<N> digamma  (void)             const { Real<N> result; mpfr_digamma(result.r_,r_,rnd_);      return result; }
    Real<N> zeta     (void)             const { Real<N> result; mpfr_zeta   (result.r_,r_,rnd_);      return result; }
    Real<N> j0       (void)             const { Real<N> result; mpfr_j0     (result.r_,r_,rnd_);      return result; }
    Real<N> j1       (void)             const { Real<N> result; mpfr_j1     (result.r_,r_,rnd_);      return result; }
    Real<N> y0       (void)             const { Real<N> result; mpfr_y0     (result.r_,r_,rnd_);      return result; }
    Real<N> y1       (void)             const { Real<N> result; mpfr_y1     (result.r_,r_,rnd_);      return result; }
    Real<N> ai       (void)             const { Real<N> result; mpfr_ai     (result.r_,r_,rnd_);      return result; }

    // two args functions
    Real<N> root     (const unsigned long int &n) const { Real<N> result; mpfr_rootn_ui(result.r_,r_,n,rnd_); return result; }
    Real<N> jn       (const long int          &n) const { Real<N> result; mpfr_jn      (result.r_,n,r_,rnd_); return result; }
    Real<N> yn       (const long int          &n) const { Real<N> result; mpfr_yn      (result.r_,n,r_,rnd_); return result; }
    Real<N> lgamma   (const int               &n) const { Real<N> result; mpfr_lgamma  (result.r_,n,r_,rnd_); return result; }
    template <size_t M> 
    Real<N> gamma_inc(const Real<M>  &r) const { Real<N> result; mpfr_gamma_inc(result.r_,r_,r.r_,rnd_);      return result; }
    template <size_t M> 
    Real<N> beta     (const Real<M>  &r) const { Real<N> result; mpfr_beta     (result.r_,r_,r.r_,rnd_);      return result; }
    template <size_t M> 
    Real<N> agm      (const Real<M>  &r) const { Real<N> result; mpfr_agm      (result.r_,r_,r.r_,rnd_);      return result; }
    template <size_t M> 
    Real<N> atan2    (const Real<M>  &r) const { Real<N> result; mpfr_atan2    (result.r_,r_,r.r_,rnd_);      return result; }
     
    // power
    template <size_t M> 
    Real<N> pow(const Real<M>           &r) const { Real<N> result; mpfr_pow(result.r_,r_,r.r_,rnd_);           return result; } 
    Real<N> pow(const mpfr_t            &r) const { Real<N> result; mpfr_pow(result.r_,r_,r,rnd_);              return result; } 
    Real<N> pow(const int               &r) const { Real<N> result; mpfr_pow_si(result.r_,r_,(long int)r,rnd_); return result; }  
    Real<N> pow(const long int          &r) const { Real<N> result; mpfr_pow_si(result.r_,r_,r,rnd_);           return result; }  
    Real<N> pow(const long unsigned int &r) const { Real<N> result; mpfr_pow_ui(result.r_,r_,r,rnd_);           return result; }  
    
    /// comparison  
    template <size_t M> 
    int compare(const Real<M>           &other) const { return mpfr_cmp(r_,other.r_); }
    int compare(const mpfr_t            &r)     const { return mpfr_cmp(r_,r); }
    int compare(const double            &r)     const { return mpfr_cmp_d(r_,r); }
    int compare(const long double       &r)     const { return mpfr_cmp_ld(r_,r); }
    int compare(const int               &r)     const { return mpfr_cmp_si(r_,(long int)r); }
    int compare(const long int          &r)     const { return mpfr_cmp_si(r_,r); }
    int compare(const unsigned long int &r)     const { return mpfr_cmp_ui(r_,r); }
    
    template <class TYPE>
    bool operator< (const TYPE &other) const { return this->compare(other)<0; } 
    template <class TYPE>
    bool operator> (const TYPE &other) const { return this->compare(other)>0; } 
    template <class TYPE>
    bool operator==(const TYPE &other) const { return this->compare(other)==0; } 
    template <class TYPE>
    bool operator!=(const TYPE &other) const { return this->compare(other)!=0; } 
    template <class TYPE>
    bool operator<=(const TYPE &other) const { return this->compare(other)<=0; } 
    template <class TYPE>
    bool operator>=(const TYPE &other) const { return this->compare(other)>=0; } 
    
    // static methods for named constant
    static constexpr Real<N> pi     (void) { Real<N> result; mpfr_const_pi     (result.r_,rnd_); return result; }
    static constexpr Real<N> euler  (void) { Real<N> result; mpfr_const_euler  (result.r_,rnd_); return result; }
    static constexpr Real<N> catalan(void) { Real<N> result; mpfr_const_catalan(result.r_,rnd_); return result; }
    
    /// destructor
    ///
    ~Real(void)
    {
      mpfr_clear (r_);
    }
  
  // all Real are friends, regardless of precision...
  template <size_t M>
  friend class Real;  
};

/// addition of arithmetic type to multiprecision Real
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,Real<N>>::type
// addition operator definition: call divide class method
operator+ (const TYPE &a, const Real<N> &b) { Real<N> c(b); return c+=a; }

/// substraction of multiprecision Real to arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,Real<N>>::type
// substraction operator definition: call substract_to class method
operator- (const TYPE &a, const Real<N> &b) { return b.substract_to(a); }

/// multiplication of arithmetic type with multiprecision Real
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,Real<N>>::type
// addition operator definition: call divide class method
operator* (const TYPE &a, const Real<N> &b) { Real<N> c(b); return c*=a; }

/// division of arithmetic type by multiprecision Real
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> / Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,Real<N>>::type
// division operatr definition: call divide class method
operator/ (const TYPE &num, const Real<N> &den) { return den.divide(num); }

/// comparison with arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,bool>::type
// comparison operator definition: call compare class method
operator<  (const TYPE &a, const Real<N> &b) { return !(b>=a); } 

/// comparison with arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,bool>::type
// comparison operator definition: call compare class method
operator>  (const TYPE &a, const Real<N> &b) { return !(b<=a); } 

/// comparison with arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,bool>::type
// comparison operator definition: call compare class method
operator<= (const TYPE &a, const Real<N> &b) { return !(b>a); } 

/// comparison with arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,bool>::type
// comparison operator definition: call compare class method
operator>= (const TYPE &a, const Real<N> &b) { return !(b<a); } 

/// comparison with arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,bool>::type
// comparison operator definition: call compare class method
operator== (const TYPE &a, const Real<N> &b) { return b==a; } 

/// comparison with arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Real<N> + Real<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value,bool>::type
// comparison operator definition: call compare class method
operator!= (const TYPE &a, const Real<N> &b) { return b!=a; } 

/// single arguments functions
///
template <size_t N>
Real<N> floor(const Real<N> &r) { return r.floor(); }
template <size_t N>
Real<N> ceil (const Real<N> &r) { return r.ceil(); }
template <size_t N>
Real<N> real (const Real<N> &r) { return r.real(); }
template <size_t N>
Real<N> conj (const Real<N> &r) { return r.conj(); }
template <size_t N>
Real<N> imag (const Real<N> &r) { return r.imag(); }
template <size_t N>
Real<N> arg  (const Real<N> &r) { return r.arg(); }
template <size_t N>
Real<N> sqrt (const Real<N> &r) { return r.sqrt(); }
template <size_t N>
Real<N> abs  (const Real<N> &r) { return r.abs(); }
template <size_t N>
Real<N> log  (const Real<N> &r) { return r.log(); }
template <size_t N>
Real<N> log2 (const Real<N> &r) { return r.log2(); }
template <size_t N>
Real<N> log10(const Real<N> &r) { return r.log10(); }
template <size_t N>
Real<N> log1p(const Real<N> &r) { return r.log1p(); }
template <size_t N>
Real<N> exp  (const Real<N> &r) { return r.exp(); }
template <size_t N>
Real<N> exp2 (const Real<N> &r) { return r.exp2(); }
template <size_t N>
Real<N> exp10(const Real<N> &r) { return r.exp10(); }
template <size_t N>
Real<N> expm1(const Real<N> &r) { return r.expm1(); }
template <size_t N>
Real<N> cos  (const Real<N> &r) { return r.cos(); }
template <size_t N>
Real<N> sin  (const Real<N> &r) { return r.sin(); }
template <size_t N>
Real<N> tan  (const Real<N> &r) { return r.tan(); }
template <size_t N>
Real<N> sec  (const Real<N> &r) { return r.sec(); }
template <size_t N>
Real<N> csc  (const Real<N> &r) { return r.csc(); }
template <size_t N>
Real<N> cot  (const Real<N> &r) { return r.cot(); }
template <size_t N>
Real<N> acos (const Real<N> &r) { return r.acos(); }
template <size_t N>
Real<N> asin (const Real<N> &r) { return r.asin(); }
template <size_t N>
Real<N> atan (const Real<N> &r) { return r.atan(); }
template <size_t N>
Real<N> cosh (const Real<N> &r) { return r.cosh(); }
template <size_t N>
Real<N> sinh (const Real<N> &r) { return r.sinh(); }
template <size_t N>
Real<N> tanh (const Real<N> &r) { return r.tanh(); }
template <size_t N>
Real<N> sech (const Real<N> &r) { return r.sech(); }
template <size_t N>
Real<N> csch (const Real<N> &r) { return r.csch(); }
template <size_t N>
Real<N> coth (const Real<N> &r) { return r.coth(); }
template <size_t N>
Real<N> acosh(const Real<N> &r) { return r.acosh(); }
template <size_t N>
Real<N> asinh(const Real<N> &r) { return r.asinh(); }
template <size_t N>
Real<N> atanh(const Real<N> &r) { return r.atanh(); }
template <size_t N>
Real<N> erf  (const Real<N> &r) { return r.erf(); }
template <size_t N>
Real<N> erfc (const Real<N> &r) { return r.erfc(); }
template <size_t N>
Real<N> eint     (const Real<N> &r) { return r.eint(); }
template <size_t N>
Real<N> li2      (const Real<N> &r) { return r.li2(); }
template <size_t N>
Real<N> gamma    (const Real<N> &r) { return r.gamma(); }
template <size_t N>
Real<N> lngamma  (const Real<N> &r) { return r.lngamma(); }
template <size_t N>
Real<N> digamma  (const Real<N> &r) { return r.digamma(); }
template <size_t N>
Real<N> zeta     (const Real<N> &r) { return r.zeta(); }
template <size_t N>
Real<N> j0       (const Real<N> &r) { return r.j0(); }
template <size_t N>
Real<N> j1       (const Real<N> &r) { return r.j1(); }
template <size_t N>
Real<N> y0       (const Real<N> &r) { return r.y0(); }
template <size_t N>
Real<N> y1       (const Real<N> &r) { return r.y1(); }
template <size_t N>
Real<N> ai       (const Real<N> &r) { return r.ai(); }

// two args functions
template <size_t N>
Real<N> root     (const Real<N> &r, const unsigned long int &n) { return r.root(n); }
template <size_t N>
Real<N> jn       (const long int &n, const Real<N> &r) { return r.jn(n); }
template <size_t N>
Real<N> yn       (const long int &n, const Real<N> &r) { return r.yn(n); }
template <size_t N>
Real<N> lgamma   (const int      &n, const Real<N> &r) { return r.lgamma(n); }
template <size_t N, size_t M> 
Real<N> gamma_inc(const Real<N>  &r1, const Real<M> &r2) { return r1.gamma_inc(r2); }
template <size_t N, size_t M> 
Real<N> beta     (const Real<N>  &r1, const Real<M> &r2) { return r1.beta(r2); }
template <size_t N, size_t M> 
Real<N> agm      (const Real<N>  &r1, const Real<M> &r2) { return r1.agm(r2); }
template <size_t N, size_t M> 
Real<N> atan2    (const Real<N>  &r1, const Real<M> &r2) { return r1.atan2(r2); }
template <size_t N, class TYPE> 
Real<N> pow      (const Real<N>  &r1, const TYPE &r2) { return r1.pow(r2); }

/// multiple precision real detetction 
template <class TYPE>
class is_multiple_precision_real
{
  public:
    static constexpr bool value=false;
};

template <size_t N>
class is_multiple_precision_real<Real<N>>
{
  public:
    static constexpr bool value=true;
};

// numeric limits
template <size_t N> 
class std::numeric_limits<Real<N>> : public std::numeric_limits<double>
{
  public:
    static constexpr bool is_specialized=true;
    static constexpr int  digits=N;
    static constexpr int  digits10=(int)std::log10(std::pow(2.0l,N));
};

// display
template <class STREAM, size_t N>
STREAM& operator << ( STREAM &str, const Real<N> &r ) { return ( str << (long double)r ); } 

////////////////////////////////////
//
// Now for the complex binding...
//
////////////////////////////////////

template <size_t N>
class Complex
{
  private:
  
                     mpc_t     c_;
    static constexpr mpc_rnd_t rnd_=MPC_RNDNN;
    
  public:
  
    ///! empty constructor
    ///
    Complex(void) { mpc_init2 (c_, N);  mpc_set_ui (c_, 0, rnd_); }

    ///! init constructor
    ///   
    Complex(const mpc_t &c)                     { mpc_init2(c_, N); mpc_set       (c_, c, rnd_); }
    Complex(const mpfr_t &c)                    { mpc_init2(c_, N); mpc_set_fr    (c_, c, rnd_); }
    Complex(const int &c)                       { mpc_init2(c_, N); mpc_set_si    (c_, (long int)c, rnd_); }
    Complex(const long int &c)                  { mpc_init2(c_, N); mpc_set_si    (c_, c, rnd_); }
    Complex(const unsigned long int &c)         { mpc_init2(c_, N); mpc_set_ui    (c_, c, rnd_); }
    Complex(const double &c)                    { mpc_init2(c_, N); mpc_set_d     (c_, c, rnd_); }
    Complex(const long double &c)               { mpc_init2(c_, N); mpc_set_ld    (c_, c, rnd_); }
    Complex(const std::complex<double> &c)      { mpc_init2(c_, N); mpc_set_d_d   (c_, std::real(c), std::imag(c), rnd_); }
    Complex(const std::complex<long double> &c) { mpc_init2(c_, N); mpc_set_ld_ld (c_, std::real(c), std::imag(c), rnd_); }
    /// 
    Complex(const mpfr_t &cr, const mpfr_t &ci)                       { mpc_init2(c_, N); mpc_set_fr_fr  (c_, cr, ci, rnd_); }
    Complex(const int &cr, const int &ci)                             { mpc_init2(c_, N); mpc_set_si_si  (c_, (long int)cr, (long int)ci, rnd_); }
    Complex(const long int &cr, const long int &ci)                   { mpc_init2(c_, N); mpc_set_si_si  (c_, cr, ci, rnd_); }
    Complex(const unsigned long int &cr, const unsigned long int &ci) { mpc_init2(c_, N); mpc_set_ui_ui  (c_, cr, ci, rnd_); }
    Complex(const double &cr, const double &ci)                       { mpc_init2(c_, N); mpc_set_d_d    (c_, cr, ci, rnd_); }
    Complex(const long double &cr, const long double &ci)             { mpc_init2(c_, N); mpc_set_ld_ld  (c_, cr, ci, rnd_); }
    ///   
    template <size_t M>
    Complex(const Complex<M> &c) { mpc_init2(c_, N); mpc_set (c_, c.c_, rnd_); }
    Complex(const Complex<N> &c) { mpc_init2(c_, N); mpc_set (c_, c.c_, rnd_); }
    template <size_t M>
    Complex(const Real<M> &r) { mpc_init2(c_, N); mpc_set_fr (c_, r.value(), rnd_); }
    template <size_t M>
    Complex(const Real<M> &r, const Real<M> &i) { mpc_init2(c_, N); mpc_set_fr_fr (c_, r.value(), i.value(), rnd_); }
    
    /// move constructor
    Complex(Complex<N> &&other) { mpc_init2(c_, N); mpc_swap(c_,other.c_); }

    /// assignment operators
    Complex<N>& operator= (      Complex<N> &&other) { mpc_swap  (c_,other.c_);      return *this; }
    template <size_t M>
    Complex<N>& operator= (const Complex<M>  &other) { mpc_set   (c_,other.c_,rnd_); return *this; }
    Complex<N>& operator= (const Complex<N>  &other) { mpc_set   (c_,other.c_,rnd_); return *this; }
    template <size_t M>
    Complex<N>& operator= (const Real<M>     &other) { mpc_set_fr (c_,other.value(),rnd_); return *this; }

    /// assignment of arithmetic type to multiprecision Real
    template <class TYPE>
    // SFINAE on return type to discard Real<N> + Real<M>
    typename std::enable_if<std::is_arithmetic<TYPE>::value,Complex<N>&>::type
    // addition operator definition: call divide class method
    operator= (const TYPE &a) { return ( *this = Complex<N>(a)); }

    // cast 
    operator double _Complex () const          { return mpc_get_dc(c_,rnd_);  }
    operator std::complex<double>() const      { return mpc_get_dc(c_,rnd_);  }
    operator long double _Complex() const      { return mpc_get_ldc(c_,rnd_); }
    operator std::complex<long double>() const { return mpc_get_ldc(c_,rnd_); }
    
    /// member access
          mpc_t&    value(void)       { return c_;   }  
    const mpc_t&    value(void) const { return c_;   }  
    const mpc_rnd_t rnd  (void) const { return rnd_; }
      
    // projections 
    Real<N> real(void) const { Real<N> result; mpc_real(result.value(),c_,result.rnd()); return result; }   
    Real<N> imag(void) const { Real<N> result; mpc_imag(result.value(),c_,result.rnd()); return result; }   
    Real<N> arg (void) const { Real<N> result; mpc_arg (result.value(),c_,result.rnd()); return result; }   
    Real<N> abs (void) const { Real<N> result; mpc_abs (result.value(),c_,result.rnd()); return result; }
    
    /// arithmetic function
    ///
    /// inplace addition
    ///
    template <size_t M> 
    Complex<N>& operator+=(const Complex<M>        &other) { mpc_add   (c_,c_,other.c_,rnd_);    return *this; }
    Complex<N>& operator+=(const mpc_t             &c)     { mpc_add   (c_,c_,c,rnd_);           return *this; }
    template <size_t M> 
    Complex<N>& operator+=(const Real<M>           &r)     { mpc_add_fr(c_,c_,r.value(),rnd_);   return *this; }
    Complex<N>& operator+=(const mpfr_t            &r)     { mpc_add_fr(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator+=(const double            &r)     { *this+=Complex<N>(r);               return *this; }
    Complex<N>& operator+=(const int               &r)     { (r>=0) ? mpc_add_ui(c_,c_,(long unsigned int)std::abs(r),rnd_) : mpc_sub_ui(c_,c_,(long unsigned int)std::abs(r),rnd_); return *this; }
    Complex<N>& operator+=(const long int          &r)     { (r>=0) ? mpc_add_ui(c_,c_,(long unsigned int)std::abs(r),rnd_) : mpc_sub_ui(c_,c_,(long unsigned int)std::abs(r),rnd_); return *this; }
    Complex<N>& operator+=(const long unsigned int &r)     { mpc_add_ui(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator+=(const long double       &r)     { *this+=Complex<N>(r);               return *this; }
    ///
    /// inplace substraction
    ///
    template <size_t M> 
    Complex<N>& operator-=(const Complex<M>        &other) { mpc_sub   (c_,c_,other.c_,rnd_);    return *this; }
    Complex<N>& operator-=(const mpc_t             &c)     { mpc_sub   (c_,c_,c,rnd_);           return *this; }
    template <size_t M> 
    Complex<N>& operator-=(const Real<M>           &r)     { mpc_sub_fr(c_,c_,r.value(),rnd_);   return *this; }
    Complex<N>& operator-=(const mpfr_t            &r)     { mpc_sub_fr(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator-=(const double            &r)     { *this-=Complex<N>(r);               return *this; }
    Complex<N>& operator-=(const int               &r)     { (r<=0) ? mpc_add_ui(c_,c_,(long unsigned int)std::abs(r),rnd_) : mpc_sub_ui(c_,c_,(long unsigned int)std::abs(r),rnd_); return *this; }
    Complex<N>& operator-=(const long int          &r)     { (r<=0) ? mpc_add_ui(c_,c_,(long unsigned int)std::abs(r),rnd_) : mpc_sub_ui(c_,c_,(long unsigned int)std::abs(r),rnd_); return *this; }
    Complex<N>& operator-=(const long unsigned int &r)     { mpc_sub_ui(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator-=(const long double       &r)     { *this-=Complex<N>(r);               return *this; }
    ///
    /// inplace multiplication
    ///
    template <size_t M> 
    Complex<N>& operator*=(const Complex<M>        &other) { mpc_mul   (c_,c_,other.c_,rnd_);    return *this; }
    Complex<N>& operator*=(const mpc_t             &c)     { mpc_mul   (c_,c_,c,rnd_);           return *this; }
    template <size_t M> 
    Complex<N>& operator*=(const Real<M>           &r)     { mpc_mul_fr(c_,c_,r.value(),rnd_);   return *this; }
    Complex<N>& operator*=(const mpfr_t            &r)     { mpc_mul_fr(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator*=(const double            &r)     { *this*=Complex<N>(r);               return *this; }
    Complex<N>& operator*=(const int               &r)     { mpc_mul_si(c_,c_,(long int)r,rnd_); return *this; }
    Complex<N>& operator*=(const long int          &r)     { mpc_mul_si(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator*=(const long unsigned int &r)     { mpc_mul_si(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator*=(const long double       &r)     { *this*=Complex<N>(r);               return *this; }
    ///
    /// inplace division
    ///
    template <size_t M> 
    Complex<N>& operator/=(const Complex<M>        &other) { mpc_div   (c_,c_,other.c_,rnd_);    return *this; }
    Complex<N>& operator/=(const mpc_t             &c)     { mpc_div   (c_,c_,c,rnd_);           return *this; }
    template <size_t M> 
    Complex<N>& operator/=(const Real<M>           &r)     { mpc_div_fr(c_,c_,r.value(),rnd_);   return *this; }
    Complex<N>& operator/=(const mpfr_t            &r)     { mpc_div_fr(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator/=(const double            &r)     { *this/=Complex<N>(r);               return *this; }
    Complex<N>& operator/=(const int               &r)     { mpc_div_ui(c_,c_,(long unsigned int)std::abs(r),rnd_); if (r<0) mpc_neg(c_,c_,rnd); return *this; }
    Complex<N>& operator/=(const long int          &r)     { mpc_div_ui(c_,c_,(long unsigned int)std::abs(r),rnd_); if (r<0) mpc_neg(c_,c_,rnd); return *this; }
    Complex<N>& operator/=(const long unsigned int &r)     { mpc_div_ui(c_,c_,r,rnd_);           return *this; }
    Complex<N>& operator/=(const long double       &r)     { *this/=Complex<N>(r);               return *this; }
    
    /// unary minus
    Complex<N> operator- () const { Complex<N> c; mpc_neg(c.c_,c_,rnd_); return c; }
    
    /// addition 
    template <class TYPE>
    Complex<N> operator+ (const TYPE &num) const { Complex<N> c(*this); return c+=num; }

    /// substraction 
    template <class TYPE>
    Complex<N> operator- (const TYPE &num) const { Complex<N> c(*this); return c-=num; }

    /// multiplication 
    template <class TYPE>
    Complex<N> operator* (const TYPE &num) const { Complex<N> c(*this); return c*=num; }

    /// division 
    template <class TYPE>
    Complex<N> operator/ (const TYPE &num) const { Complex<N> c(*this); return c/=num; }

    ///
    /// num-*this 
    ///
    template <size_t M> 
    Complex<std::max(M,N)> substract_to(const Complex<M>        &other) const { Complex<std::max(M,N)> result; mpc_sub   (result.c_,other.c_,c_,rnd_);    return result; }
    Complex<N>             substract_to(const mpc_t             &c)     const { Complex<N> result;             mpc_sub   (result.c_,c,c_,rnd_);           return result; }
    template <size_t M> 
    Complex<std::max(M,N)> substract_to(const Real<M>           &r)     const { Complex<std::max(M,N)> result; mpc_fr_sub(result.c_,r.value(),c_,rnd_);   return result; }
    Complex<N>             substract_to(const mpfr_t            &r)     const { Complex<N> result;             mpc_fr_sub(result.c_,r,c_,rnd_);           return result; }
    Complex<N>             substract_to(const double            &r)     const { return  this->divide(Complex<N>(r)); }
    Complex<N>             substract_to(const int               &r)     const { Complex<N> result; (r>=0) ? mpc_ui_sub(result.c_,(unsigned long int)std::abs(r),c_,rnd_) : mpc_add_ui(result.c_,c_,(unsigned long int)std::abs(r),rnd_), mpc_neg(result.c_,result.c_,rnd_); return result; }
    Complex<N>             substract_to(const long int          &r)     const { Complex<N> result; (r>=0) ? mpc_ui_sub(result.c_,(unsigned long int)std::abs(r),c_,rnd_) : mpc_add_ui(result.c_,c_,(unsigned long int)std::abs(r),rnd_), mpc_neg(result.c_,result.c_,rnd_); return result; }
    Complex<N>             substract_to(const long unsigned int &r)     const { Complex<N> result;             mpc_ui_sub(result.c_,r,c_,rnd_);           return result; }
    Complex<N>             substract_to(const long double       &r)     const { return  this->divide(Complex<N>(r)); }

    ///
    /// num/*this 
    ///
    template <size_t M> 
    Complex<std::max(M,N)> divide(const Complex<M>        &other) const { Complex<std::max(M,N)> result; mpc_div   (result.c_,other.c_,c_,rnd_);    return result; }
    Complex<N>             divide(const mpc_t             &c)     const { Complex<N> result;             mpc_div   (result.c_,c,c_,rnd_);           return result; }
    template <size_t M> 
    Complex<std::max(M,N)> divide(const Real<M>           &r)     const { Complex<std::max(M,N)> result; mpc_fr_div(result.c_,r.value(),c_,rnd_);   return result; }
    Complex<N>             divide(const mpfr_t            &r)     const { Complex<N> result;             mpc_fr_div(result.c_,r,c_,rnd_);           return result; }
    Complex<N>             divide(const double            &r)     const { return  this->divide(Complex<N>(r)); }
    Complex<N>             divide(const int               &r)     const { Complex<N> result;             mpc_ui_div(result.c_,(unsigned long int)std::abs(r),c_,rnd_); if (r<0) mpc_neg(result.c_,result.c_,rnd_); return result; }
    Complex<N>             divide(const long int          &r)     const { Complex<N> result;             mpc_ui_div(result.c_,(unsigned long int)std::abs(r),c_,rnd_); if (r<0) mpc_neg(result.c_,result.c_,rnd_); return result; }
    Complex<N>             divide(const long unsigned int &r)     const { Complex<N> result;             mpc_ui_div(result.c_,r,c_,rnd_);           return result; }
    Complex<N>             divide(const long double       &r)     const { return  this->divide(Complex<N>(r)); }
    
    // power
    template <size_t M> 
    Complex<N> pow(const Complex<M>        &c) const { Complex<N> result; mpc_pow(result.c_,c_,c.c_,rnd_);           return result; } 
    Complex<N> pow(const mpc_t             &c) const { Complex<N> result; mpc_pow(result.c_,c_,c,rnd_);              return result; } 
    template <size_t M> 
    Complex<N> pow(const Real<M>           &r) const { Complex<N> result; mpc_pow_fr(result.c_,c_,r.value(),rnd_);   return result; } 
    Complex<N> pow(const mpfr_t            &r) const { Complex<N> result; mpc_pow_fr(result.c_,c_,r,rnd_);           return result; } 
    Complex<N> pow(const double            &r) const { Complex<N> result; mpc_pow_d (result.c_,c_,r,rnd_);           return result; }  
    Complex<N> pow(const long double       &r) const { Complex<N> result; mpc_pow_ld(result.c_,c_,r,rnd_);           return result; }  
    Complex<N> pow(const int               &r) const { Complex<N> result; mpc_pow_si(result.c_,c_,(long int)r,rnd_); return result; }  
    Complex<N> pow(const long int          &r) const { Complex<N> result; mpc_pow_si(result.c_,c_,r,rnd_);           return result; }  
    Complex<N> pow(const long unsigned int &r) const { Complex<N> result; mpc_pow_ui(result.c_,c_,r,rnd_);           return result; }  
    
    /// functions
    ///
    Complex<N> conj (void)                const { return Complex<N>(real(),-imag()); }
    Complex<N> sqrt (void)                const { Complex<N> result; mpc_sqrt(result.c_,c_,rnd_);       return result; }
    Complex<N> log  (void)                const { Complex<N> result; mpc_log(result.c_,c_,rnd_);        return result; }
    Complex<N> log10(void)                const { Complex<N> result; mpc_log10(result.c_,c_,rnd_);      return result; }
    Complex<N> exp  (void)                const { Complex<N> result; mpc_exp(result.c_,c_,rnd_);        return result; }
    Complex<N> cos  (void)                const { Complex<N> result; mpc_cos(result.c_,c_,rnd_);        return result; }
    Complex<N> sin  (void)                const { Complex<N> result; mpc_sin(result.c_,c_,rnd_);        return result; }
    Complex<N> tan  (void)                const { Complex<N> result; mpc_tan(result.c_,c_,rnd_);        return result; }
    Complex<N> acos (void)                const { Complex<N> result; mpc_acos(result.c_,c_,rnd_);       return result; }
    Complex<N> asin (void)                const { Complex<N> result; mpc_asin(result.c_,c_,rnd_);       return result; }
    Complex<N> atan (void)                const { Complex<N> result; mpc_atan(result.c_,c_,rnd_);       return result; }
    Complex<N> cosh (void)                const { Complex<N> result; mpc_cosh(result.c_,c_,rnd_);       return result; }
    Complex<N> sinh (void)                const { Complex<N> result; mpc_sinh(result.c_,c_,rnd_);       return result; }
    Complex<N> tanh (void)                const { Complex<N> result; mpc_tanh(result.c_,c_,rnd_);       return result; }
    Complex<N> acosh(void)                const { Complex<N> result; mpc_acosh(result.c_,c_,rnd_);      return result; }
    Complex<N> asinh(void)                const { Complex<N> result; mpc_asinh(result.c_,c_,rnd_);      return result; }
    Complex<N> atanh(void)                const { Complex<N> result; mpc_atanh(result.c_,c_,rnd_);      return result; }
          
    ///! destructor
    ///
    ~Complex(void)
    {
      mpc_clear (c_);
    }

  // all Complex are friends, regardless of precision...
  template <size_t M>
  friend class Complex;  
     
};

/// addition of arithmetic type to multiprecision Complex
template <class TYPE, size_t N>
// SFINAE on return type to discard Complex<N> + Complex<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value || is_multiple_precision_real<TYPE>::value,Complex<N>>::type
// addition operator definition: call divide class method
operator+ (const TYPE &a, const Complex<N> &b) { Complex<N> c(b); return c+=a; }

/// substraction of multiprecision Complex to arithmetic type
template <class TYPE, size_t N>
// SFINAE on return type to discard Complex<N> + Complex<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value || is_multiple_precision_real<TYPE>::value,Complex<N>>::type
// substraction operator definition: call substract_to class method
operator- (const TYPE &a, const Complex<N> &b) { return b.substract_to(a); }

/// multiplication of arithmetic type with multiprecision Complex
template <class TYPE, size_t N>
// SFINAE on return type to discard Complex<N> + Complex<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value || is_multiple_precision_real<TYPE>::value,Complex<N>>::type
// addition operator definition: call divide class method
operator* (const TYPE &a, const Complex<N> &b) { Complex<N> c(b); return c*=a; }

/// division of arithmetic type by multiprecision Complex
template <class TYPE, size_t N>
// SFINAE on return type to discard Complex<N> / Complex<M>
typename std::enable_if<std::is_arithmetic<TYPE>::value || is_multiple_precision_real<TYPE>::value,Complex<N>>::type
// division operatr definition: call divide class method
operator/ (const TYPE &num, const Complex<N> &den) { return den.divide(num); }
  
/// single arguments functions
///
template <size_t N>
Real<N>    real (const Complex<N> &r) { return r.real(); }
template <size_t N>
Real<N>    imag (const Complex<N> &r) { return r.imag(); }
template <size_t N>
Real<N>    arg  (const Complex<N> &r) { return r.arg(); }
template <size_t N>
Real<N>    abs  (const Complex<N> &r) { return r.abs(); }
template <size_t N>
Complex<N> conj (const Complex<N> &r) { return r.conj(); }
template <size_t N>
Complex<N> sqrt (const Complex<N> &r) { return r.sqrt(); }
template <size_t N>
Complex<N> log  (const Complex<N> &r) { return r.log(); }
template <size_t N>
Complex<N> log10(const Complex<N> &r) { return r.log10(); }
template <size_t N>
Complex<N> exp  (const Complex<N> &r) { return r.exp(); }
template <size_t N>
Complex<N> cos  (const Complex<N> &r) { return r.cos(); }
template <size_t N>
Complex<N> sin  (const Complex<N> &r) { return r.sin(); }
template <size_t N>
Complex<N> tan  (const Complex<N> &r) { return r.tan(); }
template <size_t N>
Complex<N> acos (const Complex<N> &r) { return r.acos(); }
template <size_t N>
Complex<N> asin (const Complex<N> &r) { return r.asin(); }
template <size_t N>
Complex<N> atan (const Complex<N> &r) { return r.atan(); }
template <size_t N>
Complex<N> cosh (const Complex<N> &r) { return r.cosh(); }
template <size_t N>
Complex<N> sinh (const Complex<N> &r) { return r.sinh(); }
template <size_t N>
Complex<N> tanh (const Complex<N> &r) { return r.tanh(); }
template <size_t N>
Complex<N> acosh(const Complex<N> &r) { return r.acosh(); }
template <size_t N>
Complex<N> asinh(const Complex<N> &r) { return r.asinh(); }
template <size_t N>
Complex<N> atanh(const Complex<N> &r) { return r.atanh(); }

// power
template <size_t N, class TYPE> 
Complex<N> pow  (const Complex<N>  &r1, const TYPE &r2) { return r1.pow(r2); }


/// multiple precision complex detetction 
template <class TYPE>
class is_multiple_precision_complex
{
  public:
    static constexpr bool value=false;
};

template <size_t N>
class is_multiple_precision_complex<Complex<N>>
{
  public:
    static constexpr bool value=true;
};

// numeric limits
template <size_t N> 
class std::numeric_limits<Complex<N>> : public std::numeric_limits<Real<N>>
{
  public:
    static constexpr bool is_specialized=true;
    static constexpr int  digits=N;
    static constexpr int  digits10=(int)std::log10(std::pow(2.0l,N));
};

// display
template <class STREAM, size_t N>
STREAM& operator << ( STREAM &str, const Complex<N> &r ) { return ( str << std::complex<long double>(r) ); } 


#endif
