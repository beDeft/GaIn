#ifndef RN_H
#define RN_H

//> explicit instanciation of proxy for Yukawa R functions evaluation 
///
/// can be either called as 
///
///  Yukawa_weighted_R<order>(a, k, d)
///
/// or 
///
///  Yukawa_weighted_R(a, k, d, order)
///
template <class PRECISION>
std::complex<PRECISION> Yukawa_R(PRECISION a, std::complex<PRECISION> k, PRECISION d, int order);

template <class PRECISION>
std::complex<PRECISION> Yukawa_weighted_R(PRECISION a, std::complex<PRECISION> k, PRECISION d, int order);

// declare explicit instanciation of double and long double types
extern template std::complex<double>      Yukawa_R(double a, std::complex<double> k, double d, int order);
extern template std::complex<long double> Yukawa_R(long double a, std::complex<long double> k, long double d, int order);

#endif
