#ifndef FN_RECURSIVE_ARRAY_H
#define FN_RECURSIVE_ARRAY_H

#include "Erfc.h"
#include "Erfcx.h"

#ifndef CHECK_FINITE_H
#define CHECK_FINITE_H

template <class PREC>
bool check_isfinite(PREC a_2nm1) { return true; }

template <>
bool check_isfinite<double>(double a_2nm1) { return std::isfinite(a_2nm1); }

template <>
bool check_isfinite<long double>(long double a_2nm1) { return std::isfinite(a_2nm1); }

#endif

//> implements weighted exp(z^2)*erfc(z) for complex z argument
///
/// Uses algorithm proposed by Mohammad Al Azah and Simon N Chandler-Wilde in
///
/// "Computation of the Complex Error Function using Modified Trapezoidal Rules"
///
namespace weighted_erfcx_util {
  
  namespace constants
  {
    using std::pow;
    using std::sqrt;

    // t_k constant
    // (k+1/2)*h
    template <class  PREC,
              size_t N,
              size_t K>
    const PREC tk2   = pow(PREC( sqrt( PREC(4)*atan(PREC(1)) / PREC(N+1) )*( PREC(K) + PREC(1)/PREC(2) ) ),2);  
  
    // tau_k constant
    // k*h
    template <class  PREC,
              size_t N,
              size_t K>
    const PREC tauk2 = pow(PREC( sqrt( PREC(4)*atan(PREC(1)) / PREC(N+1) )*( PREC(K) ) ),2);  
  }  

  template <class  PREC, size_t N, size_t K>
  struct sum_helper 
  {
    //
    // sum over t_k constants
    //
    static inline PREC tk_sum(const PREC z2)
    {
      // unroll sum
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tk2<REAL,N,K>)/(z2-constants::tk2<REAL,N,K>)+sum_helper<PREC,N,K+1>::tk_sum(z2);
    }
    
    //
    // sum over tau_k constants
    //
    static inline PREC tauk_sum(const PREC z2)
    {
      // unroll sum
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tauk2<REAL,N,K>)/(z2-constants::tauk2<REAL,N,K>)+sum_helper<PREC,N,K+1>::tauk_sum(z2);
    }
  };
  
  // sum over t_k constants, stop specialization
  //
  template <class  PREC, size_t N>
  struct sum_helper<PREC,N,N> 
  {
    //
    // sum over t_k constants, return N^th term
    //
    static inline PREC tk_sum(const PREC z2)
    {
      // return first term
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tk2<REAL,N,N>)/(z2-constants::tk2<REAL,N,N>);
    }

    // sum over t_k constants, return N^th term
    //
    static inline PREC tauk_sum(const PREC z2)
    {
      // return first term
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tauk2<REAL,N,N>)/(z2-constants::tauk2<REAL,N,N>);
    }
  };
  
  // exp(-w^2) * W^M_N implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WMN(const COMPLEX w, const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::exp;
    using std::sqrt;
    using std::atan;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const COMPLEX fact(REAL(0),REAL(2)*h/PI);
    return exp(-w*w) * z * fact * sum_helper<COMPLEX,N,0>::tk_sum(z*z); 
  }

  // exp(-w^2) * W^MM_N implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WMMN(const COMPLEX w, const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::exp;
    using std::sqrt;
    using std::atan;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const COMPLEX i(REAL(0),REAL(1));
    const REAL    fact = REAL(2)*PI/h;
    return REAL(2)*exp(-(w+i*z)*(w-i*z)-fact*imag(z))/(exp(-fact*imag(z))+exp(-i*fact*real(z))) + WMN<COMPLEX,N>(w,z); 
  }
 
  // exp(-w^2) * W^MT_N implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WMTN(const COMPLEX w, const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::exp;
    using std::sqrt;
    using std::atan;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const COMPLEX i(REAL(0),REAL(1));
    const REAL    fact1 = REAL(2)*PI/h;
    const COMPLEX fact2(REAL(0),REAL(1)*h/PI);
    const COMPLEX fact3(REAL(0),REAL(2)*h/PI);
    return REAL(2)*exp(-(w+i*z)*(w-i*z)-fact1*imag(z))/(exp(-fact1*imag(z))-exp(-i*fact1*real(z))) + exp(-w*w) * ( fact2 / z + fact3 * z * sum_helper<COMPLEX,N,1>::tauk_sum(z*z) ); 
  }
 
  // exp(-w^2) * W_N, first quadrant implementation : z=x+iy with x>=0 and y>=0
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WN_1st_quadrant(const COMPLEX w, const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::max;
    using std::atan;
    using std::floor;
    using std::real;
    using std::imag;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const REAL    PI_over_h = PI/h;
    // form 1
    if ( imag(z) >= max(real(z),PI_over_h) ) return WMN<COMPLEX,N>(w,z);
    // form 2 and 3
    else
    {
      // fractional part phi
      REAL phi = real(z)/h - floor(real(z)/h);
      // form 2 
      if ( imag(z) < real(z) && REAL(1)/REAL(4) <= phi && phi <= REAL(3)/REAL(4) ) return WMTN<COMPLEX,N>(w,z);
    }
    // form 3
    return WMMN<COMPLEX,N>(w,z);
  }
  
  // exp(-w^2) * W_N, full quadrant implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WN_full_quadrant(const COMPLEX w, const COMPLEX z)
  {
    // precision of real type
    using std::exp;
    using std::abs;
    using std::real;
    using std::imag;
    using std::conj;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    const COMPLEX i(REAL(0),REAL(1));
    // 1st and 4th
    if ( real(z) >= REAL(0) )
    {
      // 1st 
      if ( imag(z) >= REAL(0) ) return WN_1st_quadrant<COMPLEX,N>( w, z );
      // 4th
      return conj( REAL(2)*exp( -(w+i*conj(z))*(w-i*conj(z)) ) - WN_1st_quadrant<COMPLEX,N>( w, conj(z) ) );
    }
    // 2nd
    if ( imag(z) >= REAL(0) ) return conj( WN_1st_quadrant<COMPLEX,N>( w, -conj(z) ) );
    // 3rd
    return REAL(2)*exp( -(w+i*z)*(w-i*z) ) - WN_1st_quadrant<COMPLEX,N>( w, -z );
  }
}

template <class COMPLEX>
inline COMPLEX weighted_erfcx( const COMPLEX w, const COMPLEX z ) 
{
  // precision of real type
  using std::abs;
  using std::real;
  using std::imag;
  using REAL = decltype( abs(std::declval<COMPLEX&>()) );
  // call routine. using same amount of quadrature point as digits, to be on the safe side...
  return weighted_erfcx_util::WN_full_quadrant<COMPLEX,std::numeric_limits<REAL>::digits10>( w, COMPLEX( -imag(z) , real(z) ) );
}

template <>
inline std::complex<double> weighted_erfcx<std::complex<double>>( const std::complex<double> w, const std::complex<double> z) 
{ 
  return weighted_erfcx_util::WN_full_quadrant<std::complex<double>,12>( w, std::complex<double> ( -std::imag(z) , std::real(z) ) );
}

template <>
inline std::complex<long double> weighted_erfcx<std::complex<long double>>( const std::complex<long double> w, const std::complex<long double> z) 
{ 
  return weighted_erfcx_util::WN_full_quadrant<std::complex<long double>,16>( w, std::complex<long double> ( -std::imag(z) , std::real(z) ) );
}

template <class  PREC1, class PREC2>
void Yukawa_Fn_recursive(PREC2 a, PREC1 k, PREC2 d, PREC1 *YF, size_t order_max)
{
  using std::abs;
  using std::min;
  using std::pow;
  using std::exp;
  using std::real;
  using std::imag;
  using std::sqrt;
      
  // init Yukawa_F_{-1} and F_{0}    
  PREC1 Fm1;
  PREC1 F0;
  {
    // compute differently depending on x/y to avoid overflow
    PREC1 val_p;
    PREC1 val_m;
    PREC1 zp = k/a/PREC1(2)+a*d;
    PREC1 zm = k/a/PREC1(2)-a*d;
    //
    if ( real(zp) <= -abs(imag(zp)) )  
    {
      val_p = exp( k/a*k/a/PREC1(4)+k*d ) * erfc<PREC1>(zp);
    }
    else
    {
      val_p = weighted_erfcx(PREC1(a*d),zp);
    }
    //
    if ( real(zm) <= -abs(imag(zm)) )  
    {
      val_m = exp( k/a*k/a/PREC1(4)-k*d ) * erfc<PREC1>(zm);
    }
    else
    {
      val_m = weighted_erfcx(PREC1(a*d),zm);
    }
    
    // return result (avoid division by k here with
    // special treatment of recursion when N=1) 
    //
    Fm1 = ( val_p + val_m ) * PREC2( sqrt( PREC2(4) * atan( PREC2(1) ) ) );
    F0  = ( val_p - val_m ) / d * PREC2( sqrt( PREC2(4) * atan( PREC2(1) ) ) );
  }
  
  // keep F_{0}
  YF[0]=F0;
  
  // compute only once
  PREC2 exp_ma2d2 = exp(-a*a*d*d);
  
  // iterate up to order_max
  for ( size_t N=1 ; N<=order_max ; N++ )    
  {    
    // if a can can be exponantiated safely
    PREC2 a_2nm1 = pow(a,2*N-1);
    PREC1 F1;
    if ( check_isfinite(a_2nm1) )
    {
      F1 = (  pow(k,min(N,size_t(2))) * Fm1  // use min to avoid division by k in F_{-1}
           -  PREC2(2*N-1)    * F0
           -  pow(-2,N+1)     * a_2nm1 * exp_ma2d2 ) / (d*d);
    }
    // otherwise
    else
    {  
      F1 = (  pow(k,min(N,size_t(2))) * Fm1  // use min to avoid division by k in F_{-1}
           -  PREC2(2*N-1)    * F0
           -  pow(-2,N+1)     * pow(a*exp(-a*a*d*d/PREC2(2*N-1)),2*N-1) ) / (d*d);
    }
    // keep result
    YF[N]=F1;
    // increment recursion
    Fm1=F0;
    F0=F1;
  }
}

#endif
