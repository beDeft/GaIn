#ifndef YUKAWA_R_FUNCTION_RECUSIVE_H
#define YUKAWA_R_FUNCTION_RECUSIVE_H

#include "Erfc.h"
#include "Erfcx.h"

//
// use struct meta for recursion
//
template <class COMPLEX,
          int   ORDER>
struct R_function_recursive_helper
{
  static inline COMPLEX eval(const COMPLEX z, const COMPLEX z2)
  {
    // get real type
    using std::abs;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // return recursion step  
    return REAL(1) - REAL(2) / REAL(2*ORDER-1) * z2 * R_function_recursive_helper<COMPLEX,ORDER-1>::eval(z,z2);
  } 
};

//
// specialization: explicit formula for R0 = 1 - z * sqrt(pi) * erfcx(z) 
//
template <class COMPLEX>
struct R_function_recursive_helper<COMPLEX,0>
{
  static inline COMPLEX eval(const COMPLEX z, const COMPLEX z2)
  {
    // get real type
    using std::abs;
    using std::atan;
    using std::sqrt;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // constants
    const REAL sqrt_pi = sqrt( REAL(4) * atan( REAL(1) ) ); 
    // return recursion step  
    return REAL(1) - z * sqrt_pi * erfcx(z);
  } 
};

//
// R function definition  
//
template <int   ORDER,
          class COMPLEX>
inline COMPLEX Yukawa_R_recursive( const COMPLEX z )
{
  return R_function_recursive_helper<COMPLEX,ORDER>::eval(z,z*z);
} 


//
// use struct meta for recursion
//
template <class WTYPE,
          class COMPLEX,
          int   ORDER>
struct R_weighted_function_recursive_helper
{
  static inline COMPLEX eval(const COMPLEX z, const COMPLEX z2, const WTYPE ad, const WTYPE exp_ad2)
  {
    // get real type
    using std::abs;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // return recursion step  
    return exp_ad2 - REAL(2) / REAL(2*ORDER-1) * z2 * R_weighted_function_recursive_helper<WTYPE,COMPLEX,ORDER-1>::eval(z,z2,ad,exp_ad2);
  } 
};

//
// specialization: explicit formula for R0 = 1 - z * sqrt(pi) * erfcx(z) 
//
template <class WTYPE,
          class COMPLEX>
struct R_weighted_function_recursive_helper<WTYPE,COMPLEX,0>
{
  static inline COMPLEX eval(const COMPLEX z, const COMPLEX z2, const WTYPE ad, const WTYPE exp_ad2)
  {
    // get real type
    using std::abs;
    using std::exp;
    using std::real;
    using std::imag;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // constants
    const REAL sqrt_pi = sqrt( REAL(4) * atan( REAL(1) ) ); 
    // compute differently depending on x/y to avoid overflow
    if ( real(z) <= -abs(imag(z)) )  
    {
      auto v1 = erfc<COMPLEX>(z);
      auto v2 = exp( z*z-ad*ad );
      return exp_ad2 - z * sqrt_pi * v2 * v1;
    }
    // return recursion step  
    return exp_ad2 * ( REAL(1) - z * sqrt_pi * erfcx(z) );
  } 
};


//
// R function definition  
//
template <int   ORDER,
          class WTYPE,
          class COMPLEX>
inline COMPLEX Yukawa_R_weighted_recursive( const WTYPE ad, const COMPLEX z  )
{
  using std::exp;
  return R_weighted_function_recursive_helper<WTYPE,COMPLEX,ORDER>::eval(z,z*z,ad,exp(-ad*ad));
} 


#endif
