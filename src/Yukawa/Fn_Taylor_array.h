#ifndef FN_TAYLOR_ARRAY_H
#define FN_TAYLOR_ARRAY_H

#include "Rn.h"
#include <array>

template <class REAL,
          class COMPLEX>
void Yukawa_Fn_Taylor(REAL a, COMPLEX k, REAL d, COMPLEX *YF, size_t n_min, size_t n_max, size_t order)
{
  using std::pow;
  using std::real;
  using std::imag;
  
  // some constant
  auto a2d2=a*a*d*d;
  
  // compute R function values only once
  std::array<COMPLEX,81> Rval;
  for ( size_t N=n_min ; N<=n_max+order ; N++ ) 
  {
    // do nt assign imedaitely so we can work with abitrary precision types
    auto val = Yukawa_R((long double)(1.0),std::complex<long double>((long double)real(k/a),(long double)imag(k/a)),(long double)(0.0),N); 
    Rval[N]  = COMPLEX(real(val),imag(val)); 
  }
  
  // init YF horner expression with Rval[N+ORDER]
  for ( int N=n_min ; N<=n_max ; N++ ) YF[N]=Rval[N+order];
  
  // compute recursion for requested orders
  for ( size_t K=order ; K>0      ; K-- )
  for ( size_t N=n_min ; N<=n_max ; N++ ) 
  {
    // c_nk
    REAL c_nk = - REAL( 2 * ( N + K ) - 1 ) / REAL( K * ( 2 * ( N + K ) + 1 ) );
    // update horner form
    YF[N] = Rval[N+K-1] + a2d2 * c_nk * YF[N];
  }
  
  // scale horner expressions 
  for ( size_t N=n_min ; N<=n_max ; N++ )
  { 
    // C_n0
    REAL C_n0 = REAL( 2 * std::pow( -2 , N+1 )) / REAL( 2*N + 1 );
    //  
    YF[N] *= pow(a,2*N+1) * C_n0;   
  }
}          

#endif
