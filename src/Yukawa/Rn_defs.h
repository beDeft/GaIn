#ifndef RN_DEFS_H
#define RN_DEFS_H

#include "Rn_core.h"

inline std::complex<double> Yukawa_R0(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R0_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R0_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R0_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 1 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R0_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 1 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R0(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 1 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R0_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R0_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R0_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 1 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R0_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 1 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R1(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R1_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R1_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R1_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 2 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R1_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 2 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R1(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 2 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R1_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R1_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R1_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 2 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R1_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 2 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R2(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R2_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R2_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R2_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 3 )
                / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R2_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 3 )
          / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R2(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 3 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R2_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R2_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R2_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 3 )
                / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R2_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 3 )
          / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R3(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R3_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R3_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R3_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 4 )
                / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R3_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 4 )
          / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R3(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 4 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R3_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R3_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R3_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 4 )
                / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R3_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 4 )
          / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R4(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R4_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R4_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R4_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 5 )
                / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R4_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 5 )
          / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R4(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 5 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R4_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R4_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R4_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 5 )
                / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R4_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 5 )
          / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R5(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R5_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R5_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R5_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 6 )
                / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R5_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 6 )
          / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R5(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 6 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R5_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R5_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R5_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 6 )
                / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R5_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 6 )
          / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R6(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R6_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R6_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R6_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 7 )
                / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R6_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 7 )
          / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R6(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 7 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R6_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R6_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R6_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 7 )
                / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R6_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 7 )
          / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R7(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R7_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R7_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R7_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 8 )
                / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R7_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 8 )
          / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R7(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 8 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R7_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R7_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R7_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 8 )
                / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R7_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 8 )
          / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R8(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R8_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R8_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R8_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 9 )
                / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R8_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 9 )
          / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R8(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 9 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R8_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R8_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R8_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 9 )
                / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R8_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 9 )
          / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R9(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R9_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R9_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R9_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 10 )
                / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R9_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 10 )
          / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R9(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 10 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R9_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R9_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R9_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 10 )
                / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R9_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 10 )
          / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R10(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R10_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R10_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R10_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 11 )
                / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R10_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 11 )
          / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R10(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 11 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R10_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R10_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R10_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 11 )
                / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R10_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 11 )
          / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R11(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R11_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R11_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R11_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 12 )
                / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R11_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 12 )
          / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R11(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 12 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R11_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R11_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R11_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 12 )
                / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R11_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 12 )
          / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R12(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R12_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R12_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R12_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 13 )
                / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R12_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 13 )
          / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R12(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 13 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R12_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R12_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R12_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 13 )
                / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R12_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 13 )
          / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R13(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R13_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R13_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R13_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 14 )
                / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R13_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 14 )
          / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R13(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 14 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R13_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R13_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R13_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 14 )
                / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R13_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 14 )
          / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R14(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R14_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R14_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R14_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 15 )
                / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R14_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 15 )
          / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R14(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 15 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R14_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R14_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R14_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 15 )
                / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R14_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 15 )
          / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R15(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R15_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R15_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R15_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 16 )
                / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R15_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 16 )
          / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R15(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 16 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R15_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R15_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R15_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 16 )
                / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R15_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 16 )
          / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R16(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R16_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R16_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R16_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 17 )
                / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R16_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 17 )
          / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R16(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 17 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R16_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R16_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R16_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 17 )
                / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R16_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 17 )
          / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R17(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R17_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R17_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R17_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 18 )
                / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R17_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 18 )
          / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R17(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 18 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R17_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R17_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R17_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 18 )
                / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R17_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 18 )
          / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R18(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R18_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R18_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R18_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 19 )
                / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R18_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 19 )
          / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R18(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 19 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R18_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R18_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R18_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 19 )
                / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R18_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 19 )
          / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R19(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R19_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R19_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R19_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 20 )
                / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R19_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 20 )
          / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R19(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 20 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R19_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R19_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R19_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 20 )
                / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R19_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 20 )
          / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R20(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R20_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R20_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R20_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 21 )
                / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R20_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 21 )
          / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R20(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 21 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R20_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R20_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R20_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 21 )
                / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R20_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 21 )
          / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R21(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R21_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R21_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R21_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 22 )
                / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R21_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 22 )
          / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R21(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 22 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R21_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R21_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R21_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 22 )
                / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R21_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 22 )
          / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R22(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R22_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R22_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R22_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 23 )
                / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R22_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 23 )
          / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R22(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 23 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R22_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R22_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R22_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 23 )
                / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R22_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 23 )
          / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R23(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R23_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R23_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R23_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 24 )
                / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R23_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 24 )
          / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R23(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 24 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R23_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R23_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R23_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 24 )
                / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R23_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 24 )
          / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R24(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R24_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R24_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R24_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 25 )
                / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R24_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 25 )
          / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R24(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 25 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R24_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R24_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R24_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 25 )
                / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R24_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 25 )
          / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R25(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R25_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R25_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R25_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 26 )
                / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R25_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 26 )
          / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R25(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 26 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R25_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R25_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R25_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 26 )
                / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R25_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 26 )
          / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R26(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R26_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R26_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R26_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 27 )
                / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R26_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 27 )
          / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R26(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 27 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R26_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R26_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R26_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 27 )
                / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R26_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 27 )
          / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R27(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R27_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R27_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R27_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 28 )
                / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R27_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 28 )
          / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R27(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 28 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R27_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R27_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R27_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 28 )
                / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R27_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 28 )
          / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R28(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R28_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R28_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R28_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 29 )
                / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R28_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 29 )
          / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R28(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 29 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R28_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R28_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R28_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 29 )
                / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R28_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 29 )
          / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R29(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R29_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R29_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R29_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 30 )
                / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R29_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 30 )
          / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R29(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 30 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R29_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R29_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R29_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 30 )
                / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R29_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 30 )
          / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R30(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R30_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R30_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R30_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 31 )
                / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R30_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 31 )
          / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R30(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 31 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R30_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R30_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R30_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 31 )
                / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R30_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 31 )
          / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R31(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R31_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R31_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R31_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 32 )
                / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R31_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 32 )
          / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R31(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 32 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R31_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R31_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R31_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 32 )
                / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R31_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 32 )
          / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R32(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R32_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R32_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R32_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 33 )
                / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R32_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 33 )
          / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R32(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 33 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R32_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R32_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R32_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 33 )
                / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R32_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 33 )
          / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R33(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R33_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R33_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R33_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 34 )
                / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R33_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 34 )
          / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R33(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 34 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R33_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R33_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R33_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 34 )
                / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R33_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 34 )
          / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R34(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R34_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R34_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R34_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 35 )
                / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R34_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 35 )
          / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R34(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 35 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R34_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R34_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R34_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 35 )
                / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R34_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 35 )
          / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R35(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R35_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R35_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R35_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 36 )
                / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R35_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 36 )
          / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R35(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 36 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R35_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R35_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R35_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 36 )
                / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R35_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 36 )
          / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R36(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R36_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R36_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R36_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 37 )
                / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R36_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 37 )
          / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R36(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 37 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R36_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R36_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R36_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 37 )
                / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R36_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 37 )
          / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R37(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R37_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R37_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R37_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 38 )
                / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R37_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 38 )
          / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R37(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 38 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R37_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R37_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R37_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 38 )
                / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R37_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 38 )
          / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R38(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R38_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R38_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R38_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 39 )
                / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R38_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 39 )
          / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R38(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 39 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R38_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R38_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R38_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 39 )
                / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R38_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 39 )
          / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R39(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R39_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R39_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R39_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 40 )
                / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R39_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 40 )
          / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R39(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 40 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R39_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R39_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R39_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 40 )
                / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R39_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 40 )
          / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R40(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R40_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R40_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R40_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 41 )
                / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R40_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 41 )
          / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R40(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 41 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R40_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R40_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R40_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 41 )
                / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R40_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 41 )
          / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R41(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R41_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R41_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R41_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 42 )
                / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R41_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 42 )
          / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R41(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 42 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R41_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R41_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R41_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 42 )
                / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R41_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 42 )
          / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R42(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R42_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R42_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R42_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 43 )
                / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R42_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 43 )
          / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R42(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 43 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R42_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R42_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R42_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 43 )
                / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R42_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 43 )
          / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R43(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R43_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R43_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R43_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 44 )
                / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R43_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 44 )
          / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R43(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 44 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R43_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R43_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R43_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 44 )
                / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R43_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 44 )
          / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R44(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R44_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R44_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R44_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 45 )
                / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R44_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 45 )
          / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R44(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 45 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R44_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R44_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R44_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 45 )
                / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R44_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 45 )
          / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R45(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R45_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R45_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R45_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 46 )
                / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R45_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 46 )
          / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R45(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 46 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R45_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R45_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R45_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 46 )
                / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R45_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 46 )
          / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R46(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R46_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R46_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R46_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 47 )
                / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R46_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 47 )
          / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R46(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 47 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R46_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R46_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R46_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 47 )
                / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R46_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 47 )
          / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R47(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R47_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R47_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R47_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 48 )
                / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R47_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 48 )
          / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R47(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 48 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R47_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R47_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R47_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 48 )
                / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R47_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 48 )
          / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R48(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R48_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R48_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R48_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 49 )
                / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R48_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 49 )
          / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R48(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 49 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R48_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R48_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R48_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 49 )
                / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R48_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 49 )
          / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R49(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R49_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R49_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R49_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 50 )
                / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R49_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 50 )
          / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R49(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 50 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R49_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R49_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R49_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 50 )
                / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R49_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 50 )
          / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R50(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R50_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R50_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R50_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 51 )
                / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R50_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 51 )
          / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R50(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 51 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R50_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R50_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R50_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 51 )
                / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R50_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 51 )
          / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R51(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R51_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R51_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R51_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 52 )
                / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R51_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 52 )
          / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R51(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 52 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R51_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R51_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R51_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 52 )
                / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R51_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 52 )
          / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R52(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R52_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R52_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R52_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 53 )
                / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R52_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 53 )
          / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R52(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 53 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R52_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R52_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R52_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 53 )
                / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R52_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 53 )
          / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R53(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R53_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R53_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R53_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 54 )
                / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R53_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 54 )
          / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R53(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 54 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R53_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R53_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R53_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 54 )
                / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R53_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 54 )
          / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R54(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R54_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R54_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R54_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 55 )
                / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R54_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 55 )
          / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R54(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 55 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R54_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R54_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R54_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 55 )
                / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R54_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 55 )
          / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R55(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R55_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R55_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R55_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 56 )
                / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R55_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 56 )
          / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R55(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 56 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R55_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R55_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R55_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 56 )
                / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R55_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 56 )
          / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R56(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R56_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R56_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R56_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 57 )
                / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R56_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 57 )
          / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R56(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 57 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R56_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R56_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R56_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 57 )
                / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R56_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 57 )
          / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R57(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R57_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R57_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R57_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 58 )
                / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R57_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 58 )
          / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R57(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 58 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R57_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R57_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R57_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 58 )
                / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R57_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 58 )
          / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R58(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R58_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R58_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R58_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 59 )
                / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R58_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 59 )
          / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R58(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 59 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R58_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R58_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R58_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 59 )
                / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R58_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 59 )
          / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R59(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R59_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R59_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R59_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 60 )
                / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R59_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 60 )
          / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R59(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 60 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R59_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R59_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R59_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 60 )
                / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R59_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 60 )
          / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R60(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R60_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R60_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R60_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 61 )
                / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R60_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 61 )
          / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R60(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 61 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R60_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R60_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R60_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 61 )
                / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R60_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 61 )
          / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R61(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R61_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R61_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R61_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 62 )
                / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R61_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 62 )
          / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R61(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 62 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R61_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R61_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R61_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 62 )
                / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R61_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 62 )
          / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R62(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R62_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R62_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R62_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 63 )
                / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R62_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 63 )
          / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R62(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 63 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R62_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R62_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R62_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 63 )
                / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R62_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 63 )
          / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R63(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R63_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R63_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R63_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 64 )
                / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R63_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 64 )
          / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R63(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 64 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R63_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R63_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R63_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 64 )
                / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R63_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 64 )
          / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R64(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R64_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R64_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R64_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 65 )
                / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R64_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 65 )
          / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R64(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 65 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R64_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R64_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R64_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 65 )
                / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R64_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 65 )
          / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R65(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R65_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R65_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R65_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 66 )
                / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R65_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 66 )
          / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R65(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 66 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R65_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R65_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R65_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 66 )
                / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R65_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 66 )
          / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R66(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R66_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R66_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R66_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 67 )
                / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R66_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 67 )
          / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R66(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 67 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R66_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R66_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R66_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 67 )
                / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R66_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 67 )
          / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R67(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R67_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R67_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R67_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 68 )
                / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R67_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 68 )
          / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R67(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 68 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R67_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R67_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R67_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 68 )
                / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R67_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 68 )
          / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R68(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R68_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R68_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R68_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 69 )
                / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R68_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 69 )
          / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R68(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 69 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R68_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R68_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R68_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 69 )
                / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R68_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 69 )
          / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R69(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R69_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R69_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R69_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 70 )
                / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R69_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 70 )
          / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R69(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 70 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R69_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R69_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R69_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 70 )
                / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R69_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 70 )
          / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R70(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R70_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R70_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R70_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 71 )
                / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R70_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 71 )
          / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R70(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 71 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R70_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R70_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R70_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 71 )
                / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R70_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 71 )
          / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R71(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R71_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R71_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R71_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 72 )
                / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R71_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 72 )
          / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R71(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 72 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R71_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R71_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R71_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 72 )
                / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R71_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 72 )
          / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R72(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R72_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R72_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R72_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 73 )
                / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R72_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 73 )
          / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R72(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 73 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R72_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R72_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R72_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 73 )
                / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R72_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 73 )
          / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R73(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R73_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R73_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R73_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 74 )
                / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R73_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 74 )
          / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R73(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 74 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R73_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R73_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R73_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 74 )
                / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R73_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 74 )
          / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R74(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R74_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R74_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R74_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 75 )
                / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R74_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 75 )
          / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R74(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 75 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R74_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R74_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R74_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 75 )
                / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R74_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 75 )
          / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R75(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R75_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R75_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R75_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 76 )
                / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R75_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 76 )
          / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R75(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 76 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R75_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R75_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R75_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 76 )
                / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R75_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 76 )
          / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R76(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R76_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R76_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R76_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 77 )
                / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R76_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 77 )
          / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R76(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 77 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R76_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R76_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R76_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 77 )
                / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R76_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 77 )
          / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R77(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R77_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R77_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R77_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 78 )
                / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R77_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 78 )
          / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R77(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 78 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R77_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R77_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R77_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 78 )
                / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R77_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 78 )
          / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R78(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R78_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R78_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R78_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 79 )
                / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R78_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 79 )
          / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R78(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 79 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R78_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R78_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R78_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 79 )
                / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R78_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 79 )
          / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R79(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R79_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R79_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R79_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 80 )
                / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R79_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 80 )
          / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R79(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 80 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R79_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R79_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R79_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 80 )
                / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R79_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 80 )
          / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_R80(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R80_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R80_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R80_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 81 )
                / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R80_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 81 )
          / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}

inline std::complex<double> Yukawa_weighted_R80(double a, std::complex<double> k, double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 81 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R80_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R80_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R80_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 81 )
                / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R80_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 81 )
          / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<double>(real(val),imag(val));
}





inline std::complex<long double> Yukawa_R0(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R0_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R0_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R0_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 1 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R0_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 1 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R0(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 1 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R0_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R0_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R0_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 1 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R0_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 1 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R1(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R1_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R1_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R1_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 2 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R1_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 2 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R1(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 2 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R1_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R1_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R1_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 2 )
                / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R1_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 2 )
          / YUKAWA_Rn_INTERN_PREC(-1.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R2(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R2_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R2_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R2_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 3 )
                / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R2_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 3 )
          / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R2(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 3 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R2_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R2_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R2_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 3 )
                / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R2_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 3 )
          / YUKAWA_Rn_INTERN_PREC(-3.000000000000000000000000e+00l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R3(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R3_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R3_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R3_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 4 )
                / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R3_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 4 )
          / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R3(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 4 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R3_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R3_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R3_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 4 )
                / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R3_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 4 )
          / YUKAWA_Rn_INTERN_PREC(-1.500000000000000000000000e+01l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R4(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R4_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R4_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R4_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 5 )
                / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R4_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 5 )
          / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R4(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 5 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R4_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R4_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R4_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 5 )
                / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R4_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 5 )
          / YUKAWA_Rn_INTERN_PREC(-1.050000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R5(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R5_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R5_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R5_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 6 )
                / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R5_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 6 )
          / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R5(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 6 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R5_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R5_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R5_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 6 )
                / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R5_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 6 )
          / YUKAWA_Rn_INTERN_PREC(-9.450000000000000000000000e+02l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R6(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R6_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R6_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R6_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 7 )
                / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R6_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 7 )
          / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R6(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 7 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R6_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R6_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R6_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 7 )
                / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R6_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 7 )
          / YUKAWA_Rn_INTERN_PREC(-1.039500000000000000000000e+04l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R7(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R7_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R7_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R7_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 8 )
                / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R7_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 8 )
          / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R7(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 8 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R7_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R7_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R7_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 8 )
                / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R7_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 8 )
          / YUKAWA_Rn_INTERN_PREC(-1.351350000000000000000000e+05l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R8(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R8_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R8_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R8_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 9 )
                / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R8_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 9 )
          / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R8(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 9 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R8_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R8_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R8_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 9 )
                / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R8_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 9 )
          / YUKAWA_Rn_INTERN_PREC(-2.027025000000000000000000e+06l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R9(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R9_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R9_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R9_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 10 )
                / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R9_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 10 )
          / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R9(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 10 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R9_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R9_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R9_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 10 )
                / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R9_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 10 )
          / YUKAWA_Rn_INTERN_PREC(-3.445942500000000000000000e+07l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R10(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R10_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R10_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R10_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 11 )
                / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R10_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 11 )
          / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R10(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 11 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R10_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R10_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R10_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 11 )
                / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R10_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 11 )
          / YUKAWA_Rn_INTERN_PREC(-6.547290750000000000000000e+08l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R11(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R11_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R11_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R11_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 12 )
                / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R11_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 12 )
          / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R11(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 12 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R11_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R11_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R11_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 12 )
                / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R11_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 12 )
          / YUKAWA_Rn_INTERN_PREC(-1.374931057500000000000000e+10l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R12(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R12_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R12_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R12_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 13 )
                / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R12_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 13 )
          / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R12(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 13 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R12_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R12_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R12_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 13 )
                / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R12_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 13 )
          / YUKAWA_Rn_INTERN_PREC(-3.162341432250000000000000e+11l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R13(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R13_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R13_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R13_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 14 )
                / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R13_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 14 )
          / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R13(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 14 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R13_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R13_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R13_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 14 )
                / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R13_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 14 )
          / YUKAWA_Rn_INTERN_PREC(-7.905853580625000000000000e+12l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R14(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R14_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R14_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R14_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 15 )
                / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R14_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 15 )
          / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R14(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 15 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R14_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R14_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R14_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 15 )
                / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R14_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 15 )
          / YUKAWA_Rn_INTERN_PREC(-2.134580466768750000000000e+14l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R15(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R15_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R15_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R15_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 16 )
                / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R15_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 16 )
          / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R15(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 16 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R15_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R15_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R15_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 16 )
                / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R15_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 16 )
          / YUKAWA_Rn_INTERN_PREC(-6.190283353629375000000000e+15l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R16(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R16_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R16_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R16_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 17 )
                / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R16_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 17 )
          / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R16(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 17 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R16_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R16_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R16_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 17 )
                / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R16_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 17 )
          / YUKAWA_Rn_INTERN_PREC(-1.918987839625106250000000e+17l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R17(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R17_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R17_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R17_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 18 )
                / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R17_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 18 )
          / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R17(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 18 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R17_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R17_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R17_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 18 )
                / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R17_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 18 )
          / YUKAWA_Rn_INTERN_PREC(-6.332659870762850625000000e+18l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R18(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R18_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R18_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R18_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 19 )
                / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R18_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 19 )
          / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R18(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 19 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R18_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R18_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R18_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 19 )
                / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R18_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 19 )
          / YUKAWA_Rn_INTERN_PREC(-2.216430954766997718720000e+20l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R19(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R19_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R19_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R19_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 20 )
                / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R19_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 20 )
          / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R19(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 20 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R19_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R19_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R19_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 20 )
                / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R19_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 20 )
          / YUKAWA_Rn_INTERN_PREC(-8.200794532637891559424000e+21l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R20(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R20_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R20_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R20_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 21 )
                / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R20_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 21 )
          / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R20(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 21 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R20_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R20_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R20_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 21 )
                / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R20_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 21 )
          / YUKAWA_Rn_INTERN_PREC(-3.198309867728777708175360e+23l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R21(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R21_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R21_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R21_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 22 )
                / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R21_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 22 )
          / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R21(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 22 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R21_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R21_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R21_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 22 )
                / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R21_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 22 )
          / YUKAWA_Rn_INTERN_PREC(-1.311307045768798860319130e+25l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R22(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R22_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R22_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R22_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 23 )
                / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R22_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 23 )
          / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R22(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 23 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R22_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R22_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R22_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 23 )
                / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R22_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 23 )
          / YUKAWA_Rn_INTERN_PREC(-5.638620296805835099393229e+26l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R23(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R23_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R23_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R23_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 24 )
                / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R23_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 24 )
          / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R23(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 24 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R23_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R23_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R23_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 24 )
                / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R23_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 24 )
          / YUKAWA_Rn_INTERN_PREC(-2.537379133562625794676621e+28l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R24(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R24_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R24_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R24_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 25 )
                / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R24_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 25 )
          / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R24(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 25 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R24_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R24_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R24_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 25 )
                / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R24_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 25 )
          / YUKAWA_Rn_INTERN_PREC(-1.192568192774434123506602e+30l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R25(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R25_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R25_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R25_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 26 )
                / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R25_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 26 )
          / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R25(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 26 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R25_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R25_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R25_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 26 )
                / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R25_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 26 )
          / YUKAWA_Rn_INTERN_PREC(-5.843584144594727205134246e+31l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R26(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R26_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R26_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R26_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 27 )
                / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R26_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 27 )
          / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R26(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 27 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R26_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R26_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R26_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 27 )
                / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R26_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 27 )
          / YUKAWA_Rn_INTERN_PREC(-2.980227913743310874644854e+33l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R27(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R27_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R27_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R27_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 28 )
                / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R27_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 28 )
          / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R27(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 28 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R27_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R27_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R27_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 28 )
                / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R27_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 28 )
          / YUKAWA_Rn_INTERN_PREC(-1.579520794283954763595549e+35l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R28(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R28_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R28_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R28_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 29 )
                / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R28_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 29 )
          / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R28(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 29 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R28_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R28_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R28_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 29 )
                / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R28_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 29 )
          / YUKAWA_Rn_INTERN_PREC(-8.687364368561751199595378e+36l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R29(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R29_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R29_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R29_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 30 )
                / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R29_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 30 )
          / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R29(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 30 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R29_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R29_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R29_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 30 )
                / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R29_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 30 )
          / YUKAWA_Rn_INTERN_PREC(-4.951797690080198183884658e+38l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R30(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R30_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R30_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R30_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 31 )
                / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R30_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 31 )
          / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R30(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 31 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R30_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R30_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R30_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 31 )
                / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R30_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 31 )
          / YUKAWA_Rn_INTERN_PREC(-2.921560637147316928587871e+40l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R31(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R31_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R31_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R31_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 32 )
                / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R31_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 32 )
          / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R31(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 32 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R31_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R31_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R31_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 32 )
                / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R31_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 32 )
          / YUKAWA_Rn_INTERN_PREC(-1.782151988659863326431518e+42l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R32(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R32_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R32_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R32_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 33 )
                / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R32_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 33 )
          / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R32(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 33 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R32_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R32_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R32_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 33 )
                / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R32_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 33 )
          / YUKAWA_Rn_INTERN_PREC(-1.122755752855713895660923e+44l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R33(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R33_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R33_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R33_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 34 )
                / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R33_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 34 )
          / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R33(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 34 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R33_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R33_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R33_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 34 )
                / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R33_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 34 )
          / YUKAWA_Rn_INTERN_PREC(-7.297912393562140321931400e+45l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R34(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R34_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R34_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R34_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 35 )
                / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R34_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 35 )
          / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R34(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 35 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R34_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R34_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R34_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 35 )
                / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R34_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 35 )
          / YUKAWA_Rn_INTERN_PREC(-4.889601303686634015588813e+47l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R35(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R35_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R35_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R35_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 36 )
                / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R35_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 36 )
          / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R35(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 36 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R35_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R35_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R35_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 36 )
                / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R35_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 36 )
          / YUKAWA_Rn_INTERN_PREC(-3.373824899543777470641400e+49l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R36(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R36_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R36_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R36_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 37 )
                / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R36_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 37 )
          / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R36(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 37 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R36_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R36_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R36_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 37 )
                / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R36_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 37 )
          / YUKAWA_Rn_INTERN_PREC(-2.395415678676082004170606e+51l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R37(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R37_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R37_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R37_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 38 )
                / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R37_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 38 )
          / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R37(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 38 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R37_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R37_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R37_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 38 )
                / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R37_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 38 )
          / YUKAWA_Rn_INTERN_PREC(-1.748653445433539863046165e+53l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R38(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R38_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R38_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R38_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 39 )
                / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R38_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 39 )
          / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R38(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 39 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R38_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R38_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R38_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 39 )
                / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R38_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 39 )
          / YUKAWA_Rn_INTERN_PREC(-1.311490084075154897338624e+55l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R39(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R39_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R39_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R39_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 40 )
                / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R39_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 40 )
          / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R39(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 40 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R39_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R39_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R39_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 40 )
                / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R39_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 40 )
          / YUKAWA_Rn_INTERN_PREC(-1.009847364737869270933460e+57l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R40(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R40_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R40_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R40_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 41 )
                / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R40_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 41 )
          / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R40(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 41 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R40_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R40_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R40_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 41 )
                / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R40_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 41 )
          / YUKAWA_Rn_INTERN_PREC(-7.977794181429167240510449e+58l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R41(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R41_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R41_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R41_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 42 )
                / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R41_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 42 )
          / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R41(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 42 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R41_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R41_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R41_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 42 )
                / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R41_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 42 )
          / YUKAWA_Rn_INTERN_PREC(-6.462013286957625465042133e+60l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R42(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R42_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R42_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R42_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 43 )
                / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R42_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 43 )
          / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R42(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 43 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R42_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R42_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R42_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 43 )
                / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R42_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 43 )
          / YUKAWA_Rn_INTERN_PREC(-5.363471028174829136110412e+62l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R43(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R43_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R43_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R43_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 44 )
                / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R43_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 44 )
          / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R43(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 44 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R43_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R43_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R43_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 44 )
                / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R43_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 44 )
          / YUKAWA_Rn_INTERN_PREC(-4.558950373948604765649249e+64l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R44(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R44_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R44_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R44_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 45 )
                / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R44_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 45 )
          / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R44(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 45 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R44_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R44_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R44_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 45 )
                / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R44_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 45 )
          / YUKAWA_Rn_INTERN_PREC(-3.966286825335286146017794e+66l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R45(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R45_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R45_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R45_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 46 )
                / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R45_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 46 )
          / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R45(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 46 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R45_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R45_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R45_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 46 )
                / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R45_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 46 )
          / YUKAWA_Rn_INTERN_PREC(-3.529995274548404669911991e+68l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R46(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R46_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R46_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R46_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 47 )
                / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R46_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 47 )
          / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R46(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 47 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R46_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R46_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R46_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 47 )
                / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R46_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 47 )
          / YUKAWA_Rn_INTERN_PREC(-3.212295699839048249615235e+70l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R47(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R47_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R47_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R47_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 48 )
                / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R47_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 48 )
          / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R47(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 48 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R47_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R47_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R47_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 48 )
                / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R47_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 48 )
          / YUKAWA_Rn_INTERN_PREC(-2.987435000850314872127203e+72l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R48(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R48_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R48_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R48_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 49 )
                / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R48_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 49 )
          / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R48(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 49 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R48_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R48_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R48_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 49 )
                / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R48_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 49 )
          / YUKAWA_Rn_INTERN_PREC(-2.838063250807799128511265e+74l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R49(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R49_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R49_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R49_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 50 )
                / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R49_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 50 )
          / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R49(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 50 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R49_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R49_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R49_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 50 )
                / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R49_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 50 )
          / YUKAWA_Rn_INTERN_PREC(-2.752921353283565154594627e+76l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R50(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R50_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R50_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R50_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 51 )
                / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R50_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 51 )
          / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R50(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 51 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R50_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R50_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R50_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 51 )
                / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R50_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 51 )
          / YUKAWA_Rn_INTERN_PREC(-2.725392139750729502971786e+78l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R51(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R51_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R51_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R51_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 52 )
                / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R51_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 52 )
          / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R51(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 52 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R51_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R51_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R51_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 52 )
                / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R51_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 52 )
          / YUKAWA_Rn_INTERN_PREC(-2.752646061148236797899062e+80l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R52(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R52_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R52_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R52_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 53 )
                / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R52_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 53 )
          / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R52(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 53 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R52_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R52_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R52_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 53 )
                / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R52_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 53 )
          / YUKAWA_Rn_INTERN_PREC(-2.835225442982683901836034e+82l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R53(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R53_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R53_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R53_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 54 )
                / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R53_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 54 )
          / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R53(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 54 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R53_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R53_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R53_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 54 )
                / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R53_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 54 )
          / YUKAWA_Rn_INTERN_PREC(-2.976986715131818096949227e+84l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R54(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R54_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R54_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R54_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 55 )
                / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R54_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 55 )
          / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R54(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 55 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R54_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R54_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R54_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 55 )
                / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R54_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 55 )
          / YUKAWA_Rn_INTERN_PREC(-3.185375785191045363619829e+86l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R55(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R55_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R55_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R55_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 56 )
                / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R55_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 56 )
          / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R55(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 56 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R55_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R55_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R55_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 56 )
                / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R55_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 56 )
          / YUKAWA_Rn_INTERN_PREC(-3.472059605858239446475022e+88l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R56(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R56_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R56_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R56_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 57 )
                / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R56_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 57 )
          / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R56(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 57 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R56_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R56_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R56_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 57 )
                / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R56_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 57 )
          / YUKAWA_Rn_INTERN_PREC(-3.853986162502645785483748e+90l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R57(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R57_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R57_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R57_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 58 )
                / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R57_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 58 )
          / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R57(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 58 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R57_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R57_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R57_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 58 )
                / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R57_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 58 )
          / YUKAWA_Rn_INTERN_PREC(-4.355004363627989737702646e+92l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R58(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R58_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R58_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R58_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 59 )
                / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R58_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 59 )
          / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R58(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 59 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R58_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R58_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R58_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 59 )
                / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R58_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 59 )
          / YUKAWA_Rn_INTERN_PREC(-5.008255018172188198273234e+94l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R59(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R59_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R59_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R59_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 60 )
                / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R59_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 60 )
          / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R59(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 60 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R59_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R59_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R59_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 60 )
                / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R59_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 60 )
          / YUKAWA_Rn_INTERN_PREC(-5.859658371261460191849418e+96l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R60(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R60_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R60_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R60_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 61 )
                / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R60_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 61 )
          / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R60(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 61 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R60_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R60_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R60_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 61 )
                / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R60_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 61 )
          / YUKAWA_Rn_INTERN_PREC(-6.972993461801137628532392e+98l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R61(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R61_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R61_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R61_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 62 )
                / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R61_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 62 )
          / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R61(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 62 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R61_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R61_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R61_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 62 )
                / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R61_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 62 )
          / YUKAWA_Rn_INTERN_PREC(-8.437322088779376530743550e+100l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R62(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R62_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R62_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R62_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 63 )
                / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R62_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 63 )
          / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R62(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 63 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R62_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R62_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R62_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 63 )
                / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R62_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 63 )
          / YUKAWA_Rn_INTERN_PREC(-1.037790616919863313317123e+103l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R63(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R63_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R63_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R63_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 64 )
                / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R63_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 64 )
          / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R63(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 64 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R63_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R63_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R63_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 64 )
                / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R63_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 64 )
          / YUKAWA_Rn_INTERN_PREC(-1.297238271149829141609493e+105l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R64(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R64_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R64_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R64_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 65 )
                / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R64_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 65 )
          / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R64(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 65 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R64_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R64_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R64_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 65 )
                / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R64_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 65 )
          / YUKAWA_Rn_INTERN_PREC(-1.647492604360283009826650e+107l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R65(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R65_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R65_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R65_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 66 )
                / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R65_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 66 )
          / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R65(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 66 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R65_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R65_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R65_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 66 )
                / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R65_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 66 )
          / YUKAWA_Rn_INTERN_PREC(-2.1252654596247650826286e+109l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R66(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R66_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R66_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R66_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 67 )
                / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R66_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 67 )
          / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R66(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 67 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R66_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R66_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R66_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 67 )
                / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R66_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 67 )
          / YUKAWA_Rn_INTERN_PREC(-2.7840977521084422583393e+111l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R67(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R67_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R67_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R67_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 68 )
                / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R67_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 68 )
          / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R67(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 68 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R67_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R67_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R67_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 68 )
                / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R67_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 68 )
          / YUKAWA_Rn_INTERN_PREC(-3.7028500103042282034973e+113l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R68(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R68_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R68_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R68_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 69 )
                / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R68_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 69 )
          / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R68(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 69 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R68_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R68_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R68_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 69 )
                / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R68_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 69 )
          / YUKAWA_Rn_INTERN_PREC(-4.9988475139107080747948e+115l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R69(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R69_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R69_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R69_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 70 )
                / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R69_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 70 )
          / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R69(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 70 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R69_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R69_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R69_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 70 )
                / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R69_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 70 )
          / YUKAWA_Rn_INTERN_PREC(-6.8484210940576700623408e+117l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R70(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R70_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R70_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R70_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 71 )
                / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R70_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 71 )
          / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R70(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 71 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R70_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R70_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R70_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 71 )
                / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R70_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 71 )
          / YUKAWA_Rn_INTERN_PREC(-9.5193053207401613863310e+119l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R71(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R71_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R71_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R71_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 72 )
                / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R71_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 72 )
          / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R71(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 72 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R71_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R71_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R71_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 72 )
                / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R71_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 72 )
          / YUKAWA_Rn_INTERN_PREC(-1.3422220502243627554279e+122l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R72(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R72_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R72_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R72_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 73 )
                / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R72_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 73 )
          / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R72(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 73 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R72_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R72_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R72_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 73 )
                / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R72_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 73 )
          / YUKAWA_Rn_INTERN_PREC(-1.9193775318208387402457e+124l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R73(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R73_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R73_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R73_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 74 )
                / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R73_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 74 )
          / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R73(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 74 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R73_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R73_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R73_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 74 )
                / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R73_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 74 )
          / YUKAWA_Rn_INTERN_PREC(-2.7830974211402161733254e+126l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R74(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R74_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R74_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R74_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 75 )
                / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R74_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 75 )
          / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R74(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 75 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R74_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R74_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R74_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 75 )
                / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R74_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 75 )
          / YUKAWA_Rn_INTERN_PREC(-4.0911532090761177747707e+128l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R75(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R75_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R75_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R75_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 76 )
                / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R75_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 76 )
          / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R75(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 76 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R75_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R75_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R75_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 76 )
                / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R75_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 76 )
          / YUKAWA_Rn_INTERN_PREC(-6.0958182815234154845210e+130l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R76(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R76_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R76_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R76_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 77 )
                / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R76_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 77 )
          / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R76(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 77 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R76_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R76_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R76_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 77 )
                / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R76_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 77 )
          / YUKAWA_Rn_INTERN_PREC(-9.2046856051003573814440e+132l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R77(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R77_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R77_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R77_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 78 )
                / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R77_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 78 )
          / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R77(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 78 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R77_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R77_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R77_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 78 )
                / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R77_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 78 )
          / YUKAWA_Rn_INTERN_PREC(-1.4083168975803546793240e+135l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R78(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R78_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R78_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R78_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 79 )
                / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R78_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 79 )
          / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R78(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 79 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R78_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R78_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R78_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 79 )
                / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R78_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 79 )
          / YUKAWA_Rn_INTERN_PREC(-2.1828911912495497528860e+137l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R79(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R79_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R79_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R79_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 80 )
                / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R79_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 80 )
          / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R79(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 80 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R79_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R79_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R79_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 80 )
                / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R79_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 80 )
          / YUKAWA_Rn_INTERN_PREC(-3.4271391702617931121379e+139l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_R80(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = Yukawa_R80_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = conj( Yukawa_R80_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    auto zd=k/(2*a)+a*d;
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(zd)*conj(zd));
      val = conj( Yukawa_R80_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z) , 81 )
                / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp(zd*zd);
      val = Yukawa_R80_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z , 81 )
          / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}

inline std::complex<long double> Yukawa_weighted_R80(long double a, std::complex<long double> k, long double d)
{
  using std::exp;
  using std::real;
  using std::imag;
  using std::conj;
  
  constexpr YUKAWA_Rn_INTERN_PREC pi=YUKAWA_Rn_INTERN_PREC(4)*atan(YUKAWA_Rn_INTERN_PREC(1));
  
  // form a*d arg
  long double ad = a * d;
  
  // form z arg
  std::complex<YUKAWA_Rn_INTERN_PREC> z = std::complex<YUKAWA_Rn_INTERN_PREC>(real(k),imag(k)) / ( YUKAWA_Rn_INTERN_PREC(2) * YUKAWA_Rn_INTERN_PREC(a) ) + YUKAWA_Rn_INTERN_PREC(ad);
  
  // result variable in internal precision
  std::complex<YUKAWA_Rn_INTERN_PREC> val;
  
  // form exp(-ad^2) / ad^(2n+2)
  auto exp_ad2 = YUKAWA_Rn_INTERN_PREC( exp(-ad*ad) ) / std::pow( ad*ad , 81 );
  
  // 1st and 4th quadrant
  if ( real(z)>=0 )
  {
    // 1st quadrant 
    if ( imag(z)>=0 )
    {
      val = exp_ad2 * Yukawa_R80_1st_Quadrant(z);
    }
    // 4th quadrant
    else
    {
      val = exp_ad2 * conj( Yukawa_R80_1st_Quadrant( conj(z) ) );
    }
  }
  else
  {
    // form z2 in long double prec
    // 2nd quadrant 
    if ( imag(z)>=0 )
    {
      auto exp_k2_m_ad2 = exp(conj(k/(2*a))*conj(k/(2*a)) + conj(k)*d);
      val = conj( exp_ad2 * Yukawa_R80_1st_Quadrant( conj(-z) ) 
                - std::sqrt(pi)/(conj(z)) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*conj(z)*conj(z)/YUKAWA_Rn_INTERN_PREC(ad*ad) , 81 )
                / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) ) );
    }
    // 3rd quadrant
    else
    {
      auto exp_k2_m_ad2 = exp((k/(2*a))*(k/(2*a)) + k*d);
      val = exp_ad2 * Yukawa_R80_1st_Quadrant( -z ) 
          - std::sqrt(pi)/(z) * std::pow( -YUKAWA_Rn_INTERN_PREC(2)*z*z/YUKAWA_Rn_INTERN_PREC(ad*ad) , 81 )
          / YUKAWA_Rn_INTERN_PREC(-5.4491512807162510483148e+141l) * std::complex<YUKAWA_Rn_INTERN_PREC>( real(exp_k2_m_ad2), imag(exp_k2_m_ad2) );
    }
  }
  return std::complex<long double>(real(val),imag(val));
}


//> proxy for Yukawa R functions evaluation 
///
/// can be either called as 
///
///  Yukawa_weighted_R<order>(a, k, d)
///
/// or 
///
///  Yukawa_weighted_R(a, k, d, order)
///
template <class PRECISION>
std::complex<PRECISION> Yukawa_R(PRECISION a, std::complex<PRECISION> k, PRECISION d, int order)
{
  switch (order)
  {
    case 0: return Yukawa_R0(a,k,d);
    case 1: return Yukawa_R1(a,k,d);
    case 2: return Yukawa_R2(a,k,d);
    case 3: return Yukawa_R3(a,k,d);
    case 4: return Yukawa_R4(a,k,d);
    case 5: return Yukawa_R5(a,k,d);
    case 6: return Yukawa_R6(a,k,d);
    case 7: return Yukawa_R7(a,k,d);
    case 8: return Yukawa_R8(a,k,d);
    case 9: return Yukawa_R9(a,k,d);
    //
    case 10: return Yukawa_R10(a,k,d);
    case 11: return Yukawa_R11(a,k,d);
    case 12: return Yukawa_R12(a,k,d);
    case 13: return Yukawa_R13(a,k,d);
    case 14: return Yukawa_R14(a,k,d);
    case 15: return Yukawa_R15(a,k,d);
    case 16: return Yukawa_R16(a,k,d);
    case 17: return Yukawa_R17(a,k,d);
    case 18: return Yukawa_R18(a,k,d);
    case 19: return Yukawa_R19(a,k,d);
    //
    case 20: return Yukawa_R20(a,k,d);
    case 21: return Yukawa_R21(a,k,d);
    case 22: return Yukawa_R22(a,k,d);
    case 23: return Yukawa_R23(a,k,d);
    case 24: return Yukawa_R24(a,k,d);
    case 25: return Yukawa_R25(a,k,d);
    case 26: return Yukawa_R26(a,k,d);
    case 27: return Yukawa_R27(a,k,d);
    case 28: return Yukawa_R28(a,k,d);
    case 29: return Yukawa_R29(a,k,d);
    //
    case 30: return Yukawa_R30(a,k,d);
    case 31: return Yukawa_R31(a,k,d);
    case 32: return Yukawa_R32(a,k,d);
    case 33: return Yukawa_R33(a,k,d);
    case 34: return Yukawa_R34(a,k,d);
    case 35: return Yukawa_R35(a,k,d);
    case 36: return Yukawa_R36(a,k,d);
    case 37: return Yukawa_R37(a,k,d);
    case 38: return Yukawa_R38(a,k,d);
    case 39: return Yukawa_R39(a,k,d);
    //
    case 40: return Yukawa_R40(a,k,d);
    case 41: return Yukawa_R41(a,k,d);
    case 42: return Yukawa_R42(a,k,d);
    case 43: return Yukawa_R43(a,k,d);
    case 44: return Yukawa_R44(a,k,d);
    case 45: return Yukawa_R45(a,k,d);
    case 46: return Yukawa_R46(a,k,d);
    case 47: return Yukawa_R47(a,k,d);
    case 48: return Yukawa_R48(a,k,d);
    case 49: return Yukawa_R49(a,k,d);
    //
    case 50: return Yukawa_R50(a,k,d);
    case 51: return Yukawa_R51(a,k,d);
    case 52: return Yukawa_R52(a,k,d);
    case 53: return Yukawa_R53(a,k,d);
    case 54: return Yukawa_R54(a,k,d);
    case 55: return Yukawa_R55(a,k,d);
    case 56: return Yukawa_R56(a,k,d);
    case 57: return Yukawa_R57(a,k,d);
    case 58: return Yukawa_R58(a,k,d);
    case 59: return Yukawa_R59(a,k,d);
    //
    case 60: return Yukawa_R60(a,k,d);
    case 61: return Yukawa_R61(a,k,d);
    case 62: return Yukawa_R62(a,k,d);
    case 63: return Yukawa_R63(a,k,d);
    case 64: return Yukawa_R64(a,k,d);
    case 65: return Yukawa_R65(a,k,d);
    case 66: return Yukawa_R66(a,k,d);
    case 67: return Yukawa_R67(a,k,d);
    case 68: return Yukawa_R68(a,k,d);
    case 69: return Yukawa_R69(a,k,d);
    //
    case 70: return Yukawa_R70(a,k,d);
    case 71: return Yukawa_R71(a,k,d);
    case 72: return Yukawa_R72(a,k,d);
    case 73: return Yukawa_R73(a,k,d);
    case 74: return Yukawa_R74(a,k,d);
    case 75: return Yukawa_R75(a,k,d);
    case 76: return Yukawa_R76(a,k,d);
    case 77: return Yukawa_R77(a,k,d);
    case 78: return Yukawa_R78(a,k,d);
    case 79: return Yukawa_R79(a,k,d);
    //
    case 80: return Yukawa_R80(a,k,d);
    //
    default: throw( std::invalid_argument("wrong function order, detected in Yukawa_R") );
  }
} 

//> proxy for Yukawa R functions evaluation 
///
/// can be either called as 
///
///  Yukawa_weighted_R<order>(a, k, d)
///
/// or 
///
///  Yukawa_weighted_R(a, k, d, order)
///
template <class PRECISION>
std::complex<PRECISION> Yukawa_weighted_R(PRECISION a, std::complex<PRECISION> k, PRECISION d, int order)
{
  switch (order)
  {
    case 0: return Yukawa_weighted_R0(a,k,d);
    case 1: return Yukawa_weighted_R1(a,k,d);
    case 2: return Yukawa_weighted_R2(a,k,d);
    case 3: return Yukawa_weighted_R3(a,k,d);
    case 4: return Yukawa_weighted_R4(a,k,d);
    case 5: return Yukawa_weighted_R5(a,k,d);
    case 6: return Yukawa_weighted_R6(a,k,d);
    case 7: return Yukawa_weighted_R7(a,k,d);
    case 8: return Yukawa_weighted_R8(a,k,d);
    case 9: return Yukawa_weighted_R9(a,k,d);
    //
    case 10: return Yukawa_weighted_R10(a,k,d);
    case 11: return Yukawa_weighted_R11(a,k,d);
    case 12: return Yukawa_weighted_R12(a,k,d);
    case 13: return Yukawa_weighted_R13(a,k,d);
    case 14: return Yukawa_weighted_R14(a,k,d);
    case 15: return Yukawa_weighted_R15(a,k,d);
    case 16: return Yukawa_weighted_R16(a,k,d);
    case 17: return Yukawa_weighted_R17(a,k,d);
    case 18: return Yukawa_weighted_R18(a,k,d);
    case 19: return Yukawa_weighted_R19(a,k,d);
    //
    case 20: return Yukawa_weighted_R20(a,k,d);
    case 21: return Yukawa_weighted_R21(a,k,d);
    case 22: return Yukawa_weighted_R22(a,k,d);
    case 23: return Yukawa_weighted_R23(a,k,d);
    case 24: return Yukawa_weighted_R24(a,k,d);
    case 25: return Yukawa_weighted_R25(a,k,d);
    case 26: return Yukawa_weighted_R26(a,k,d);
    case 27: return Yukawa_weighted_R27(a,k,d);
    case 28: return Yukawa_weighted_R28(a,k,d);
    case 29: return Yukawa_weighted_R29(a,k,d);
    //
    case 30: return Yukawa_weighted_R30(a,k,d);
    case 31: return Yukawa_weighted_R31(a,k,d);
    case 32: return Yukawa_weighted_R32(a,k,d);
    case 33: return Yukawa_weighted_R33(a,k,d);
    case 34: return Yukawa_weighted_R34(a,k,d);
    case 35: return Yukawa_weighted_R35(a,k,d);
    case 36: return Yukawa_weighted_R36(a,k,d);
    case 37: return Yukawa_weighted_R37(a,k,d);
    case 38: return Yukawa_weighted_R38(a,k,d);
    case 39: return Yukawa_weighted_R39(a,k,d);
    //
    //
    case 40: return Yukawa_weighted_R40(a,k,d);
    case 41: return Yukawa_weighted_R41(a,k,d);
    case 42: return Yukawa_weighted_R42(a,k,d);
    case 43: return Yukawa_weighted_R43(a,k,d);
    case 44: return Yukawa_weighted_R44(a,k,d);
    case 45: return Yukawa_weighted_R45(a,k,d);
    case 46: return Yukawa_weighted_R46(a,k,d);
    case 47: return Yukawa_weighted_R47(a,k,d);
    case 48: return Yukawa_weighted_R48(a,k,d);
    case 49: return Yukawa_weighted_R49(a,k,d);
    //
    case 50: return Yukawa_weighted_R50(a,k,d);
    case 51: return Yukawa_weighted_R51(a,k,d);
    case 52: return Yukawa_weighted_R52(a,k,d);
    case 53: return Yukawa_weighted_R53(a,k,d);
    case 54: return Yukawa_weighted_R54(a,k,d);
    case 55: return Yukawa_weighted_R55(a,k,d);
    case 56: return Yukawa_weighted_R56(a,k,d);
    case 57: return Yukawa_weighted_R57(a,k,d);
    case 58: return Yukawa_weighted_R58(a,k,d);
    case 59: return Yukawa_weighted_R59(a,k,d);
    //
    case 60: return Yukawa_weighted_R60(a,k,d);
    case 61: return Yukawa_weighted_R61(a,k,d);
    case 62: return Yukawa_weighted_R62(a,k,d);
    case 63: return Yukawa_weighted_R63(a,k,d);
    case 64: return Yukawa_weighted_R64(a,k,d);
    case 65: return Yukawa_weighted_R65(a,k,d);
    case 66: return Yukawa_weighted_R66(a,k,d);
    case 67: return Yukawa_weighted_R67(a,k,d);
    case 68: return Yukawa_weighted_R68(a,k,d);
    case 69: return Yukawa_weighted_R69(a,k,d);
    //
    case 70: return Yukawa_weighted_R70(a,k,d);
    case 71: return Yukawa_weighted_R71(a,k,d);
    case 72: return Yukawa_weighted_R72(a,k,d);
    case 73: return Yukawa_weighted_R73(a,k,d);
    case 74: return Yukawa_weighted_R74(a,k,d);
    case 75: return Yukawa_weighted_R75(a,k,d);
    case 76: return Yukawa_weighted_R76(a,k,d);
    case 77: return Yukawa_weighted_R77(a,k,d);
    case 78: return Yukawa_weighted_R78(a,k,d);
    case 79: return Yukawa_weighted_R79(a,k,d);
    //
    case 80: return Yukawa_weighted_R80(a,k,d);
    //
    default: throw( std::invalid_argument("wrong function order, detected in Yukawa_weighted_R") );
  }
} 

#endif
