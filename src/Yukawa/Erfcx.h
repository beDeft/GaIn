/*  Copyright (C) 2021 Ivan Duchemin.
 *  
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program. If not, see <http://www.gnu.org/licenses/>
 */
 
#ifndef ERFCX_H
#define ERFCX_H

//> implements exp(z^2)*erfc(z) for complex z argument
///
/// Uses algorithm proposed by Mohammad Al Azah and Simon N Chandler-Wilde in
///
/// "Computation of the Complex Error Function using Modified Trapezoidal Rules"
///
namespace erfcx_util {
  
  namespace constants
  {
    using std::pow;
    using std::sqrt;

    // t_k constant
    // (k+1/2)*h
    template <class  PREC,
              size_t N,
              size_t K>
    const PREC tk2   = pow(PREC( sqrt( PREC(4)*atan(PREC(1)) / PREC(N+1) )*( PREC(K) + PREC(1)/PREC(2) ) ),2);  
  
    // tau_k constant
    // k*h
    template <class  PREC,
              size_t N,
              size_t K>
    const PREC tauk2 = pow(PREC( sqrt( PREC(4)*atan(PREC(1)) / PREC(N+1) )*( PREC(K) ) ),2);  
  }  

  template <class  PREC, size_t N, size_t K>
  struct sum_helper 
  {
    //
    // sum over t_k constants
    //
    static inline PREC tk_sum(const PREC z2)
    {
      // unroll sum
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tk2<REAL,N,K>)/(z2-constants::tk2<REAL,N,K>)+sum_helper<PREC,N,K+1>::tk_sum(z2);
    }
    
    //
    // sum over tau_k constants
    //
    static inline PREC tauk_sum(const PREC z2)
    {
      // unroll sum
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tauk2<REAL,N,K>)/(z2-constants::tauk2<REAL,N,K>)+sum_helper<PREC,N,K+1>::tauk_sum(z2);
    }
  };
  
  // sum over t_k constants, stop specialization
  //
  template <class  PREC, size_t N>
  struct sum_helper<PREC,N,N> 
  {
    //
    // sum over t_k constants, return N^th term
    //
    static inline PREC tk_sum(const PREC z2)
    {
      // return first term
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tk2<REAL,N,N>)/(z2-constants::tk2<REAL,N,N>);
    }

    // sum over t_k constants, return N^th term
    //
    static inline PREC tauk_sum(const PREC z2)
    {
      // return first term
      using std::abs;
      using std::exp;
      using REAL = decltype( abs(std::declval<PREC&>()) );
      return exp(-constants::tauk2<REAL,N,N>)/(z2-constants::tauk2<REAL,N,N>);
    }
  };
  
  // W^M_N implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WMN(const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::sqrt;
    using std::atan;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const COMPLEX fact(REAL(0),REAL(2)*h/PI);
    return z * fact * sum_helper<COMPLEX,N,0>::tk_sum(z*z); 
  }

  // W^MM_N implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WMMN(const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::exp;
    using std::sqrt;
    using std::atan;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const COMPLEX fact(REAL(0),REAL(2)*PI/h);
    return REAL(2)*exp(-z*z)/(REAL(1)+exp(-fact*z)) + WMN<COMPLEX,N>(z); 
  }
 
  // W^MT_N implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WMTN(const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::exp;
    using std::sqrt;
    using std::atan;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const COMPLEX fact1(REAL(0),REAL(2)*PI/h);
    const COMPLEX fact2(REAL(0),REAL(1)*h/PI);
    const COMPLEX fact3(REAL(0),REAL(2)*h/PI);
    return REAL(2)*exp(-z*z)/(REAL(1)-exp(-fact1*z)) + fact2 / z + fact3 * z * sum_helper<COMPLEX,N,1>::tauk_sum(z*z); 
  }
 
  // W_N, first quadrant implementation : z=x+iy with x>=0 and y>=0
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WN_1st_quadrant(const COMPLEX z)
  {
    // precision of real type
    using std::abs;
    using std::max;
    using std::atan;
    using std::floor;
    using std::real;
    using std::imag;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // some constants
    const REAL    PI = REAL(4)*atan(REAL(1));
    const REAL    h  = sqrt(PI/REAL(N+1));
    const REAL    PI_over_h = PI/h;
    // form 1
    if ( imag(z) >= max(real(z),PI_over_h) ) return WMN<COMPLEX,N>(z);
    // form 2 and 3
    else
    {
      // fractional part phi
      REAL phi = real(z)/h - floor(real(z)/h);
      // form 2 
      if ( imag(z) < real(z) && REAL(1)/REAL(4) <= phi && phi <= REAL(3)/REAL(4) ) return WMTN<COMPLEX,N>(z);
    }
    // form 3
    return WMMN<COMPLEX,N>(z);
  }
  
  // W_N, full quadrant implementation
  //
  template <class  COMPLEX,
            size_t N>
  inline COMPLEX WN_full_quadrant(const COMPLEX z)
  {
    // precision of real type
    using std::exp;
    using std::abs;
    using std::real;
    using std::imag;
    using std::conj;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // 1st and 4th
    if ( real(z) >= REAL(0) )
    {
      // 1st 
      if ( imag(z) >= REAL(0) ) return WN_1st_quadrant<COMPLEX,N>(z);
      // 4th
      return conj( REAL(2)*exp( -conj(z*z) ) - WN_1st_quadrant<COMPLEX,N>( conj(z) ) );
    }
    // 2nd
    if ( imag(z) >= REAL(0) ) return conj( WN_1st_quadrant<COMPLEX,N>( -conj(z) ) );
    // 3rd
    return REAL(2)*exp( -z*z ) - WN_1st_quadrant<COMPLEX,N>( -z );
  }
}

template <class COMPLEX>
inline COMPLEX erfcx(const COMPLEX z) 
{
  // precision of real type
  using std::abs;
  using std::real;
  using std::imag;
  using REAL = decltype( abs(std::declval<COMPLEX&>()) );
  // call routine. using same amount of quadrature point as digits, to be on the safe side...
  return erfcx_util::WN_full_quadrant<COMPLEX,std::numeric_limits<REAL>::digits10>( COMPLEX( -imag(z) , real(z) ) );
}

template <>
inline std::complex<double> erfcx<std::complex<double>>(const std::complex<double> z) 
{ 
  return erfcx_util::WN_full_quadrant<std::complex<double>,12>( std::complex<double> ( -std::imag(z) , std::real(z) ) );
}

template <>
inline std::complex<long double> erfcx<std::complex<long double>>(const std::complex<long double> z) 
{ 
  return erfcx_util::WN_full_quadrant<std::complex<long double>,16>( std::complex<long double> ( -std::imag(z) , std::real(z) ) );
}


#endif
