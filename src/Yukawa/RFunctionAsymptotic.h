#ifndef YUKAWA_R_FUNCTION_ASYMPTOTIC_H
#define YUKAWA_R_FUNCTION_ASYMPTOTIC_H


//
// use struct meta for recursion
//
// compute R_order(z) as 1/(2z^2) * 1/(2*order-1)!! * sum_{k=0}^{depht} (-1)^k * (2*(order+k)+1)!! / (2*z^2)^k 
//
// use  (2*ORDER+1)/(2z^2) * ( -(2*(ORDER+1)+1)/(2z^2) * ( -(2*(ORDER+2)+1)/(2z^2) * (... * ( -(2*(ORDER+2)+DEPHT)/(2z^2) + 1 ) + 1 ) ... ) + 1 )    
//
template <class COMPLEX,
          int   ORDER,
          int   DEPHT,
          int   STAGE>
struct R_function_asymptotic_helper
{
  //
  // compute  1/(2*order-1)!! * sum_{k=0}^{depht} (-1)^k * (2*(order+k)+1)!! / (2*z^2)^k 
  //
  static inline COMPLEX eval(const COMPLEX one_over_2z2)
  {
    // get real type
    using std::abs;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // return recursion step  
    return REAL( 2*(ORDER+STAGE) + 1 ) * one_over_2z2 * ( REAL(1) - R_function_asymptotic_helper<COMPLEX,ORDER,DEPHT,STAGE+1>::eval(one_over_2z2) );
  } 
};

template <class COMPLEX,
          int   ORDER,
          int   DEPHT>
struct R_function_asymptotic_helper<COMPLEX,ORDER,DEPHT,DEPHT>
{
  //
  // compute  1/(2*order-1)!! * sum_{k=0}^{depht} (-1)^k * (2*(order+k)+1)!! / (2*z^2)^k 
  //
  static inline COMPLEX eval(const COMPLEX one_over_2z2)
  {
    // get real type
    using std::abs;
    using REAL = decltype( abs(std::declval<COMPLEX&>()) );
    // return recursion step  
    return REAL( 2*(ORDER+DEPHT) + 1 ) * one_over_2z2; 
  } 
};


//
// R function definition  
//
template <int   ORDER,
          int   DEPHT,
          class COMPLEX>
inline COMPLEX Yukawa_R_asymptotic( const COMPLEX z )
{
  // get real type
  using std::abs;
  using REAL = decltype( abs(std::declval<COMPLEX&>()) );
  // compute value through helper
  return R_function_asymptotic_helper<COMPLEX,ORDER,DEPHT,0>::eval(REAL(1)/(REAL(2)*z*z));
} 

#endif
