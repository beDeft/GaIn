
!> Derivative with respect to x of a Solid real spherical harmonic centered in (0,0,0) at point r
!! 
recursive function dYdx(a,l,m,r)
  
  implicit none
  
  real(kind=8), intent(in)                :: a !< a exponent for Ylm
  integer     , intent(in)                :: l !< l for Ylm, l must be < 6
  integer     , intent(in)                :: m !< m for Ylm
  real(kind=8), intent(in),dimension(3)   :: r !< relative position in space
  
  ! return value
  real(kind=8)                            :: dYdx
  
  ! constants
  real(kind=8), parameter :: PI=4.0d0*atan(1.0d0) 
  
  ! local variables
  real(kind=8) :: x
  real(kind=8) :: y
  real(kind=8) :: z
  real(kind=8) :: r2
  
  ! set coordinates
  x=r(1)
  y=r(2)
  z=r(3)
  r2=x**2+y**2+z**2
  
  ! selection on l
  select case (l)
    case (0)
      ! selection on m: l=0
      select case (m)
        case (0)
          dYdx=(-5.64189583547756287d-1*a*x)*exp(-a*r2)
        case default
          print *,'Error: dYdx not implemented for l=',l,'m=',m
          stop
      end select
    case (1)
      ! selection on m: l=1
      select case (m)
        case (-1)
          dYdx=(-9.77205023805839844d-1*a*x*y)*exp(-a*r2)
        case (0)
          dYdx=(-9.77205023805839844d-1*a*x*z)*exp(-a*r2)
        case (1)
          dYdx=(-4.88602511902919922d-1*(2.0d0*a*x**2-1.0d0))*exp(-a*r2)
        case default
          print *,'Error: dYdx not implemented for l=',l,'m=',m
          stop
      end select
    case (2)
      ! selection on m: l=2
      select case (m)
        case (-2)
          dYdx=(-1.09254843059207907d0*(2.0d0*a*x**2-1.0d0)*y)*exp(-a*r2)
        case (-1)
          dYdx=(-2.18509686118415814d0*a*x*y*z)*exp(-a*r2)
        case (0)
          dYdx=(-6.30783130505040012d-1*x*(1.0d0*a*(3.0d0*z**2-1.0d0*r2)+1.0d0))*exp(-a*r2)
        case (1)
          dYdx=(-1.09254843059207907d0*(2.0d0*a*x**2-1.0d0)*z)*exp(-a*r2)
        case (2)
          dYdx=(1.09254843059207907d0*x*(1.0d0*a*(y+x)*(y-1.0d0*x)+1.0d0))*exp(-a*r2)
        case default
          print *,'Error: dYdx not implemented for l=',l,'m=',m
          stop
      end select
    case (3)
      ! selection on m: l=3
      select case (m)
        case (-3)
          dYdx=(1.18008717985328702d0*x*y*(1.0d0*a*(y**2-3.0d0*x**2)+3.0d0))*exp(-a*r2)
        case (-2)
          dYdx=(-2.89061144264055406d0*(2.0d0*a*x**2-1.0d0)*y*z)*exp(-a*r2)
        case (-1)
          dYdx=(-9.14091598928931473d-1*x*y*(1.0d0*a*(5.0d0*z**2-1.0d0*r2)+1.0d0))*exp(-a*r2)
        case (0)
          dYdx=(-7.46352665180230783d-1*x*z*(1.0d0*a*(5.0d0*z**2-3.0d0*r2)+3.0d0))*exp(-a*r2)
        case (1)
          dYdx=(-4.57045799464465736d-1*(1.0d0*(1.0d1*a*x**2-5.0d0)*z**2+1.0d0*(2.0d0-2.0d0*a*r2)*x**2+r2))*exp(-a*r2)
        case (2)
          dYdx=(2.89061144264055406d0*x*(1.0d0*a*(y+x)*(y-1.0d0*x)+1.0d0)*z)*exp(-a*r2)
        case (3)
          dYdx=(5.90043589926643511d-1*(1.0d0*(6.0d0*a*x**2-3.0d0)*y**2-2.0d0*a*x**4+3.0d0*x**2))*exp(-a*r2)
        case default
          print *,'Error: dYdx not implemented for l=',l,'m=',m
          stop
      end select
    case (4)
      ! selection on m: l=4
      select case (m)
        case (-4)
          dYdx=(2.50334294179670454d0*y*(1.0d0*(2.0d0*a*x**2-1.0d0)*y**2-2.0d0*a*x**4+3.0d0*x**2))*exp(-a*r2)
        case (-3)
          dYdx=(3.54026153955986106d0*x*y*(1.0d0*a*(y**2-3.0d0*x**2)+3.0d0)*z)*exp(-a*r2)
        case (-2)
          dYdx=(-9.46174695757560018d-1*y*(1.0d0*(1.4d1*a*x**2-7.0d0)*z**2+1.0d0*(2.0d0-2.0d0*a*r2)*x**2+r2))*exp(-a*r2)
        case (-1)
          dYdx=(-1.33809308711457834d0*x*y*z*(1.0d0*a*(7.0d0*z**2-3.0d0*r2)+3.0d0))*exp(-a*r2)
        case (0)
          dYdx=(-2.11571093830408608d-1*x*(3.5d1*a*z**4+1.0d0*(3.0d1-3.0d1*a*r2)*z**2-6.0d0*r2+3.0d0*a*r2**2))*exp(-a*r2)
        case (1)
          dYdx=(-6.69046543557289168d-1*z*(1.0d0*(1.4d1*a*x**2-7.0d0)*z**2+1.0d0*(6.0d0-6.0d0*a*r2)*x**2+3.0d0*r2))*exp(-a*r2)
        case (2)
          dYdx=(9.46174695757560018d-1*x*(1.0d0*(7.0d0*a*y**2-7.0d0*a*x**2+7.0d0)*z**2+1.0d0*(1.0d0-1.0d0*a*r2)*y**2+1.0d0* &
(1.0d0*a*r2-1.0d0)*x**2-1.0d0*r2))*exp(-a*r2)
        case (3)
          dYdx=(1.77013076977993053d0*(1.0d0*(6.0d0*a*x**2-3.0d0)*y**2-2.0d0*a*x**4+3.0d0*x**2)*z)*exp(-a*r2)
        case (4)
          dYdx=(-1.25167147089835227d0*x*(1.0d0*a*y**4+1.0d0*(6.0d0-6.0d0*a*x**2)*y**2+1.0d0*a*x**4-2.0d0*x**2))*exp(-a*r2)
        case default
          print *,'Error: dYdx not implemented for l=',l,'m=',m
          stop
      end select
    case (5)
      ! selection on m: l=5
      select case (m)
        case (-5)
          dYdx=(-1.31276411368034021d0*x*y*(1.0d0*a*y**4+1.0d0*(1.0d1-1.0d1*a*x**2)*y**2+5.0d0*a*x**4-1.0d1*x**2))*exp(-a*r2)
        case (-4)
          dYdx=(8.30264925952416511d0*y*(1.0d0*(2.0d0*a*x**2-1.0d0)*y**2-2.0d0*a*x**4+3.0d0*x**2)*z)*exp(-a*r2)
        case (-3)
          dYdx=(9.78476598870500776d-1*x*y*(1.0d0*(9.0d0*a*y**2-2.7d1*a*x**2+2.7d1)*z**2+1.0d0*(1.0d0-1.0d0*a*r2)*y**2+1.0d0* &
(3.0d0*a*r2-3.0d0)*x**2-3.0d0*r2))*exp(-a*r2)
        case (-2)
          dYdx=(-4.79353678497332376d0*y*z*(1.0d0*(6.0d0*a*x**2-3.0d0)*z**2+1.0d0*(2.0d0-2.0d0*a*r2)*x**2+r2))*exp(-a*r2)
        case (-1)
          dYdx=(-9.05893302391393843d-1*x*y*(2.1d1*a*z**4+1.0d0*(1.4d1-1.4d1*a*r2)*z**2-2.0d0*r2+1.0d0*a*r2**2))*exp(-a*r2)
        case (0)
          dYdx=(-2.33900644906847193d-1*x*z*(6.3d1*a*z**4+1.0d0*(7.0d1-7.0d1*a*r2)*z**2-3.0d1*r2+1.5d1*a*r2**2))*exp(-a*r2)
        case (1)
          dYdx=(-4.52946651195696922d-1*(1.0d0*(4.2d1*a*x**2-2.1d1)*z**4+1.0d0*(1.0d0*(2.8d1-2.8d1*a*r2)*x**2+1.4d1*r2)*z**2 &
+1.0d0*(2.0d0*a*r2**2-4.0d0*r2)*x**2-1.0d0*r2**2))*exp(-a*r2)
        case (2)
          dYdx=(4.79353678497332376d0*x*z*(1.0d0*(3.0d0*a*y**2-3.0d0*a*x**2+3.0d0)*z**2+1.0d0*(1.0d0-1.0d0*a*r2)*y**2+1.0d0* &
(1.0d0*a*r2-1.0d0)*x**2-1.0d0*r2))*exp(-a*r2)
        case (3)
          dYdx=(4.89238299435250388d-1*(1.0d0*(1.0d0*(5.4d1*a*x**2-2.7d1)*y**2-1.8d1*a*x**4+2.7d1*x**2)*z**2+1.0d0*(1.0d0*(6.0d0 &
-6.0d0*a*r2)*x**2+3.0d0*r2)*y**2+1.0d0*(2.0d0*a*r2-2.0d0)*x**4-3.0d0*r2*x**2))*exp(-a*r2)
        case (4)
          dYdx=(-4.15132462976208256d0*x*(1.0d0*a*y**4+1.0d0*(6.0d0-6.0d0*a*x**2)*y**2+1.0d0*a*x**4-2.0d0*x**2)*z)*exp(-a*r2)
        case (5)
          dYdx=(-6.56382056840170103d-1*(1.0d0*(1.0d1*a*x**2-5.0d0)*y**4+1.0d0*(3.0d1*x**2-2.0d1*a*x**4)*y**2+2.0d0*a*x**6 &
-5.0d0*x**4))*exp(-a*r2)
        case default
          print *,'Error: dYdx not implemented for l=',l,'m=',m
          stop
      end select
    case (6)
      ! selection on m: l=6
      select case (m)
        case (-6)
          dYdx=(-1.36636821038382864d0*y*(1.0d0*(6.0d0*a*x**2-3.0d0)*y**4+1.0d0*(3.0d1*x**2-2.0d1*a*x**4)*y**2+6.0d0*a*x**6 &
-1.5d1*x**4))*exp(-a*r2)
        case (-5)
          dYdx=(-4.73323832446350407d0*x*y*(1.0d0*a*y**4+1.0d0*(1.0d1-1.0d1*a*x**2)*y**2+5.0d0*a*x**4-1.0d1*x**2)*z)*exp(-a*r2)
        case (-4)
          dYdx=(2.01825960291489664d0*y*(1.0d0*(1.0d0*(2.2d1*a*x**2-1.1d1)*y**2-2.2d1*a*x**4+3.3d1*x**2)*z**2+1.0d0*(1.0d0*(2.0d0 &
-2.0d0*a*r2)*x**2+r2)*y**2+1.0d0*(2.0d0*a*r2-2.0d0)*x**4-3.0d0*r2*x**2))*exp(-a*r2)
        case (-3)
          dYdx=(1.842410519029847d0*x*y*z*(1.0d0*(1.1d1*a*y**2-3.3d1*a*x**2+3.3d1)*z**2+1.0d0*(3.0d0-3.0d0*a*r2)*y**2+1.0d0* &
(9.0d0*a*r2-9.0d0)*x**2-9.0d0*r2))*exp(-a*r2)
        case (-2)
          dYdx=(-9.21205259514923499d-1*y*(1.0d0*(6.6d1*a*x**2-3.3d1)*z**4+1.0d0*(1.0d0*(3.6d1-3.6d1*a*r2)*x**2+1.8d1*r2)*z**2 &
+1.0d0*(2.0d0*a*r2**2-4.0d0*r2)*x**2-1.0d0*r2**2))*exp(-a*r2)
        case (-1)
          dYdx=(-1.16524272503746278d0*x*y*z*(3.3d1*a*z**4+1.0d0*(3.0d1-3.0d1*a*r2)*z**2-1.0d1*r2+5.0d0*a*r2**2))*exp(-a*r2)
        case (0)
          dYdx=(-1.27138404535256852d-1*x*(2.31d2*a*z**6+1.0d0*(3.15d2-3.15d2*a*r2)*z**4+1.0d0*(1.05d2*a*r2**2-2.1d2*r2)*z**2 &
-5.0d0*a*r2**3+1.5d1*r2**2))*exp(-a*r2)
        case (1)
          dYdx=(-5.82621362518731389d-1*z*(1.0d0*(6.6d1*a*x**2-3.3d1)*z**4+1.0d0*(1.0d0*(6.0d1-6.0d1*a*r2)*x**2+3.0d1*r2)*z**2 &
+1.0d0*(1.0d1*a*r2**2-2.0d1*r2)*x**2-5.0d0*r2**2))*exp(-a*r2)
        case (2)
          dYdx=(9.21205259514923499d-1*x*(1.0d0*(3.3d1*a*y**2-3.3d1*a*x**2+3.3d1)*z**4+1.0d0*(1.0d0*(1.8d1-1.8d1*a*r2)*y**2+1.0d0* &
(1.8d1*a*r2-1.8d1)*x**2-1.8d1*r2)*z**2+1.0d0*(1.0d0*a*r2**2-2.0d0*r2)*y**2+1.0d0*(2.0d0*r2-1.0d0*a*r2**2)*x**2+r2**2))*exp(-a*r2)
        case (3)
          dYdx=(9.21205259514923499d-1*z*(1.0d0*(1.0d0*(6.6d1*a*x**2-3.3d1)*y**2-2.2d1*a*x**4+3.3d1*x**2)*z**2+1.0d0*(1.0d0*(1.8d1 &
-1.8d1*a*r2)*x**2+9.0d0*r2)*y**2+1.0d0*(6.0d0*a*r2-6.0d0)*x**4-9.0d0*r2*x**2))*exp(-a*r2)
        case (4)
          dYdx=(-1.00912980145744832d0*x*(1.0d0*(1.1d1*a*y**4+1.0d0*(6.6d1-6.6d1*a*x**2)*y**2+1.1d1*a*x**4-2.2d1*x**2)*z**2+1.0d0* &
(1.0d0-1.0d0*a*r2)*y**4+1.0d0*(1.0d0*(6.0d0*a*r2-6.0d0)*x**2-6.0d0*r2)*y**2+1.0d0*(1.0d0-1.0d0*a*r2)*x**4+2.0d0*r2*x**2))*exp( &
-a*r2)
        case (5)
          dYdx=(-2.36661916223175203d0*(1.0d0*(1.0d1*a*x**2-5.0d0)*y**4+1.0d0*(3.0d1*x**2-2.0d1*a*x**4)*y**2+2.0d0*a*x**6 &
-5.0d0*x**4)*z)*exp(-a*r2)
        case (6)
          dYdx=(1.36636821038382864d0*x*(1.0d0*a*y**6+1.0d0*(1.5d1-1.5d1*a*x**2)*y**4+1.0d0*(1.5d1*a*x**4-3.0d1*x**2)*y**2 &
-1.0d0*a*x**6+3.0d0*x**4))*exp(-a*r2)
        case default
          print *,'Error: dYdx not implemented for l=',l,'m=',m
          stop
      end select
    case default
      print *,'Error: dYdx not implemented for l=',l,'m=',m
      stop
  end select
  
end function
!> Derivative with respect to x of a Solid real spherical harmonic centered in (0,0,0) at point r
!! 
recursive function dYdy(a,l,m,r)
  
  implicit none
  
  real(kind=8), intent(in)                :: a !< a exponent for Ylm
  integer     , intent(in)                :: l !< l for Ylm, l must be < 6
  integer     , intent(in)                :: m !< m for Ylm
  real(kind=8), intent(in),dimension(3)   :: r !< relative position in space
  
  ! return value
  real(kind=8)                            :: dYdy
  
  ! constants
  real(kind=8), parameter :: PI=4.0d0*atan(1.0d0) 
  
  ! local variables
  real(kind=8) :: x
  real(kind=8) :: y
  real(kind=8) :: z
  real(kind=8) :: r2
  
  ! set coordinates
  x=r(1)
  y=r(2)
  z=r(3)
  r2=x**2+y**2+z**2
  
  ! selection on l
  select case (l)
    case (0)
      ! selection on m: l=0
      select case (m)
        case (0)
          dYdy=(-5.64189583547756287d-1*a*y)*exp(-a*r2)
        case default
          print *,'Error: dYdy not implemented for l=',l,'m=',m
          stop
      end select
    case (1)
      ! selection on m: l=1
      select case (m)
        case (-1)
          dYdy=(-4.88602511902919922d-1*(2.0d0*a*y**2-1.0d0))*exp(-a*r2)
        case (0)
          dYdy=(-9.77205023805839844d-1*a*y*z)*exp(-a*r2)
        case (1)
          dYdy=(-9.77205023805839844d-1*a*x*y)*exp(-a*r2)
        case default
          print *,'Error: dYdy not implemented for l=',l,'m=',m
          stop
      end select
    case (2)
      ! selection on m: l=2
      select case (m)
        case (-2)
          dYdy=(-1.09254843059207907d0*x*(2.0d0*a*y**2-1.0d0))*exp(-a*r2)
        case (-1)
          dYdy=(-1.09254843059207907d0*(2.0d0*a*y**2-1.0d0)*z)*exp(-a*r2)
        case (0)
          dYdy=(-6.30783130505040012d-1*y*(1.0d0*a*(3.0d0*z**2-1.0d0*r2)+1.0d0))*exp(-a*r2)
        case (1)
          dYdy=(-2.18509686118415814d0*a*x*y*z)*exp(-a*r2)
        case (2)
          dYdy=(1.09254843059207907d0*y*(1.0d0*a*(y+x)*(y-1.0d0*x)-1.0d0))*exp(-a*r2)
        case default
          print *,'Error: dYdy not implemented for l=',l,'m=',m
          stop
      end select
    case (3)
      ! selection on m: l=3
      select case (m)
        case (-3)
          dYdy=(5.90043589926643511d-1*(2.0d0*a*y**4+1.0d0*((-6.0d0*a*x**2)-3.0d0)*y**2+3.0d0*x**2))*exp(-a*r2)
        case (-2)
          dYdy=(-2.89061144264055406d0*x*(2.0d0*a*y**2-1.0d0)*z)*exp(-a*r2)
        case (-1)
          dYdy=(-4.57045799464465736d-1*(1.0d0*(1.0d1*a*y**2-5.0d0)*z**2+1.0d0*(2.0d0-2.0d0*a*r2)*y**2+r2))*exp(-a*r2)
        case (0)
          dYdy=(-7.46352665180230783d-1*y*z*(1.0d0*a*(5.0d0*z**2-3.0d0*r2)+3.0d0))*exp(-a*r2)
        case (1)
          dYdy=(-9.14091598928931473d-1*x*y*(1.0d0*a*(5.0d0*z**2-1.0d0*r2)+1.0d0))*exp(-a*r2)
        case (2)
          dYdy=(2.89061144264055406d0*y*(1.0d0*a*(y+x)*(y-1.0d0*x)-1.0d0)*z)*exp(-a*r2)
        case (3)
          dYdy=(1.18008717985328702d0*x*y*(1.0d0*a*(3.0d0*y**2-1.0d0*x**2)-3.0d0))*exp(-a*r2)
        case default
          print *,'Error: dYdy not implemented for l=',l,'m=',m
          stop
      end select
    case (4)
      ! selection on m: l=4
      select case (m)
        case (-4)
          dYdy=(2.50334294179670454d0*x*(2.0d0*a*y**4+1.0d0*((-2.0d0*a*x**2)-3.0d0)*y**2+x**2))*exp(-a*r2)
        case (-3)
          dYdy=(1.77013076977993053d0*(2.0d0*a*y**4+1.0d0*((-6.0d0*a*x**2)-3.0d0)*y**2+3.0d0*x**2)*z)*exp(-a*r2)
        case (-2)
          dYdy=(-9.46174695757560018d-1*x*(1.0d0*(1.4d1*a*y**2-7.0d0)*z**2+1.0d0*(2.0d0-2.0d0*a*r2)*y**2+r2))*exp(-a*r2)
        case (-1)
          dYdy=(-6.69046543557289168d-1*z*(1.0d0*(1.4d1*a*y**2-7.0d0)*z**2+1.0d0*(6.0d0-6.0d0*a*r2)*y**2+3.0d0*r2))*exp(-a*r2)
        case (0)
          dYdy=(-2.11571093830408608d-1*y*(3.5d1*a*z**4+1.0d0*(3.0d1-3.0d1*a*r2)*z**2-6.0d0*r2+3.0d0*a*r2**2))*exp(-a*r2)
        case (1)
          dYdy=(-1.33809308711457834d0*x*y*z*(1.0d0*a*(7.0d0*z**2-3.0d0*r2)+3.0d0))*exp(-a*r2)
        case (2)
          dYdy=(9.46174695757560018d-1*y*(1.0d0*(7.0d0*a*y**2-7.0d0*a*x**2-7.0d0)*z**2+1.0d0*(1.0d0-1.0d0*a*r2)*y**2+1.0d0* &
(1.0d0*a*r2-1.0d0)*x**2+r2))*exp(-a*r2)
        case (3)
          dYdy=(3.54026153955986106d0*x*y*(1.0d0*a*(3.0d0*y**2-1.0d0*x**2)-3.0d0)*z)*exp(-a*r2)
        case (4)
          dYdy=(-1.25167147089835227d0*y*(1.0d0*a*y**4+1.0d0*((-6.0d0*a*x**2)-2.0d0)*y**2+1.0d0*a*x**4+6.0d0*x**2))*exp(-a*r2)
        case default
          print *,'Error: dYdy not implemented for l=',l,'m=',m
          stop
      end select
    case (5)
      ! selection on m: l=5
      select case (m)
        case (-5)
          dYdy=(-6.56382056840170103d-1*(2.0d0*a*y**6+1.0d0*((-2.0d1*a*x**2)-5.0d0)*y**4+1.0d0*(1.0d1*a*x**4+3.0d1*x**2)*y**2 &
-5.0d0*x**4))*exp(-a*r2)
        case (-4)
          dYdy=(8.30264925952416511d0*x*(2.0d0*a*y**4+1.0d0*((-2.0d0*a*x**2)-3.0d0)*y**2+x**2)*z)*exp(-a*r2)
        case (-3)
          dYdy=(4.89238299435250388d-1*(1.0d0*(1.8d1*a*y**4+1.0d0*((-5.4d1*a*x**2)-2.7d1)*y**2+2.7d1*x**2)*z**2+1.0d0*(2.0d0 &
-2.0d0*a*r2)*y**4+1.0d0*(1.0d0*(6.0d0*a*r2-6.0d0)*x**2+3.0d0*r2)*y**2-3.0d0*r2*x**2))*exp(-a*r2)
        case (-2)
          dYdy=(-4.79353678497332376d0*x*z*(1.0d0*(6.0d0*a*y**2-3.0d0)*z**2+1.0d0*(2.0d0-2.0d0*a*r2)*y**2+r2))*exp(-a*r2)
        case (-1)
          dYdy=(-4.52946651195696922d-1*(1.0d0*(4.2d1*a*y**2-2.1d1)*z**4+1.0d0*(1.0d0*(2.8d1-2.8d1*a*r2)*y**2+1.4d1*r2)*z**2 &
+1.0d0*(2.0d0*a*r2**2-4.0d0*r2)*y**2-1.0d0*r2**2))*exp(-a*r2)
        case (0)
          dYdy=(-2.33900644906847193d-1*y*z*(6.3d1*a*z**4+1.0d0*(7.0d1-7.0d1*a*r2)*z**2-3.0d1*r2+1.5d1*a*r2**2))*exp(-a*r2)
        case (1)
          dYdy=(-9.05893302391393843d-1*x*y*(2.1d1*a*z**4+1.0d0*(1.4d1-1.4d1*a*r2)*z**2-2.0d0*r2+1.0d0*a*r2**2))*exp(-a*r2)
        case (2)
          dYdy=(4.79353678497332376d0*y*z*(1.0d0*(3.0d0*a*y**2-3.0d0*a*x**2-3.0d0)*z**2+1.0d0*(1.0d0-1.0d0*a*r2)*y**2+1.0d0* &
(1.0d0*a*r2-1.0d0)*x**2+r2))*exp(-a*r2)
        case (3)
          dYdy=(9.78476598870500776d-1*x*y*(1.0d0*(2.7d1*a*y**2-9.0d0*a*x**2-2.7d1)*z**2+1.0d0*(3.0d0-3.0d0*a*r2)*y**2+1.0d0* &
(1.0d0*a*r2-1.0d0)*x**2+3.0d0*r2))*exp(-a*r2)
        case (4)
          dYdy=(-4.15132462976208256d0*y*(1.0d0*a*y**4+1.0d0*((-6.0d0*a*x**2)-2.0d0)*y**2+1.0d0*a*x**4+6.0d0*x**2)*z)*exp(-a*r2)
        case (5)
          dYdy=(-1.31276411368034021d0*x*y*(5.0d0*a*y**4+1.0d0*((-1.0d1*a*x**2)-1.0d1)*y**2+1.0d0*a*x**4+1.0d1*x**2))*exp(-a*r2)
        case default
          print *,'Error: dYdy not implemented for l=',l,'m=',m
          stop
      end select
    case (6)
      ! selection on m: l=6
      select case (m)
        case (-6)
          dYdy=(-1.36636821038382864d0*x*(6.0d0*a*y**6+1.0d0*((-2.0d1*a*x**2)-1.5d1)*y**4+1.0d0*(6.0d0*a*x**4+3.0d1*x**2)*y**2 &
-3.0d0*x**4))*exp(-a*r2)
        case (-5)
          dYdy=(-2.36661916223175203d0*(2.0d0*a*y**6+1.0d0*((-2.0d1*a*x**2)-5.0d0)*y**4+1.0d0*(1.0d1*a*x**4+3.0d1*x**2)*y**2 &
-5.0d0*x**4)*z)*exp(-a*r2)
        case (-4)
          dYdy=(2.01825960291489664d0*x*(1.0d0*(2.2d1*a*y**4+1.0d0*((-2.2d1*a*x**2)-3.3d1)*y**2+1.1d1*x**2)*z**2+1.0d0*(2.0d0 &
-2.0d0*a*r2)*y**4+1.0d0*(1.0d0*(2.0d0*a*r2-2.0d0)*x**2+3.0d0*r2)*y**2-1.0d0*r2*x**2))*exp(-a*r2)
        case (-3)
          dYdy=(9.21205259514923499d-1*z*(1.0d0*(2.2d1*a*y**4+1.0d0*((-6.6d1*a*x**2)-3.3d1)*y**2+3.3d1*x**2)*z**2+1.0d0*(6.0d0 &
-6.0d0*a*r2)*y**4+1.0d0*(1.0d0*(1.8d1*a*r2-1.8d1)*x**2+9.0d0*r2)*y**2-9.0d0*r2*x**2))*exp(-a*r2)
        case (-2)
          dYdy=(-9.21205259514923499d-1*x*(1.0d0*(6.6d1*a*y**2-3.3d1)*z**4+1.0d0*(1.0d0*(3.6d1-3.6d1*a*r2)*y**2+1.8d1*r2)*z**2 &
+1.0d0*(2.0d0*a*r2**2-4.0d0*r2)*y**2-1.0d0*r2**2))*exp(-a*r2)
        case (-1)
          dYdy=(-5.82621362518731389d-1*z*(1.0d0*(6.6d1*a*y**2-3.3d1)*z**4+1.0d0*(1.0d0*(6.0d1-6.0d1*a*r2)*y**2+3.0d1*r2)*z**2 &
+1.0d0*(1.0d1*a*r2**2-2.0d1*r2)*y**2-5.0d0*r2**2))*exp(-a*r2)
        case (0)
          dYdy=(-1.27138404535256852d-1*y*(2.31d2*a*z**6+1.0d0*(3.15d2-3.15d2*a*r2)*z**4+1.0d0*(1.05d2*a*r2**2-2.1d2*r2)*z**2 &
-5.0d0*a*r2**3+1.5d1*r2**2))*exp(-a*r2)
        case (1)
          dYdy=(-1.16524272503746278d0*x*y*z*(3.3d1*a*z**4+1.0d0*(3.0d1-3.0d1*a*r2)*z**2-1.0d1*r2+5.0d0*a*r2**2))*exp(-a*r2)
        case (2)
          dYdy=(9.21205259514923499d-1*y*(1.0d0*(3.3d1*a*y**2-3.3d1*a*x**2-3.3d1)*z**4+1.0d0*(1.0d0*(1.8d1-1.8d1*a*r2)*y**2+1.0d0* &
(1.8d1*a*r2-1.8d1)*x**2+1.8d1*r2)*z**2+1.0d0*(1.0d0*a*r2**2-2.0d0*r2)*y**2+1.0d0*(2.0d0*r2-1.0d0*a*r2**2)*x**2-1.0d0*r2**2))*exp( &
-a*r2)
        case (3)
          dYdy=(1.842410519029847d0*x*y*z*(1.0d0*(3.3d1*a*y**2-1.1d1*a*x**2-3.3d1)*z**2+1.0d0*(9.0d0-9.0d0*a*r2)*y**2+1.0d0* &
(3.0d0*a*r2-3.0d0)*x**2+9.0d0*r2))*exp(-a*r2)
        case (4)
          dYdy=(-1.00912980145744832d0*y*(1.0d0*(1.1d1*a*y**4+1.0d0*((-6.6d1*a*x**2)-2.2d1)*y**2+1.1d1*a*x**4+6.6d1*x**2)*z**2 &
+1.0d0*(1.0d0-1.0d0*a*r2)*y**4+1.0d0*(1.0d0*(6.0d0*a*r2-6.0d0)*x**2+2.0d0*r2)*y**2+1.0d0*(1.0d0-1.0d0*a*r2)*x**4-6.0d0*r2*x**2) &
)*exp(-a*r2)
        case (5)
          dYdy=(-4.73323832446350407d0*x*y*(5.0d0*a*y**4+1.0d0*((-1.0d1*a*x**2)-1.0d1)*y**2+1.0d0*a*x**4+1.0d1*x**2)*z)*exp(-a*r2)
        case (6)
          dYdy=(1.36636821038382864d0*y*(1.0d0*a*y**6+1.0d0*((-1.5d1*a*x**2)-3.0d0)*y**4+1.0d0*(1.5d1*a*x**4+3.0d1*x**2)*y**2 &
-1.0d0*a*x**6-1.5d1*x**4))*exp(-a*r2)
        case default
          print *,'Error: dYdy not implemented for l=',l,'m=',m
          stop
      end select
    case default
      print *,'Error: dYdy not implemented for l=',l,'m=',m
      stop
  end select
  
end function
!> Derivative with respect to x of a Solid real spherical harmonic centered in (0,0,0) at point r
!! 
recursive function dYdz(a,l,m,r)
  
  implicit none
  
  real(kind=8), intent(in)                :: a !< a exponent for Ylm
  integer     , intent(in)                :: l !< l for Ylm, l must be < 6
  integer     , intent(in)                :: m !< m for Ylm
  real(kind=8), intent(in),dimension(3)   :: r !< relative position in space
  
  ! return value
  real(kind=8)                            :: dYdz
  
  ! constants
  real(kind=8), parameter :: PI=4.0d0*atan(1.0d0) 
  
  ! local variables
  real(kind=8) :: x
  real(kind=8) :: y
  real(kind=8) :: z
  real(kind=8) :: r2
  
  ! set coordinates
  x=r(1)
  y=r(2)
  z=r(3)
  r2=x**2+y**2+z**2
  
  ! selection on l
  select case (l)
    case (0)
      ! selection on m: l=0
      select case (m)
        case (0)
          dYdz=(-5.64189583547756287d-1*a*z)*exp(-a*r2)
        case default
          print *,'Error: dYdz not implemented for l=',l,'m=',m
          stop
      end select
    case (1)
      ! selection on m: l=1
      select case (m)
        case (-1)
          dYdz=(-9.77205023805839844d-1*a*y*z)*exp(-a*r2)
        case (0)
          dYdz=(-4.88602511902919922d-1*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (1)
          dYdz=(-9.77205023805839844d-1*a*x*z)*exp(-a*r2)
        case default
          print *,'Error: dYdz not implemented for l=',l,'m=',m
          stop
      end select
    case (2)
      ! selection on m: l=2
      select case (m)
        case (-2)
          dYdz=(-2.18509686118415814d0*a*x*y*z)*exp(-a*r2)
        case (-1)
          dYdz=(-1.09254843059207907d0*y*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (0)
          dYdz=(-6.30783130505040012d-1*z*(1.0d0*a*(3.0d0*z**2-1.0d0*r2)-2.0d0))*exp(-a*r2)
        case (1)
          dYdz=(-1.09254843059207907d0*x*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (2)
          dYdz=(1.09254843059207907d0*a*(y+x)*(y-1.0d0*x)*z)*exp(-a*r2)
        case default
          print *,'Error: dYdz not implemented for l=',l,'m=',m
          stop
      end select
    case (3)
      ! selection on m: l=3
      select case (m)
        case (-3)
          dYdz=(1.18008717985328702d0*a*y*(y**2-3.0d0*x**2)*z)*exp(-a*r2)
        case (-2)
          dYdz=(-2.89061144264055406d0*x*y*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (-1)
          dYdz=(-9.14091598928931473d-1*y*z*(1.0d0*a*(5.0d0*z**2-1.0d0*r2)-4.0d0))*exp(-a*r2)
        case (0)
          dYdz=(-3.73176332590115392d-1*(1.0d1*a*z**4+1.0d0*((-6.0d0*a*r2)-9.0d0)*z**2+3.0d0*r2))*exp(-a*r2)
        case (1)
          dYdz=(-9.14091598928931473d-1*x*z*(1.0d0*a*(5.0d0*z**2-1.0d0*r2)-4.0d0))*exp(-a*r2)
        case (2)
          dYdz=(1.44530572132027703d0*(y+x)*(y-1.0d0*x)*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (3)
          dYdz=(1.18008717985328702d0*a*x*(3.0d0*y**2-1.0d0*x**2)*z)*exp(-a*r2)
        case default
          print *,'Error: dYdz not implemented for l=',l,'m=',m
          stop
      end select
    case (4)
      ! selection on m: l=4
      select case (m)
        case (-4)
          dYdz=(5.00668588359340908d0*a*x*y*(y+x)*(y-1.0d0*x)*z)*exp(-a*r2)
        case (-3)
          dYdz=(1.77013076977993053d0*y*(y**2-3.0d0*x**2)*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (-2)
          dYdz=(-1.89234939151512004d0*x*y*z*(1.0d0*a*(7.0d0*z**2-1.0d0*r2)-6.0d0))*exp(-a*r2)
        case (-1)
          dYdz=(-6.69046543557289168d-1*y*(1.4d1*a*z**4+1.0d0*((-6.0d0*a*r2)-1.5d1)*z**2+3.0d0*r2))*exp(-a*r2)
        case (0)
          dYdz=(-2.11571093830408608d-1*z*(3.5d1*a*z**4+1.0d0*((-3.0d1*a*r2)-4.0d1)*z**2+2.4d1*r2+3.0d0*a*r2**2))*exp(-a*r2)
        case (1)
          dYdz=(-6.69046543557289168d-1*x*(1.4d1*a*z**4+1.0d0*((-6.0d0*a*r2)-1.5d1)*z**2+3.0d0*r2))*exp(-a*r2)
        case (2)
          dYdz=(9.46174695757560018d-1*(y+x)*(y-1.0d0*x)*z*(1.0d0*a*(7.0d0*z**2-1.0d0*r2)-6.0d0))*exp(-a*r2)
        case (3)
          dYdz=(1.77013076977993053d0*x*(3.0d0*y**2-1.0d0*x**2)*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (4)
          dYdz=(-1.25167147089835227d0*a*(y**2-2.0d0*x*y-1.0d0*x**2)*(y**2+2.0d0*x*y-1.0d0*x**2)*z)*exp(-a*r2)
        case default
          print *,'Error: dYdz not implemented for l=',l,'m=',m
          stop
      end select
    case (5)
      ! selection on m: l=5
      select case (m)
        case (-5)
          dYdz=(-1.31276411368034021d0*a*y*(y**4-1.0d1*x**2*y**2+5.0d0*x**4)*z)*exp(-a*r2)
        case (-4)
          dYdz=(8.30264925952416511d0*x*y*(y+x)*(y-1.0d0*x)*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (-3)
          dYdz=(9.78476598870500776d-1*y*(y**2-3.0d0*x**2)*z*(1.0d0*a*(9.0d0*z**2-r2)-8.0d0))*exp(-a*r2)
        case (-2)
          dYdz=(-4.79353678497332376d0*x*y*(6.0d0*a*z**4+1.0d0*((-2.0d0*a*r2)-7.0d0)*z**2+r2))*exp(-a*r2)
        case (-1)
          dYdz=(-9.05893302391393843d-1*y*z*(2.1d1*a*z**4+1.0d0*((-1.4d1*a*r2)-2.8d1)*z**2+1.2d1*r2+1.0d0*a*r2**2))*exp(-a*r2)
        case (0)
          dYdz=(-1.16950322453423597d-1*(1.26d2*a*z**6+1.0d0*((-1.4d2*a*r2)-1.75d2)*z**4+1.0d0*(1.5d2*r2+3.0d1*a*r2**2)*z**2 &
-1.5d1*r2**2))*exp(-a*r2)
        case (1)
          dYdz=(-9.05893302391393843d-1*x*z*(2.1d1*a*z**4+1.0d0*((-1.4d1*a*r2)-2.8d1)*z**2+1.2d1*r2+1.0d0*a*r2**2))*exp(-a*r2)
        case (2)
          dYdz=(2.39676839248666188d0*(y+x)*(y-1.0d0*x)*(6.0d0*a*z**4+1.0d0*((-2.0d0*a*r2)-7.0d0)*z**2+r2))*exp(-a*r2)
        case (3)
          dYdz=(9.78476598870500776d-1*x*(3.0d0*y**2-1.0d0*x**2)*z*(1.0d0*a*(9.0d0*z**2-r2)-8.0d0))*exp(-a*r2)
        case (4)
          dYdz=(-2.07566231488104128d0*(y**2-2.0d0*x*y-1.0d0*x**2)*(y**2+2.0d0*x*y-1.0d0*x**2)*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (5)
          dYdz=(-1.31276411368034021d0*a*x*(5.0d0*y**4-1.0d1*x**2*y**2+x**4)*z)*exp(-a*r2)
        case default
          print *,'Error: dYdz not implemented for l=',l,'m=',m
          stop
      end select
    case (6)
      ! selection on m: l=6
      select case (m)
        case (-6)
          dYdz=(-2.73273642076765729d0*a*x*y*(y**2-3.0d0*x**2)*(3.0d0*y**2-1.0d0*x**2)*z)*exp(-a*r2)
        case (-5)
          dYdz=(-2.36661916223175203d0*y*(y**4-1.0d1*x**2*y**2+5.0d0*x**4)*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (-4)
          dYdz=(4.03651920582979328d0*x*y*(y+x)*(y-1.0d0*x)*z*(1.0d0*a*(1.1d1*z**2-1.0d0*r2)-1.0d1))*exp(-a*r2)
        case (-3)
          dYdz=(9.21205259514923499d-1*y*(y**2-3.0d0*x**2)*(2.2d1*a*z**4+1.0d0*((-6.0d0*a*r2)-2.7d1)*z**2+3.0d0*r2))*exp(-a*r2)
        case (-2)
          dYdz=(-1.842410519029847d0*x*y*z*(3.3d1*a*z**4+1.0d0*((-1.8d1*a*r2)-4.8d1)*z**2+1.6d1*r2+1.0d0*a*r2**2))*exp(-a*r2)
        case (-1)
          dYdz=(-5.82621362518731389d-1*y*(6.6d1*a*z**6+1.0d0*((-6.0d1*a*r2)-1.05d2)*z**4+1.0d0*(7.0d1*r2+1.0d1*a*r2**2)*z**2 &
-5.0d0*r2**2))*exp(-a*r2)
        case (0)
          dYdz=(-1.27138404535256852d-1*z*(2.31d2*a*z**6+1.0d0*((-3.15d2*a*r2)-3.78d2)*z**4+1.0d0*(4.2d2*r2+1.05d2*a*r2**2)*z**2 &
-5.0d0*a*r2**3-9.0d1*r2**2))*exp(-a*r2)
        case (1)
          dYdz=(-5.82621362518731389d-1*x*(6.6d1*a*z**6+1.0d0*((-6.0d1*a*r2)-1.05d2)*z**4+1.0d0*(7.0d1*r2+1.0d1*a*r2**2)*z**2 &
-5.0d0*r2**2))*exp(-a*r2)
        case (2)
          dYdz=(9.21205259514923499d-1*(y+x)*(y-1.0d0*x)*z*(3.3d1*a*z**4+1.0d0*((-1.8d1*a*r2)-4.8d1)*z**2+1.6d1*r2+1.0d0*a*r2**2) &
)*exp(-a*r2)
        case (3)
          dYdz=(9.21205259514923499d-1*x*(3.0d0*y**2-1.0d0*x**2)*(2.2d1*a*z**4+1.0d0*((-6.0d0*a*r2)-2.7d1)*z**2+3.0d0*r2))*exp( &
-a*r2)
        case (4)
          dYdz=(-1.00912980145744832d0*(y**2-2.0d0*x*y-1.0d0*x**2)*(y**2+2.0d0*x*y-1.0d0*x**2)*z*(1.0d0*a*(1.1d1*z**2-1.0d0*r2) &
-1.0d1))*exp(-a*r2)
        case (5)
          dYdz=(-2.36661916223175203d0*x*(5.0d0*y**4-1.0d1*x**2*y**2+x**4)*(2.0d0*a*z**2-1.0d0))*exp(-a*r2)
        case (6)
          dYdz=(1.36636821038382864d0*a*(y+x)*(y-1.0d0*x)*(y**2-4.0d0*x*y+x**2)*(y**2+4.0d0*x*y+x**2)*z)*exp(-a*r2)
        case default
          print *,'Error: dYdz not implemented for l=',l,'m=',m
          stop
      end select
    case default
      print *,'Error: dYdz not implemented for l=',l,'m=',m
      stop
  end select
  
end function

