#include "eri.h"

extern "C" {

double c_eri_(const double  *a1, const double* r1, int *l1, int *m1, int *n1,
              const double  *a2, const double* r2, int *l2, int *m2, int *n2,
              const double  *a3, const double* r3, int *l3, int *m3, int *n3,
              const double  *a4, const double* r4, int *l4, int *m4, int *n4)
  {
    LIBINT2_REF_REALTYPE r1_[3];
    LIBINT2_REF_REALTYPE r2_[3];
    LIBINT2_REF_REALTYPE r3_[3];
    LIBINT2_REF_REALTYPE r4_[3];
    LIBINT2_REF_REALTYPE a1_;
    LIBINT2_REF_REALTYPE a2_;
    LIBINT2_REF_REALTYPE a3_;
    LIBINT2_REF_REALTYPE a4_;
    
    a1_ = *a1;
    a2_ = *a2;
    a3_ = *a3;
    a4_ = *a4;
    
    for ( int i=0 ; i<3 ; i++ )
    {
      r1_[i]=r1[i];
      r2_[i]=r2[i];
      r3_[i]=r3[i];
      r4_[i]=r4[i];
    }
    
    return eri(*l1,*m1,*n1,a1_,r1_,
               *l2,*m2,*n2,a2_,r2_,
               *l3,*m3,*n3,a3_,r3_,
               *l4,*m4,*n4,a4_,r4_,0);
  }
}
