program test_Coulomb_consistency
  
  implicit none
  
  ! local variables
  integer, parameter :: lmax = 4
  real(kind=8) :: r1(3)
  real(kind=8) :: r2(3)
  real(kind=8) :: r3(3)
  real(kind=8) :: r4(3)
  real(kind=8) :: r_tmp1(3)
  real(kind=8) :: r_tmp2(3)
  integer      :: it1
  integer      :: it2
  integer      :: it3
  integer      :: it4
  integer      :: i
  integer      :: l1
  integer      :: m1
  integer      :: l2
  integer      :: m2
  integer      :: l3
  integer      :: m3
  integer      :: l4
  integer      :: m4
  integer      :: nx1
  integer      :: ny1
  integer      :: nz1
  integer      :: nx2
  integer      :: ny2
  integer      :: nz2
  integer      :: nx3
  integer      :: ny3
  integer      :: nz3
  integer      :: nx4
  integer      :: ny4
  integer      :: nz4
  real(kind=8) :: S1
  real(kind=8) :: S2
  real(kind=8) :: C_Coulomb_C
  real(kind=8) :: CC_Coulomb_C
  real(kind=8) :: CC_Coulomb_Y
  real(kind=8) :: CC_Coulomb_CC
  real(kind=8) :: Y_Coulomb_C
  real(kind=8) :: YY_Coulomb_C
  real(kind=8) :: YY_Coulomb_CC
  real(kind=8) :: D_Coulomb_D
  real(kind=8) :: c_eri
  real(kind=8) :: aD
  real(kind=8) :: cD_pck(455)
  real(kind=8) :: cD(455)
  integer      :: iD(455)
  real(kind=8) :: rD(3)
  integer      :: nD
  real(kind=8) :: a1
  real(kind=8) :: a2
  real(kind=8) :: a3
  real(kind=8) :: a4
  integer      :: is
  logical      :: flag
  logical      :: D_Coulomb_C_shell
  logical      :: D_Coulomb_Y_shell
  real(kind=8) :: shell_value(128)
  
  ! set exponents
  a1=1.5d0
  a2=0.75d0
  a3=2.0d0
  a4=0.5d0
  
  r_tmp1 = (/ 0.0d0 , 0.0d0 , 0.0d0 /)
  r_tmp2 = (/ 0.0d0 , 0.0d0 , 2.0d0 /)
  
  do it2 = 1,2
  do it3 = 1,2
  do it4 = 1,2
    
    print *,"tic"
    
    r1 = 0.0d0
    r2 = 0.0d0
    r3 = 0.0d0
    r4 = 0.0d0
    
    r1(3)=2.0d0
    if ( it2 > 1 ) r2(3)=2.0d0
    if ( it3 > 1 ) r3(3)=2.0d0
    if ( it4 > 1 ) r4(3)=2.0d0
    
    do l1=1,lmax
    do nx1=l1,0,-1
    do ny1=l1-nx1,0,-1
      nz1 = l1 - nx1 - ny1
    
      do l2=4,lmax
      do nx2=l2,0,-1
      do ny2=l2-nx2,0,-1
        nz2 = l2 - nx2 - ny2
        
        print *,nx1,ny1,nz1,nx2,ny2,nz2
        
        do l3=4,lmax
        do nx3=l3,0,-1
        do ny3=l3-nx3,0,-1
          nz3 = l3 - nx3 - ny3
          
          do l4=4,lmax
          do nx4=l4,0,-1
          do ny4=l4-nx4,0,-1
            nz4 = l4 - nx4 - ny4
            
            S1 = CC_Coulomb_CC(a1,r1,nx1,ny1,nz1,a2,r2,nx2,ny2,nz2,a3,r3,nx3,ny3,nz3,a4,r4,nx4,ny4,nz4)
            S2 = c_eri(a1,r1,nx1,ny1,nz1,a2,r2,nx2,ny2,nz2,a3,r3,nx3,ny3,nz3,a4,r4,nx4,ny4,nz4)
            
!            print *,S1,S2,(S1-S2)/(S1+S2)
!            if ( abs(S2)>1.0d-10 .or. abs(S1)>1.0d-10 ) print *,S1,S2,(S1-S2)/(S1+S2)
            if ( abs(S2)>1.0d-10 .or. abs(S1)>1.0d-10 ) then
              if ( (S1-S2)/(S1+S2)>1.0d-8 ) then
                print *,S1,S2,(S1-S2)/(S1+S2)
                print *,nx1,ny1,nz1
                print *,nx2,ny2,nz2
                print *,nx3,ny3,nz3
                print *,nx4,ny4,nz4
                stop
              end if
            end if
          end do
          end do
          end do
          
        end do
        end do
        end do
        
      end do
      end do
      end do
      
    end do
    end do
    end do
    
  end do
  end do
  end do
  
  return 
  
  print *,CC_Coulomb_C(a1,r1,1,1,0,a2,r2,2,0,2,a3,r3,0,2,2)
  print *,c_eri(a1,r1,1,1,0,a2,r2,2,0,2,a3,r3,0,2,2,0.0d0,r3,0,0,0)
  
  return
  
  ! loop on r1 centers
  do it1=1,16
    
    ! randomize r1 center
    call random_number(r_tmp1)
    call random_number(r_tmp2)
    r1 = r_tmp1-r_tmp2
    
    ! explore special confs for r1
    if ( iand(it1,1) > 0 ) r1(1)=0.0d0
    if ( iand(it1,2) > 0 ) r1(2)=0.0d0
    if ( iand(it1,4) > 0 ) r1(3)=0.0d0
    
    ! loop on r2 center
    do it2=1,16
      
      ! randomize r2 center
      call random_number(r_tmp1)
      call random_number(r_tmp2)
      r2 = r_tmp1-r_tmp2
      
      ! explore special confs for r2
      if ( iand(it2,1) > 0 ) r2(1)=0.0d0
      if ( iand(it2,2) > 0 ) r2(2)=0.0d0
      if ( iand(it2,4) > 0 ) r2(3)=0.0d0
      
      ! loop on r3 center
      do it3=1,16
        
        ! randomize r2 center
        call random_number(r_tmp1)
        call random_number(r_tmp2)
        r3 = r_tmp1-r_tmp2
        
        ! explore special confs for r3
        if ( iand(it3,1) > 0 ) r3(1)=0.0d0
        if ( iand(it3,2) > 0 ) r3(2)=0.0d0
        if ( iand(it3,4) > 0 ) r3(3)=0.0d0
        
        print *,r1,r2,r3
        
        ! loop on orb 1 momenta
        do l1=0,lmax
          do m1=-l1,l1
            ! loop on orb 2 momenta
            do l2=0,lmax
              do m2=-l2,l2
                
                ! compute D basis decomposition of Y1 x Y2 product
                call YY_to_D(a1,r1,l1,m1,a2,r2,l2,m2,aD,rD,cD_pck,iD,nD)
                
                ! unpack coeffs 
                cD=0.0d0
                do i=1,nD
                  cD(iD(i))=cD_pck(i)
                end do
                
                ! loop on orb 3 exponents
                do l3=0,lmax
                  
                  ! compute shell at once
                  flag = D_Coulomb_C_shell(aD,rD,cD,l1+l2,a3,r3,l3,shell_value)
                  
                  ! compare with single values
                  is=0
                  do nx3=l3,0,-1
                    do ny3=l3-nx3,0,-1
                      
                      ! get nz3
                      nz3=l3-nx3-ny3
                      
                      ! increment is
                      is=is+1
                      
                      ! get value
                      S1 = shell_value(is)
                      S2 = YY_Coulomb_C(a1,r1,l1,m1,a2,r2,l2,m2,a3,r3,nx3,ny3,nz3)
                      
                      if ( 2.0* abs( ( S1 - S2 ) / ( S1 + S2 ) ) > 1.0d-3 .or. S1.ne.S1 ) then

                        ! compare
                        write (*,'(A,I2,A,I2,A,I2,A,I2,A,I2,A,I2,A,I2,A,E22.16,A,E22.16)')  & 
                                 '  l1=',l1,'  m1=',m1, &
                                 '  l2=',l2,'  m2=',m2, &
                                 '  nx3=',nx3,'  ny3=',ny3,'  nz3=',nz3, &
                                 '  shell=',S1,'  YY_Coulomb_C=',S2
                        print *,'Error: expert <CC> Two center Coulomb failed'
                        print *,''
                        print *,'test_Coulomb failed'
                        print *,''
                        stop 1
                      end if
                    end do
                  end do
                end do
              end do
            end do
          end do
        end do
      end do
    end do
  end do
  
end program

